<?php namespace Usablenet\Providers;

use Illuminate\Support\ServiceProvider;
use Usablenet\Leads\Marketo\MarketoHandler;

class LeadsServiceProvider extends ServiceProvider {
	public function register()
	{
		$this->app->bind(
			'Usablenet\Leads\LeadsInterface',
			'Usablenet\Leads\Marketo\MarketoHandler'
		);
	}
}