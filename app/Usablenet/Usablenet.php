<?php namespace Usablenet;

use URL;

class Usablenet {

	public static function slug($str)
	{
		$slug = urlencode(str_replace(' ', '-', strtolower($str)));
		return $slug;
	}

	public static function unslug($str)
	{
		$slug = str_replace('-', ' ', urldecode($str));
		return $slug;
	}

	public static function buildcrumbs($url)
	{
		preg_match_all(
			'/^(.*:)\/\/([a-z\-.]+)(:[0-9]+)?(.*)$/',
			$url,
			$matches);

		$path = end($matches);

		if (substr($path[0], 0, 1) == '/')
		{
			$path[0] = substr($path[0], 1);
		}

		if (substr($path[0], -1) == '/')
		{
			$path[0] = substr($path[0], 0, strlen($path[0]));
		}

		$pathParts = explode('/', $path[0]);

		$crumbs = '<li><a href="'.URL::to('/').'">Home</a></li>';
		$previous_parts = '';

		foreach($pathParts as $path)
		{
			$previous_parts .= '/' . $path;
			$name = ( ucwords(self::unslug(substr($path, 0, 100))) === 'Archive' ) ? 'News' : ucwords(self::unslug(substr($path, 0, 100)));

			$crumbs .= '<li>';
			$crumbs .= '<a href="'.URL::to($previous_parts).'">'.$name.'</a>';
			$crumbs .= '</li>';
		}

		return $crumbs;
	}
}