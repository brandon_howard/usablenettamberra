<?php namespace Usablenet\Leads\Marketo;

use Usablenet\Leads\LeadsInterface;
use Usablenet\Leads\Marketo\MarketoApi;
use DateTimeZone;
use DateTime;
use stdClass;
use SoapClient;
use SoapHeader;

class MarketoHandler implements LeadsInterface {

	/**
	 * Sends an email with the provided params
	 *
	 * @param  string $email
	 * @param  string $htmlView
	 * @param  string $textView
	 * @param  string $subject
	 * @param  array $data
	 * @return boolean
	*/
	public function create($programName, $data)
	{
		$debug = true;

		// $marketoSoapEndPoint     = "https://644-RZH-969.mktoapi.com/soap/mktows/2_3";
		// $marketoUserId           = "usablenet1_53740914508AD33CEC0B14";
		// $marketoSecretKey        = "0112125726715796550088AADD3333DEEECC111EFC89";
		// $marketoNameSpace        = "http://www.marketo.com/mktows/";

		$user_id = 'usablenet1_53740914508AD33CEC0B14';
		$encryption_key = '0112125726715796550088AADD3333DEEECC111EFC89';
		$soap_endpoint = 'https://644-RZH-969.mktoapi.com/soap/mktows/2_3';

		//Connecting to Marketo
		$marketo_client = new mktSampleMktowsClient($user_id, $encryption_key, $soap_endpoint);

		//Passing in the user's email address to see if they
		//already exist as a lead in Marketo
		$checkLead = $marketo_client->getLead('EMAIL', $data['Email']);

		//If the user's email exists in Marketo, grabbing the Lead Record ID so we can
		//update this lead with the new form as well as Marketo cookie on their system
		//Otherwise, we're creating a new lead.
		if (isset($_COOKIE['_mkto_trk'])) {
			if (is_object($checkLead)) {
				$leadRecord = $checkLead->result->leadRecordList->leadRecord;
				$submitLead = $marketo_client->syncLead($leadRecord->Id, $data['Email'], $_COOKIE['_mkto_trk'], $data);
			} else {
				$submitLead = $marketo_client->syncLead("", $data['Email'], $_COOKIE['_mkto_trk'], $data);
			}
		}

		if (isset($_COOKIE['_mkto_trk'])) {
			//Get Campaigns
			$campaigns = $marketo_client->getCampaigns();

			// Search campaigns for program name
			foreach($campaigns->result->campaignRecordList->campaignRecord as $campaign)
			{
				if (strstr($campaign->name, $programName))
				{
					//Adding the lead to one of our campaigns
					$marketo_client->requestCampaign($campaign->id, $submitLead->result->leadId);
				}
			}
		}

		if ($debug)
		{
			//print_r($marketo_client);
			return true;
		}

		return true;

	}
}