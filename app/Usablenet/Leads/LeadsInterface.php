<?php namespace Usablenet\Leads;

interface LeadsInterface {

	public function create($campaignName, $data);

}