# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.29)
# Database: thingms
# Generation Time: 2013-12-09 17:33:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cars`;

CREATE TABLE `cars` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_name` varchar(255) DEFAULT NULL,
  `sku` int(11) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_date` date NOT NULL,
  `color` char(6) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;

INSERT INTO `cars` (`id`, `model_name`, `sku`, `picture`, `release_date`, `color`, `video`, `published`, `created_at`, `updated_at`)
VALUES
	(1,'Jeep Wrangler',0,'http://localhost:8000/img/uploads/jeep-wrangler-01_5.jpg','0000-00-00','Green',NULL,1,'0000-00-00 00:00:00','2013-12-09 17:32:53');

/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dealerships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dealerships`;

CREATE TABLE `dealerships` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `type` varchar(10) NOT NULL DEFAULT '',
  `website_url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dealerships` WRITE;
/*!40000 ALTER TABLE `dealerships` DISABLE KEYS */;

INSERT INTO `dealerships` (`id`, `name`, `description`, `type`, `website_url`)
VALUES
	(1,'Oakwood\'s Arrow Auto Auction',NULL,'','');

/*!40000 ALTER TABLE `dealerships` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tms_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tms_pages`;

CREATE TABLE `tms_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `short_description` text,
  `blade` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `head` text,
  `footer` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tms_pages` WRITE;
/*!40000 ALTER TABLE `tms_pages` DISABLE KEYS */;

INSERT INTO `tms_pages` (`id`, `name`, `short_description`, `blade`, `meta_title`, `meta_description`, `meta_keywords`, `head`, `footer`, `created_at`, `updated_at`)
VALUES
	(1,'FrontEnd Example','This page provides a quick demo of all the editable fields for ThingMS.','frontend-examples',NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `tms_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `email`, `created_at`, `updated_at`)
VALUES
	(2,'dolbex','$2y$08$1lSJDJPF2roMjMEom6avNOq1t4pUDxFBZy9/yhTIkjo.s376UN1wO','gary@lbm.co','2013-12-09 15:55:58','2013-12-09 15:55:58');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
