<?php

use Illuminate\Database\Migrations\Migration;

class CreatePosts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function($table) {
$table->increments('id');
$table->integer('author_id')->nullable();
$table->integer('category_id')->nullable();
$table->string('main_image', 255)->nullable();
$table->string('image_thumb', 255)->nullable();
$table->string('title', 255)->nullable();
$table->string('publisher', 255)->nullable();
$table->text('article')->nullable();
$table->text('headline')->nullable();
$table->dateTime('posted_on')->nullable();
$table->string('slug', 255)->nullable();
$table->string('source_link', 255)->nullable();
$table->dateTime('created_at')->nullable();
$table->dateTime('updated_at')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }

}
?>