<?php

use Illuminate\Database\Migrations\Migration;

class CreateTmsMenus extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_menus', function($table) {
$table->increments('id');
$table->string('name', 50);
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tms_menus');
    }

}
?>