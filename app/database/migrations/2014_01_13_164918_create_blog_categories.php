<?php

use Illuminate\Database\Migrations\Migration;

class CreateBlogCategories extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_categories', function($table) {
$table->increments('id');
$table->string('name', 255)->nullable();
$table->string('about', 255)->nullable();
$table->dateTime('created_at')->nullable();
$table->dateTime('updated_at')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_categories');
    }

}
?>