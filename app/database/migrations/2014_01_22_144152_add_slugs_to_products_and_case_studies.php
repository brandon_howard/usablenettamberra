<?php

use Illuminate\Database\Migrations\Migration;

class AddSlugsToProductsAndCaseStudies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function($table) {
            $table->string('slug')->after('title');
        });

        Schema::table('case_studies', function($table) {
            $table->string('slug')->after('company_name');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function($table) {
            $table->dropColumn('slug');
        });

        Schema::table('case_studies', function($table) {
            $table->dropColumn('slug');
        });
	}

}