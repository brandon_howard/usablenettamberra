<?php

use Illuminate\Database\Migrations\Migration;

class CreatePartners extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function($table) {
$table->increments('id');
$table->string('name', 255)->nullable();
$table->text('headline')->nullable();
$table->text('description')->nullable();
$table->string('logo', 255)->nullable();
$table->string('website', 255)->nullable();
$table->integer('sort')->nullable();
$table->boolean('fully_published')->nullable();
$table->dateTime('created_at')->nullable();
$table->dateTime('updated_at')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partners');
    }

}
?>