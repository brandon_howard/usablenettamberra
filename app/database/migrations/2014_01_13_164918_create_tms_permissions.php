<?php

use Illuminate\Database\Migrations\Migration;

class CreateTmsPermissions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_permissions', function($table) {
$table->increments('id');
$table->string('action', 100);
$table->text('rules')->nullable();
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tms_permissions');
    }

}
?>