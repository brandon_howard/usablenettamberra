<?php

use Illuminate\Database\Migrations\Migration;

class AddSlugToPartner extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partners', function($table) {
            $table->string('slug')->after('name');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partners', function($table) {
            $table->dropColumn('slug');
        });
	}

}