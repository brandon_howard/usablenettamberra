<?php

use Illuminate\Database\Migrations\Migration;

class CreateTmsContentDates extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_content_dates', function($table) {
$table->increments('id');
$table->integer('page_id');
$table->string('key', 100);
$table->dateTime('value');
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tms_content_dates');
    }

}
?>