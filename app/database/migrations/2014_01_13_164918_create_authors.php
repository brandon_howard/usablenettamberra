<?php

use Illuminate\Database\Migrations\Migration;

class CreateAuthors extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function($table) {
$table->increments('id');
$table->string('name', 255)->nullable();
$table->string('title', 255)->nullable();
$table->dateTime('created_at')->nullable();
$table->dateTime('updated_at')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('authors');
    }

}
?>