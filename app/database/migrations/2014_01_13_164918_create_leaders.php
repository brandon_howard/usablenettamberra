<?php

use Illuminate\Database\Migrations\Migration;

class CreateLeaders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaders', function($table) {
$table->increments('id');
$table->string('name', 255)->nullable();
$table->string('title', 255)->nullable();
$table->text('description')->nullable();
$table->string('headshot', 255)->nullable();
$table->integer('sort')->nullable();
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leaders');
    }

}
?>