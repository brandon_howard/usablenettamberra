<?php

use Illuminate\Database\Migrations\Migration;

class CreateCareers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function($table) {
$table->increments('id');
$table->string('job_title', 255);
$table->text('description')->nullable();
$table->text('responsibilities')->nullable();
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('careers');
    }

}
?>