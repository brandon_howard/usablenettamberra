<?php

use Illuminate\Database\Migrations\Migration;

class CreateTmsContentColors extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_content_colors', function($table) {
$table->increments('id');
$table->integer('page_id')->nullable();
$table->string('key', 100);
$table->string('value', 7);
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tms_content_colors');
    }

}
?>