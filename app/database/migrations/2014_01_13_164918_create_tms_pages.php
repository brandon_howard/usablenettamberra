<?php

use Illuminate\Database\Migrations\Migration;

class CreateTmsPages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_pages', function($table) {
$table->increments('id');
$table->integer('template_id');
$table->string('name', 255);
$table->boolean('published')->nullable();
$table->string('slug', 50);
$table->text('short_description')->nullable();
$table->string('meta_title', 255)->nullable();
$table->string('meta_description', 255)->nullable();
$table->string('meta_keywords', 255)->nullable();
$table->text('head')->nullable();
$table->text('footer')->nullable();
$table->dateTime('created_at');
$table->dateTime('updated_at');
$table->dateTime('deleted_at')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tms_pages');
    }

}
?>