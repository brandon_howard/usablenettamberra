<?php

use Illuminate\Database\Migrations\Migration;

class CreateCaseStudies extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_studies', function($table) {
$table->increments('id');
$table->string('company_name', 255);
$table->string('large_logo', 255);
$table->string('small_logo', 255)->nullable();
$table->string('image_1', 255);
$table->string('image_2', 255);
$table->string('image_3', 255);
$table->text('services');
$table->string('headline', 255);
$table->text('description');
$table->integer('sort');
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('case_studies');
    }

}
?>