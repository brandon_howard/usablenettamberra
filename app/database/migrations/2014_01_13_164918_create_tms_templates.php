<?php

use Illuminate\Database\Migrations\Migration;

class CreateTmsTemplates extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_templates', function($table) {
$table->increments('id');
$table->string('name', 255);
$table->string('layout', 255)->nullable();
$table->string('blade', 255);
$table->text('values')->nullable();
$table->text('models');
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tms_templates');
    }

}
?>