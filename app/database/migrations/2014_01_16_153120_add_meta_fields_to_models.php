<?php

use Illuminate\Database\Migrations\Migration;

class AddMetaFieldsToModels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function($table) {
            $table->string('meta_title')->after('case_study_id');
            $table->text('meta_description')->after('meta_title');
            $table->string('meta_keywords')->after('meta_description');
        });

        Schema::table('posts', function($table) {
            $table->string('meta_title')->after('source_link');
            $table->text('meta_description')->after('meta_title');
            $table->string('meta_keywords')->after('meta_description');
        });

        Schema::table('case_studies', function($table) {
            $table->string('meta_title')->after('description');
            $table->text('meta_description')->after('meta_title');
            $table->string('meta_keywords')->after('meta_description');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function($table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keywords');
        });

        Schema::table('posts', function($table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keywords');
        });

        Schema::table('case_studies', function($table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keywords');
        });
	}

}