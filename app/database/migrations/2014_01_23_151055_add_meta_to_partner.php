<?php

use Illuminate\Database\Migrations\Migration;

class AddMetaToPartner extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partners', function($table) {
            $table->string('meta_title')->after('slug');
            $table->text('meta_description')->after('meta_title');
            $table->string('meta_keywords')->after('meta_description');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partners', function($table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keywords');
        });
	}

}