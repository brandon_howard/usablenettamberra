<?php

use Illuminate\Database\Migrations\Migration;

class CreateLocations extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function($table) {
$table->increments('id');
$table->string('map', 255)->nullable();
$table->string('name', 255);
$table->string('city_abbreviation', 15)->nullable();
$table->string('street_address', 255)->nullable();
$table->string('street_address_2', 255)->nullable();
$table->string('city', 255)->nullable();
$table->string('state', 255)->nullable();
$table->string('zipcode', 255)->nullable();
$table->string('phone', 50)->nullable();
$table->string('fax', 50)->nullable();
$table->dateTime('created_at')->nullable();
$table->dateTime('updated_at')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }

}
?>