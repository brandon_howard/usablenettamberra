<?php

use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function($table) {
$table->increments('id');
$table->string('icon', 50)->nullable();
$table->string('title', 255)->nullable();
$table->string('description', 255)->nullable();
$table->text('headline')->nullable();
$table->text('long_description')->nullable();
$table->integer('case_study_id')->nullable();
$table->string('side_column_video_title', 255)->nullable();
$table->string('side_column_video_image', 255)->nullable();
$table->string('side_column_video', 255)->nullable();
$table->string('main_column_video_title', 255)->nullable();
$table->string('main_column_video_image', 255)->nullable();
$table->string('main_column_video', 255)->nullable();
$table->dateTime('created_at')->nullable();
$table->dateTime('updated_at')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }

}
?>