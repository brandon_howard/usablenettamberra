<?php

use Illuminate\Database\Migrations\Migration;

class CreateTmsMenuItems extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_menu_items', function($table) {
$table->increments('id');
$table->integer('menu_id');
$table->integer('page_id')->nullable();
$table->integer('parent')->nullable();
$table->string('label', 255);
$table->string('url', 255)->nullable();
$table->integer('sort')->nullable();
$table->dateTime('created_at');
$table->dateTime('updated_at');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tms_menu_items');
    }

}
?>