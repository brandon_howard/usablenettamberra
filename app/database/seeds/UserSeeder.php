<?php
class UserSeeder
extends DatabaseSeeder
{
    public function run()
    {
        $users = [
            [
                "username" => "dolbex",
                "password" => Hash::make("sublime"),
                "email"    => "gary@lbm.co"
            ]
        ];
        foreach ($users as $user)
        {
            User::create($user);
        }
    }
}