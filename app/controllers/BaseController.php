<?php

use Illuminate\Routing\Controller;

class BaseController extends Controller {

	/**
	 * The Validator instac
	 *
	 * @var Validator
	 */
	public $validator;

	/**
	 * Any validation or error message
	 *
	 * @var Array
	 */
	public $message;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
	 * Accepts a model instance, input data, validates and saves a new record
	 *
	 * @return false | instance of $model
	*/
	public function destroy($id)
	{
		if (Request::ajax())
		{
			$input = array_except(Input::all(), array('_method'));
			$model = new $input['model'];

			$record = $model->find($id);

			$affectedRows = $record->delete();

			return Response::json(['success' => true, 'data' => $affectedRows]);
		}
	}

	/**
	 * Accepts a model instance, input data, validates and saves a new record
	 *
	 * @return false | instance of $model
	*/
	public function store()
	{
		if (Request::ajax())
		{
			$input            = array_except(Input::all(), '_method');
			$records_effected = array();
			$models           = array();
			$encode_que       = array();

			// Build models array
			foreach($input as $model => $fields)
			{
				$models[$model] = new $model;
			}

			// Check for attributed content types such as links
			foreach($input as $model => $fields)
			{
				foreach($fields as $fieldName => $field)
				{
					if (strpos($fieldName, '||') > 0)
					{
						$fieldAndAtt = explode('||', $fieldName);

						if (is_object($field) && get_class($field) === 'Symfony\Component\HttpFoundation\File\UploadedFile')
						{
							if (Input::hasFile($model.'.'.$fieldName))
							{
								$input[$model][$fieldAndAtt[0]][$fieldAndAtt[1]] =
									$models[$model]->writeFile(Input::file($model.'.'.$fieldName));
							}
						}
						else
						{
							$input[$model][$fieldAndAtt[0]][$fieldAndAtt[1]] = $field;
						}

						$encode_que[$model][$fieldAndAtt[0]] = 1;

						unset($input[$model][$fieldName]);
					}
				}
			}

			foreach ($encode_que as $model => $fields) {
				foreach ($fields as $fieldName => $value) {
					if ($fieldName !== 'clearfield')
					{
						$input[$model][$fieldName] = json_encode($input[$model][$fieldName]);
					}
				}
			}



			foreach($input as $model => $fields)
			{
				$this->validator = Validator::make($fields, $models[$model]->storeRules, $models[$model]->messages);

				if ($this->validator->fails())
				{

					$this->message = "There were validation errors.";
					return Response::json(array(
						'flash' => 'There were validation errors',
						'errors' => $this->validator->messages()->toArray()
					), 400);
				}
			}


			foreach($input as $model => $fields)
			{
				unset($fields['clearfield']);

				foreach($fields as $fieldName => $field)
				{
					if (is_object($field) && get_class($field) === 'Symfony\Component\HttpFoundation\File\UploadedFile')
					{
						if (Input::hasFile($model.'.'.$fieldName))
						{
							$fields[$fieldName] = $models[$model]->writeFile(Input::file($model.'.'.$fieldName));
							$input[$model][$fieldName] = $fields[$fieldName];

							dd($input);
						}
					}
				}

				$record = new $model($fields);
				$records_effected[] = $record->save();
			}

			return Response::json(['success' => true, 'data' => $input]);
		}
	}

	public function update($id)
	{
		if (Request::ajax())
		{
			$input            = array_except(Input::all(), '_method');
			$records_effected = array();
			$models           = array();
			$encode_que       = array();

			// Build models array
			foreach($input as $model => $fields)
			{
				$models[$model] = new $model;
			}

			// Check for attributed content types such as links and images
			foreach($input as $model => $fields)
			{
				foreach($fields as $fieldName => $field)
				{
					if (strpos($fieldName, '||') > 0)
					{
						$fieldAndAtt = explode('||', $fieldName);

						if (is_object($field) && get_class($field) === 'Symfony\Component\HttpFoundation\File\UploadedFile')
						{
							if (Input::hasFile($model.'.'.$fieldName))
							{
								$input[$model][$fieldAndAtt[0]][$fieldAndAtt[1]] =
									$models[$model]->writeFile(Input::file($model.'.'.$fieldName));
							}
						}
						else
						{
							$input[$model][$fieldAndAtt[0]][$fieldAndAtt[1]] = $field;
						}

						$encode_que[$model][$fieldAndAtt[0]] = 1;
						unset($input[$model][$fieldName]);
					}
				}
			}

			// JSON Encode any attributed variables
			foreach ($encode_que as $model => $fields) {
				foreach ($fields as $fieldName => $value) {
					if ($fieldName !== 'clearfield')
					{
						if(array_key_exists('orig_src', $input[$model][$fieldName]))
						{
							if (!array_key_exists('src', $input[$model][$fieldName]))
							{
								$input[$model][$fieldName]['src'] = $input[$model][$fieldName]['orig_src'];
							}

							unset($input[$model][$fieldName]['orig_src']);
						}

						$input[$model][$fieldName] = json_encode($input[$model][$fieldName]);
					}
				}
			}

			// Validate the various models
			foreach($input as $model => $fields)
			{
				$validation = Validator::make($input, $models[$model]->updateFieldsRules, $models[$model]->messages);

				if ($validation->fails())
				{
					// Throw validation error
					return Response::json(array(
						'flash' => 'There were validation errors',
						'errors' => $validation->messages()->toArray()
					), 400);
				}
			}

			// Set final data and save
			foreach($input as $model => $fields)
			{
				foreach($fields as $fieldName => $field)
				{
					if (is_object($field) && get_class($field) === 'Symfony\Component\HttpFoundation\File\UploadedFile')
					{
						if (Input::hasFile($model.'.'.$fieldName))
						{
							$fields[$fieldName] = $models[$model]->writeFile(Input::file($model.'.'.$fieldName));
							$input[$model][$fieldName] = $fields[$fieldName];
						}
					}
				}

				if (isset($fields['clearfield']))
				{
					if(is_array($fields['clearfield'])){
						foreach($fields['clearfield'] as $clearfield)
						{
							if($clearfield !== '')
							{
								$fields[$clearfield] = null;
							}
						}
					}
					unset($fields['clearfield']);
				}

				if (isset($fields[''])) unset($fields['']);
				$fieldsUpdated[] = $fields;

				$records_effected[] = $models[$model]->find($id)->update($fields);
			}

			return Response::json(['success' => $records_effected, 'data' => $input]);
		}
	}
}
