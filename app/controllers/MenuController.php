<?php

use Tms\Menu;
use Tms\MenuItem;

class MenuController extends BaseController {

	protected $Menu;
	protected $MenuItem;

	public function __construct(Menu $Menu, MenuItem $MenuItem)
	{
		$this->Menu = $Menu;
		$this->MenuItem = $MenuItem;
	}

	public function edit($id)
	{
		$menu = $this->Menu->buildMenus($id);

		return View::make('tms.menu-edit')
						->withMenu($menu[key($menu)]);
	}

	public function update($id)
	{
		$input = array_except(Input::all(), '_method');

		$this->validator = Validator::make($input, $this->Menu->updateRules, $this->Menu->messages);

		if ($this->validator->passes())
		{
			$menu = $this->Menu->findOrFail($id);
			$menuItems = $input['menuitem'];
			unset($input['menuitem']);

			$menu->update($input);

			$i = 0;
			foreach($menuItems as $menuitem_id => $parent)
			{
				if ($parent === 'null') $parent = null;

				$menuItem = $this->MenuItem->findOrFail($menuitem_id);
				$menuItem->update(array('sort' => $i, 'parent' => $parent));
				$i++;
			}

			return $menu;

		} else {
			$this->message = "There were validation errors.";
			return false;
		}
	}

}