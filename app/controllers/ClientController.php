<?php

use Tms\Menu;
use Tms\Page;

class ClientController extends BaseController {

	protected $Client;
	protected $Menu;
	protected $Page;

	public function __construct(Client $Client, Menu $Menu, Page $Page)
	{
		$this->Client = $Client;
		$this->Menu = $Menu;
		$this->Page = $Page;
	}

	public function show($slug)
	{
		if(ctype_digit((string)$slug))
		{
			$client = $this->Client->findOrFail($slug);
			return Redirect::action('ClientController@show', $client->slug);
		}

		$client = $this->Client->whereSlug($slug)->first();
		$menus = $this->Menu->buildMenus();
		$page = $this->Page->find(1);

		$meta = array();
		$meta['meta_keywords']    = $client->meta_keywords;
		$meta['meta_title']       = $client->meta_title;
		$meta['meta_description'] = $client->meta_description;

		return View::make('usablenet.client')
						->withPage($page)
						->withMeta($meta)
						->withClient($client)
						->withMenus($menus);

	}

	public function slugThemAll()
	{
		$clients = $this->Client->get();
		foreach($clients as $client)
		{
			$slug = Str::slug(strip_tags($client->company_name));
			$iteration = $this->testSlug($slug);
			if($iteration > 0)
			{
				$slug = $slug . '-' . $iteration;
			}

			$client->update(array('slug' => $slug));
		}

	}

	private function testSlug($slug, $i = 0)
	{
		if ($this->Client->whereSlug($slug . '-' . $i)->count() > 0) {
			$j = $i + 1;
			$i = $this->testSlug($slug, $j);
		}

		return $i;
	}

}