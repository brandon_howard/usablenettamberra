<?php
class UserController extends BaseController
{
	public function login()
	{
		return View::make("login");
	}

	public function logout(){
		Auth::logout();
		return Redirect::action('UserController@login');
	}

	public function loginAction()
	{
		$username    = Input::get('username');
		$password    = Input::get('password');
		$remember_me = (is_null(Input::get('remember_me'))) ? null : true;

		if (Auth::attempt(array('username' => $username, 'password' => $password), $remember_me))
		{
			return Redirect::to('/');
		}

		return Redirect::action('UserController@login');
	}
}