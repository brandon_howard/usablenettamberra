<?php

use Tms\ThingmsModel;
use Tms\Page;
use Tms\Menu;
use Tms\Repositories\TemplatesRepository;

class ServicesController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Injected Page Modal
     * @var Page
     */
    public $Page;

    /**
     * Injected Menu Modal
     * @var Menu
     */
    public $Menu;

    /**
     * Inject TemplatesRepository
     * @var  TemplatesRepository
     */
    public $TemplatesRepository;

    /**
     * Injected Dealership Model
     * @var [type]
     */
    public $Dealership;

    public function __construct(Page $Page, Menu $Menu, TemplatesRepository $TemplatesRepository)
    {
        $this->Page                = $Page;
        $this->Menu                = $Menu;
        $this->TemplatesRepository = $TemplatesRepository;
    }

    public function fileUpload()
    {
        $CKEditor = Input::get('CKEditor');
        $funcNum = Input::get('CKEditorFuncNum');
        $langCode = Input::get('langCode');

        if (Input::hasFile('upload'))
        {
            $validation = Validator::make(Input::all(), $this->Page->imageUploadRules, $this->Page->messages);

            if ($validation->passes())
            {

                $Thingms = new ThingmsModel;
                $url = $Thingms->writeFile(Input::file('upload'));

                $message = "File saved successfully";

                return "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction($funcNum, '/$url', '$message')</script>";

            }
        }
    }

    public function getPageList()
    {
        return $this->Page->orderBy('name')->lists('name', 'id');
    }

    public function pageManager($id=null)
    {
        if(Request::ajax())
        {
            $pages = $this->Page->where('name', '<>', 'Stub')->get();
            $menus = $this->Menu->buildMenus();

            return View::make('tms.page-manager')
                        ->withCurrent($id)
                        ->withPages($pages)
                        ->withMenus($menus);
        }
        else
        {
            App::abort(404);
        }
    }

    public function create()
    {
        if(Request::ajax())
        {
            $templateOptions = $this->TemplatesRepository->getTemplateOptions();
            $templateOptions = array('' => 'Please Select a Template') + $templateOptions;

            return View::make('tms.page-create')
                        ->withTemplates($templateOptions);
        }
        else
        {
            App::abort(404);
        }
    }

    public function storePage()
    {
        if(Request::ajax())
        {
            $input = array_except(Input::all(), array('_method', '_token'));

            $this->validator = Validator::make($input, $this->Page->storeRules, $this->Page->messages);

            if ($this->validator->passes())
            {
                $this->Page->fill($input);

                if ($this->Page->save())
                {
                    $page = $this->Page->find($this->Page->id);
                    $templateSaved = $this->TemplatesRepository->populateFields($page);

                    if ($templateSaved)
                    {
                        return Response::json(['success' => true, 'data' => $input]);
                    }
                    else
                    {
                        return $templateSaved;
                    }
                }
                else
                {
                    return Response::json(array(
                        'flash' => 'There was a problem storing the page data',
                        'errors' => array('There was a problem storing the page data')
                    ), 400);
                }
            } else {
                return Response::json(array(
                    'flash' => 'There were validation errors',
                    'errors' => $this->validator->messages()->toArray()
                ), 400);
            }
        }
        else
        {
            App::abort(404);
        }
    }

    public function edit($id)
    {
        if(Request::ajax())
        {
            $page = $this->Page->findOrFail($id);
            $templateOptions = $this->TemplatesRepository->getTemplateOptions();

            return View::make('tms.page-edit')
                        ->withTemplates($templateOptions)
                        ->withPage($page);
        }
        else
        {
            App::abort(404);
        }
    }

    public function updatePage($id)
    {
        if(Request::ajax())
        {
            $input = array_except(Input::all(), array('_method', '_token'));

            $page = $this->Page->find($id);
            $page->updateRules['slug'] = 'required|unique:tms_pages,slug,'. $id .',id,deleted_at,NULL';

            if(isset($input['delete']))
            {
                $page->delete();
                return Response::json(['success' => true, 'data' => $input]);
            }

            $this->validator = Validator::make($input, $page->updateRules, $page->messages);

            if ($this->validator->passes())
            {
                if ($page->update($input))
                {
                    return Response::json(['success' => true, 'data' => $input]);
                }
                else
                {
                    return Response::json(array(
                        'flash' => 'There was a problem storing the page data',
                        'errors' => array('There was a problem storing the page data')
                    ), 400);
                }
            } else {
                return Response::json(array(
                    'flash' => 'There were validation errors',
                    'errors' => $this->validator->messages()->toArray()
                ), 400);
            }
        }
        else
        {
            App::abort(404);
        }
    }

    public function scanTemplate($template_id)
    {
        $scannedTemplate = $this->TemplatesRepository->scanTemplate($template_id);
        dd($scannedTemplate);
    }

    public function sitemap()
    {
	    $blacklist = array(
		    'services',
		    'usablenet-u-campaign-enables-brands-to -drive-real-time-mobile-marketing',
			'usablenet-u-campaign-enables-brands-to-drive-real-time-mobile-marketing',
		    '/about-us/leadership',
		    'insights',
		    'leadership'
	    );

        $sitemap = App::make("sitemap");

        // set cache (key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean))
        $sitemap->setCache('Laravel.Sitemap.MySitemap.', 1);

        $sitemap->add(URL::to('/'), date('Y-m-d H:i:s', strtotime('now')), '1.0', 'daily');

        $pages = DB::table('tms_pages')->where('published', '=', 1)->get();
        foreach ($pages as $page) {
	        if ( ! in_array( $page->slug, $blacklist ) ) {
			        $sitemap->add( URL::to( $page->slug ), $page->updated_at, '1.0', 'monthly' );
	        }
        }

        $products = DB::table('products')->get();
        foreach ($products as $product)
        {
	        if ( ! in_array( $product->slug, $blacklist ) ) {
		        $sitemap->add( URL::action( 'ProductController@show', $product->slug ), $product->updated_at, '1.0', 'monthly' );
	        }
        }

        $clients = DB::table('case_studies')->get();
        foreach ($clients as $client)
        {
	        if ( ! in_array( $client->slug, $blacklist ) ) {
		        $sitemap->add( URL::action( 'ClientController@show', $client->slug ), $client->updated_at, '1.0', 'monthly' );
	        }
        }

        $partnerships = DB::table('partners')->get();
        foreach ($partnerships as $partner)
        {
	        if ( ! in_array( $partner->slug, $blacklist ) ) {
		        $sitemap->add( URL::action( 'PartnerController@show', $partner->slug ), $partner->updated_at, '1.0', 'monthly' );
	        }
        }

        $sitemap->add(URL::to('news'), $page->updated_at, '1.0', 'monthly');
        $sitemap->add(URL::to('archive/2013'), $page->updated_at, '1.0', 'yearly');
        $sitemap->add(URL::to('archive/2014'), $page->updated_at, '1.0', 'daily');

        // news
        $posts = DB::table('posts')->leftJoin('post_tag', 'posts.id', '=', 'post_tag.post_id')->whereNull('post_tag.post_id')->orderBy('posts.created_at', 'desc')->get();
        foreach ($posts as $post)
        {
	        if ( ! in_array( $post->slug, $blacklist ) ) {
		        $sitemap->add( URL::action( 'PostController@show', $post->slug ), $post->updated_at, '1.0', 'daily' );
	        }
        }
        // blog
        $posts = DB::table('posts')->join('post_tag', 'posts.id', '=', 'post_tag.post_id')->groupBy('slug')->orderBy('posts.created_at', 'desc')->get();
        foreach ($posts as $post)
        {
            if ( ! in_array( $post->slug, $blacklist ) ) {
                $sitemap->add( URL::action( 'PostController@showBlog', $post->slug ), $post->updated_at, '1.0', 'daily' );
            }
        }

        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');

    }

    public function show($slug = 'services')
    {
        $page = $this->Page->with(array('template'))->whereSlug($slug)->firstOrFail();
        $menus = $this->Menu->buildMenus();

        if (!$page->published && !(TmsAuth::isInGroup('Administrators') || TmsAuth::isInGroup('Developers')))
        {
            App::abort(404);
        }

        if(count($page) > 0)
        {
            if (!is_null($page->template->layout))
            {
                $this->layout = $page->template->layout;
            }

            $meta['meta_keywords']    = $page->meta_keywords;
            $meta['meta_title']       = $page->meta_title;
            $meta['meta_description'] = $page->meta_description;

            return View::make($page->template->blade)
                ->with(array('page'=>$page))
                ->with($page->getAdditionalPageModels())
                ->with(array('menus'=>$menus))
                ->with(array('meta'=>$meta));
        }
        else
        {
            App::abort(404);
        }
    }
}