<?php

use Usablenet\Leads\LeadsInterface;

class ContactController extends BaseController {
	private $LeadsInterface;
	public function __construct(LeadsInterface $LeadsInterface){
		$this->LeadsInterface = $LeadsInterface;
	}

	function email()
	{
		$input = Input::all();
		$validator = Validator::make($input, [
												'Email'=>'required|email',
												'FirstName'=>'required',
												'LastName'=>'required',
												'Company'=>'required'
											],
											[
												'Email.email'=>'Please provide a valid email address.',
												'Email.required'=>'Please provide your email address',
												'FirstName.required'=>'Please provide your first name.',
												'LastName.required'=>'Please provide your last name.',
												'Company.required'=>'Please provide your organiztions name.'
											]);

		if($validator->fails()){
			if ( ! Request::ajax() ) {
				return Redirect::to(URL::previous() . '#' . $input['from'])
						->withErrors($validator)
						->withInput()
						->with('message-fail-' . $input['from'], 'Please complete all fields');
			} else {
				$messages = $validator->messages();
				return Response::json(array('errors'=>$messages->all(), 'message'=>'Please complete all fields'));
			}
		}

		$this->LeadsInterface->create(Input::get('program_name'), Input::except('from', 'program_name', '_token'));

		if ( ! Request::ajax() ) {
			return Redirect::to(URL::previous() . '#' . $input['from'])
						->with('message-success-' . $input['from'], 'Thanks for your interest. We will be in touch with you soon. Have a great day!');
		} else {
			return Response::json(array('message'=>Config::get('responses.contact_us')));
		}
	}
}