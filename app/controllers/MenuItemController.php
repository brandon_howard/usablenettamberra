<?php

use Tms\MenuItem;
use Tms\Page;

class MenuItemController extends BaseController {

	protected $MenuItem;
	protected $Page;

	public function __construct(MenuItem $MenuItem, Page $Page)
	{
		$this->MenuItem = $MenuItem;
		$this->Page = $Page;
	}

	public function create($menu_id)
	{
		$pages = $this->Page->lists('name', 'id');

		array_unshift($pages, 'Please Select a Page');

		return View::make('tms.menu-item-create')
						->with('pages', $pages)
						->with('menu_id', $menu_id);
	}

	public function store()
	{
		$input = array_except(Input::all(), array('_method', 'type'));

		$this->validator = Validator::make($input, $this->MenuItem->updateRules, $this->MenuItem->messages);

		if ($this->validator->passes())
		{
			$input['sort'] = 999;
			$MenuItem = $this->MenuItem->create($input);

			return $MenuItem;
		} else {
			$this->message = "There were validation errors.";
			return false;
		}
	}

	public function storeBreak($menu_id)
	{
		$input = array(
				'menu_id' => $menu_id,
				'label' => '(Break)',
				'sort' => 999
			);
		$MenuItem = $this->MenuItem->create($input);

		return $MenuItem;
	}

	public function edit($id)
	{
		$menuItem = $this->MenuItem->find($id);
		$pages = $this->Page->lists('name', 'id');

		$pages = array('0' => 'Please Select a Page') + $pages;

		return View::make('tms.menu-item-edit')
						->with('menuItem', $menuItem)
						->with('pages', $pages);
	}

	public function update($id)
	{
		$input = array_except(Input::all(), array('_method', 'type'));

		$this->validator = Validator::make($input, $this->MenuItem->updateRules, $this->MenuItem->messages);

		if ($this->validator->passes())
		{
			$menuItem = $this->MenuItem->findOrFail($id);
			$menuItem->update($input);

			return $menuItem;
		} else {
			$this->message = "There were validation errors.";
			return false;
		}
	}

	public function destroy($id)
	{
		$menuItem = $this->MenuItem->findOrFail($id)->delete();

		return Response::json(['success' => true, 'data' => $id]);
	}

}