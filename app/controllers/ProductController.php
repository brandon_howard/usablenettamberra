<?php

use Tms\Menu;
use Tms\Page;

class ProductController extends BaseController {

	protected $Product;
	protected $Menu;
	protected $Client;
	protected $Page;

	public function __construct(Product $Product, Menu $Menu, Page $Page, Client $Client)
	{
		$this->Product = $Product;
		$this->Menu = $Menu;
		$this->Page = $Page;
		$this->Client = $Client;
	}

	public function show($slug)
	{
		if($slug == 'usablenet-assistive'){
			return Redirect::away('http://45.55.170.194/products/web-accessibility');
		}
		if(ctype_digit((string)$slug))
		{
			$product = $this->Product->find($slug);
			return Redirect::action('ProductController@show', $product->slug);
		}

		$product = $this->Product->whereSlug($slug)->first();

		$products = $this->Product->get();
		$page = $this->Page->find(1);
		$menus = $this->Menu->buildMenus();
		$caseStudiesOptions = $this->Client->lists('company_name', 'id');
		$caseStudy = $this->Client->find($product->case_study_id);
		$caseStudies = $this->Client->orderBy('sort')->limit(4)->get();

		$meta = array();
		$meta['meta_keywords']    = $product->meta_keywords;
		$meta['meta_title']       = $product->meta_title;
		$meta['meta_description'] = $product->meta_description;

		return View::make('usablenet.product')
						->withProduct($product)
						->withProducts($products)
						->withMenus($menus)
						->withPage($page)
						->withMeta($meta)
						->with('caseStudy', $caseStudy)
						->with('caseStudiesOptions', $caseStudiesOptions)
						->with('caseStudies', $caseStudies);
	}

	public function slugThemAll()
	{
		$products = $this->Product->get();
		foreach($products as $product)
		{
			$slug = Str::slug(strip_tags($product->title));
			$iteration = $this->testSlug($slug);
			if($iteration > 0)
			{
				$slug = $slug . '-' . $iteration;
			}

			$product->update(array('slug' => $slug));
		}

	}

	private function testSlug($slug, $i = 0)
	{
		if ($this->Product->whereSlug($slug . '-' . $i)->count() > 0) {
			$j = $i + 1;
			$i = $this->testSlug($slug, $j);
		}

		return $i;
	}

}