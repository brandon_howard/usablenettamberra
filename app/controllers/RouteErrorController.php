<?php

use Tms\ThingmsModel;
use Tms\Page;
use Tms\Menu;
use Tms\Repositories\TemplatesRepository;

class RouteErrorController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Injected Page Modal
     * @var Page
     */
    public $Page;

    /**
     * Injected Menu Modal
     * @var Menu
     */
    public $Menu;

    /**
     * Inject TemplatesRepository
     * @var  TemplatesRepository
     */
    public $TemplatesRepository;

    /**
     * Injected Dealership Model
     * @var [type]
     */
    public $Dealership;

    public function __construct(Page $Page, Menu $Menu, TemplatesRepository $TemplatesRepository)
    {
        $this->Page                = $Page;
        $this->Menu                = $Menu;
        $this->TemplatesRepository = $TemplatesRepository;
    }

    public function fourZeroFour()
    {
        $pages = $this->Page->where('name', '<>', 'Stub')->get();
        $menus = $this->Menu->buildMenus();

        return View::make('404')->withMenus($menus)->withPages($pages);
    }
}