<?php

use Tms\Page;
use Tms\Menu;

class PartnerController extends BaseController {


	protected $Partner;
	protected $Page;
	protected $Menu;

	public function __construct(Partner $Partner, Page $Page, Menu $Menu)
	{
		$this->Partner = $Partner;
		$this->Page = $Page;
		$this->Menu = $Menu;
	}

	public function show($slug)
	{
		if(ctype_digit((string)$slug))
		{
			$partner = $this->Partner->find($slug);
			return Redirect::action('PartnerController@show', urlencode(strtolower($partner->name)));
		}

		$slug = Usablenet::unslug($slug);

		$partner = $this->Partner->where('name', '=', $slug)->first();
		$page = $this->Page->with(array('template'))->whereId(37)->firstOrFail();
		$menus = $this->Menu->buildMenus();

		$meta = array();
		$meta['meta_keywords']    = $partner->meta_keywords;
		$meta['meta_title']       = $partner->meta_title;
		$meta['meta_description'] = $partner->meta_description;

		return View::make('usablenet.partner')
						->withPartner($partner)
						->withPage($page)
						->withMeta($meta)
						->withMenus($menus);
	}

	public function index()
	{
		$partners = $this->Partner->orderBy('name')->get();
		$page = $this->Page->with(array('template'))->whereId(37)->firstOrFail();
		$menus = $this->Menu->buildMenus();

		return View::make('usablenet.partner-list')
						->withPartners($partners)
						->withPage($page)
						->withMenus($menus);
	}

	public function slugThemAll()
	{
		$partners = $this->Partner->get();
		foreach($partners as $partner)
		{
			$slug = Str::slug(strip_tags($partner->name));
			$iteration = $this->testSlug($slug);
			if($iteration > 0)
			{
				$slug = $slug . '-' . $iteration;
			}

			$partner->update(array('slug' => $slug));
		}

	}

	private function testSlug($slug, $i = 0)
	{
		if ($this->Partner->whereSlug($slug . '-' . $i)->count() > 0) {
			$j = $i + 1;
			$i = $this->testSlug($slug, $j);
		}

		return $i;
	}

}