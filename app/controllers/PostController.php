<?php

use Tms\Menu;
use Tms\Page;
use Tms\Video;
use \Carbon\Carbon;
use \HappyR\ExcerptBundle\HappyRExcerptBundle;

/**
 * Class PostController
 */
class PostController extends BaseController
{

    /**
     * @var Post
     */
    protected $Post;
    /**
     * @var Tms\Page
     */
    protected $Page;
    /**
     * @var Tms\Menu
     */
    protected $Menu;
    /**
     * @var Category
     */
    protected $Category;
    /**
     * @var Author
     */
    protected $Author;
    /**
     * @var Product
     */
    protected $Product;
    /**
     * @var Client
     */
    protected $Client;
    /**
     * @var Video
     */
    protected $Video;

    /**
     * @var Tag
     */
    protected $Tag;

    /**
     * @param Post $Post
     * @param Menu $Menu
     * @param Page $Page
     * @param Category $Category
     * @param Author $Author
     * @param Product $Product
     * @param Client $Client
     * @param Video $Video
     */
    public function __construct(
        Post $Post,
        Menu $Menu,
        Page $Page,
        Category $Category,
        Author $Author,
        Product $Product,
        Client $Client,
        Tag $Tag,
        Video $Video
    )
    {
        $this->Post = $Post;
        $this->Page = $Page;
        $this->Menu = $Menu;
        $this->Category = $Category;
        $this->Author = $Author;
        $this->Product = $Product;
        $this->Client = $Client;
        $this->Tag = $Tag;
    }

    /**
     * show news article list
     * @return mixed
     */
    public function index()
    {
        $posts = $this->Post->with('category')->leftJoin('post_tag', 'posts.id', '=', 'post_tag.post_id')->whereNull('post_tag.post_id')->orderBy('posted_on', 'DESC')->paginate(15);
        $page = $this->Page->find(32);
        $menus = $this->Menu->buildMenus();
        $authors = $this->Author->lists('name', 'id');
        $categoryList = $this->Category->lists('name', 'id');
        $categories = $this->Category->get();
        $products = $this->Product->orderBy(DB::raw('RAND()'))->limit(4)->get();
        $clients = $this->Client->orderBy(DB::raw('RAND()'))->limit(4)->get();

        $meta = array();
        $meta['meta_keywords'] = $page->meta_keywords;
        $meta['meta_title'] = $page->meta_title;
        $meta['meta_description'] = $page->meta_description;

        $authors = array('' => 'No Author') + $authors;

        return View::make('usablenet.insights')
            ->withPage($page)
            ->withMeta($meta)
            ->withMenus($menus)
            ->withProducts($products)
            ->withClients($clients)
            ->with('categoryList', $categoryList)
            ->withCategories($categories)
            ->withTitle('Current News')
            ->withAuthors($authors)
            ->withPosts($posts);
    }

    /**
     * show a news article
     * @param $slug
     * @return mixed
     */
    public function show($slug)
    {
        if (ctype_digit((string)$slug)) {
            $post = $this->Post->find($slug);

            return Redirect::action('PostController@show', $post->slug);
        }

        $post = $this->Post->with(array('category', 'author'))->leftJoin('post_tag', 'posts.id', '=', 'post_tag.post_id')->whereNull('post_tag.post_id')->whereSlug($slug)->first();
        $page = $this->Page->find(1);
        $menus = $this->Menu->buildMenus();
        $categoryList = $this->Category->lists('name', 'id');
        $categories = $this->Category->get();
        $authors = $this->Author->lists('name', 'id');

        $authors = array('' => 'No Author') + $authors;

        $meta = array();
        if ( is_null($post) ) {
            return Redirect::to('404');
        } else {
            $meta['meta_keywords'] = $post->meta_keywords;
            $meta['meta_title'] = $post->meta_title;
            $meta['meta_description'] = $post->meta_description;
        }

        return View::make('usablenet.post')
            ->withPage($page)
            ->withMenus($menus)
            ->withMeta($meta)
            ->withAuthors($authors)
            ->with('categoryList', $categoryList)
            ->withCategories($categories)
            ->withPost($post);
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function category($category_id)
    {
        $posts = $this->Post->with('category')->where('category_id', '=', $category_id)->orderBy('posted_on', 'DESC')->paginate(5);

        $page = $this->Page->find(1);
        $menus = $this->Menu->buildMenus();
        $authors = $this->Author->lists('name', 'id');

        $authors = array('' => 'No Author') + $authors;

        return View::make('usablenet.insights')
            ->withPage($page)
            ->withMenus($menus)
            ->withCategories($categories)
            ->with('categoryList', $categoryList)
            ->withAuthors($authors)
            ->withPosts($posts);
    }

    /**
     * @param $year
     * @return mixed
     */
    public function archive($year)
    {
        $backAYear = $year - 1;
        $forwardAYear = $year + 1;

        $posts = $this->Post->with('category')
            ->where('posted_on', '>', date("Y-m-d H:i:s", strtotime('January 1 ' . $backAYear)))
            ->where('posted_on', '<', date("Y-m-d H:i:s", strtotime('January 1 ' . $forwardAYear)))
            ->orderBy('posted_on', 'DESC')
            ->paginate(15);
        $page = $this->Page->find(32);
        $menus = $this->Menu->buildMenus();
        $authors = $this->Author->lists('name', 'id');
        $products = $this->Product->orderBy(DB::raw('RAND()'))->limit(4)->get();
        $clients = $this->Client->orderBy(DB::raw('RAND()'))->limit(4)->get();
        $categoryList = $this->Category->lists('name', 'id');

        if ( $year == date('Y') ) {
            $meta['meta_keywords'] = 'article';
            $meta['meta_title'] = 'Archive '. $year.' | News | Usablenet';
            $meta['meta_description'] = 'Collection of articles and bylines about Usablenet';
        } else {
    	    $meta['meta_keywords'] = $page->meta_keywords;
    	    $meta['meta_title'] = 'Archive '. $year.' | News | Usablenet';
    	    $meta['meta_description'] = 'Read here the articles we published in '.$year.'.';
        }

        return View::make('usablenet.insights')
            ->withPage($page)
            ->withMeta($meta)
            ->withMenus($menus)
            ->withProducts($products)
            ->withClients($clients)
            ->with('categoryList', $categoryList)
            ->withTitle('Archived News From ' . $year)
            ->withAuthors($authors)
            ->withPosts($posts);
    }

    /**
     * @param $version
     * @return \Illuminate\Http\Response
     */
    public function rss($version)
    {
        $feed = Rss::feed($version, 'UTF-8');
        $feed->channel(array('title' => 'Usablenet News', 'description' => 'Know more. Create better mobile strategies.', 'link' => URL::current()));

        $posts = $this->Post->with('category')
            ->orderBy('posted_on', 'DESC')
            ->limit(25)
            ->get();

        foreach ($posts as $post) {
            $feed->item(
                array(
                    'title'             => strip_tags($post->title),
                    'pubDate'           => date('D, d M Y H:i:s O', strtotime($post->created_at)),
                    'description|cdata' => $post->headline,
                    'content|cdata'     => $post->article,
                    'link'              => URL::action('PostController@show', $post->slug))
            );
        }

        return Response::make($feed, 200, array('Content-Type' => 'text/xml'));
    }

    /**
     * @param $version
     * @return \Illuminate\Http\Response
     */
    public function blogRSS($version)
    {
        $feed = Rss::feed($version, 'UTF-8');
        $feed->channel(array('title' => 'Usablenet Blog', 'description' => 'Know more. Create better mobile strategies.', 'link' => URL::current()));

        $posts = $this->Post->with('category')->select('posts.*')->join('blog_categories', function ($join) {
            $join->on('posts.category_id', '=', 'blog_categories.id')->where('blog_categories.blog', '=', 1);
        })->orderBy('posted_on', 'DESC')->limit(25)->get();

        foreach ($posts as $post) {
            $feed->item(
                array(
                    'title'             => strip_tags($post->title),
                    'pubDate'           => date('D, d M Y H:i:s O', strtotime($post->created_at)),
                    'description|cdata' => $post->headline,
                    'content|cdata'     => $post->article,
                    'link'              => URL::action('PostController@show', $post->slug))
            );
        }

        return Response::make($feed, 200, array('Content-Type' => 'text/xml'));
    }

    /**
     * @param $version
     * @return \Illuminate\Http\Response
     */
    public function kcRSS($version)
    {
        $kcFeeds = array(
            'http://knowledgecenter.usablenet.com/usablenet/rss',
            URL::to('/') . '/DB_files/knowledge_center/knowledgecenter.usablenet.com.2015.08.27-16.41.23.xml'
        );

        $feed = Rss::feed($version, 'UTF-8');
        $feed->channel(array('title' => 'Usablenet Knowledge Center', 'description' => 'Know more. Create better mobile strategies.', 'link' => URL::current()));

        $stories = array();

        $storyLimit = Input::get('limit') ?: 0;

        foreach ($kcFeeds as $feedURL) {
            $feed2 = new SimplePie();
            $feed2->set_feed_url($feedURL);

            $feed2->set_cache_location(storage_path() . '/cache');

            $feed2->init();

            foreach ($feed2->get_items() as $item) {
                if ($item->get_content() != "" & isset($stories) & !in_array($item->get_permalink(), $stories)) {
                    if ($storyLimit == 0 || $storyLimit > count($stories)){
                        array_push($stories, $item->get_permalink());
                        $feed->item(
                            array(
                                'title'             => strip_tags($item->get_title()),
                                'pubDate'           => date('D, d M Y H:i:s O', strtotime($item->get_date())),
                                'description|cdata' => $item->get_content(),
                                'link'              => $item->get_permalink())
                            );
                    }
                }
            }
        }

        return Response::make($feed, 200, array('Content-Type' => 'text/xml'));
    }

    /**
     * show a list of blog articles
     * @return mixed
     */
    public function blog()
    {
        $posts = $this->Post->with('category', 'tags')->select('posts.*')->join('blog_categories', function ($join) {
            $join->on('posts.category_id', '=', 'blog_categories.id')->where('blog_categories.blog', '=', 1);
        })->orderBy('posted_on', 'DESC')->paginate(15);
        $page = $this->Page->find(44);
        $menus = $this->Menu->buildMenus();
        $authors = $this->Author->lists('name', 'id');
        $categoryList = $this->Category->where('blog', '=', true)->lists('name', 'id');
        $categories = $this->Category->with(array('posts'=>function($query){
            $query->orderBy('posted_on', 'DESC');
        }))->where('blog', '=', true)->orderBy('name', 'ASC')->get();

        $allTags = $this->Tag->has('posts')->get();

        $authors = array('' => 'No Author') + $authors;

        $meta = array();
        $meta['meta_keywords'] = 'blog';//$post->meta_keywords;
        $meta['meta_title'] = 'Blog | Usablenet';//$post->meta_title;
        $meta['meta_description'] = "Read our blog and get informed about what's new in the mobile ecosystem";//$post->meta_description;

        return View::make('usablenet.knowledgecenter')
            ->withPage($page)
            ->withMenus($menus)
            ->withMeta($meta)
            ->with('categoryList', $categoryList)
            ->withCategories($categories)
            ->withTitle('Current News')
            ->withAuthors($authors)
            ->withPosts($posts)
            ->with('allTags', $allTags);
    }


    /**
     * show a single blog post
     * @param $slug
     * @return mixed
     */
    public function showBlog($slug)
    {
        if (ctype_digit((string)$slug)) {
            $post = $this->Post->find($slug);

            return Redirect::action('PostController@show', $post->slug);
        }

        $post = $this->Post->with(array('category', 'author'))->join('post_tag', 'posts.id', '=', 'post_tag.post_id')->whereSlug($slug)->first();

        $posts = $this->Post->with('category')->select('posts.*')->join('blog_categories', function ($join) {
            $join->on('posts.category_id', '=', 'blog_categories.id')->where('blog_categories.blog', '=', 1);
        })->orderBy('posted_on', 'DESC')->limit(3)->get();

        $page = $this->Page->find(44);
        $menus = $this->Menu->buildMenus();
        $categoryList = $this->Category->where('blog', '=', true)->lists('name', 'id');
        $categories = $this->Category->with(array('posts'=>function($query){
            $query->orderBy('posted_on', 'DESC');
        }))->where('blog', '=', true)->orderBy('name', 'ASC')->get();
        $authors = $this->Author->lists('name', 'id');
	    $allTags = $this->Tag->has('posts')->get();

        $authors = array('' => 'No Author') + $authors;

        $meta = array();
        if ( is_null($post) ) {
            return Redirect::to('404');
        } else {
            $meta['meta_keywords'] = $post->meta_keywords;
            $meta['meta_title'] = $post->meta_title;
            $meta['meta_description'] = $post->meta_description;
        }

        return View::make('usablenet.blogpost')
            ->withPage($page)
            ->withMenus($menus)
            ->withMeta($meta)
            ->withAuthors($authors)
            ->with('categoryList', $categoryList)
            ->withCategories($categories)
            ->with('allTags', $allTags)
            ->withPost($post)
            ->withPosts($posts);
    }

    /**
     * @param $search
     * @param null $category
     * @return mixed
     */
    public function search($search=null, $category = null, $tag = null)
    {
	    if ($search == null) {
		    $search = $_GET['q'];
	    }

        if($search == 'all') {
            $search = null;
        }

        if ($category !== null) {
            $catForSearch = $this->Category->where('name', '=', $category)->first();
        }
        unset ($category);

        if ($tag !== null) {
            $tagForSearch = $this->Tag->where('name', '=', $tag)->first();
        }

        if (isset($catForSearch)) {
            $posts = $this->Post->with('category')
	            ->whereHas('category', function($query) use ($catForSearch){
		            $query->where('blog', 1)->where('id', $catForSearch->id);
	            })
                ->orderBy('posted_on', 'DESC');
        } else if (isset($tagForSearch)) {
            $posts = $this->Post->with('category', 'tags')
	            ->whereHas('tags', function($query) use ($tag){
		            $query->where('name', $tag);
	            })
	            ->whereHas('category', function($query){
		            $query->where('blog', 1);
	            })
                ->orderBy('posted_on', 'DESC');

        } else {
            $posts = $this->Post->with('category')
	            ->whereHas('category', function($query){
		            $query->where('blog', 1);
	            })
                ->orderBy('posted_on', 'DESC');
        }

        if ($search !== null) {
            $posts = $posts->
	            where(function($query) use ($search) {
		            $query->where('title', 'LIKE', '%' . $search . '%')
			        ->orWhere('article', 'LIKE', '%' . $search . '%');
	            });
        }

        $posts = $posts->paginate(15);
        $page = $this->Page->find(44);
        $menus = $this->Menu->buildMenus();
        $authors = $this->Author->lists('name', 'id');
        $categoryList = $this->Category->where('blog', '=', true)->lists('name', 'id');
        $categories = $this->Category->with(array('posts'=>function($query){
            $query->orderBy('posted_on', 'DESC');
        }))->where('blog', '=', true)->orderBy('name', 'ASC')->get();
        $allTags = $this->Tag->has('posts')->get();

        $authors = array('' => 'No Author') + $authors;

        return View::make('usablenet.knowledgecenter')
            ->withPage($page)
            ->withMenus($menus)
            ->with('categoryList', $categoryList)
            ->withSearch($search)
            ->withCategories($categories)
            ->withTitle('Current News')
            ->withAuthors($authors)
            ->withPosts($posts)
            ->with('allTags', $allTags);
    }

    /**
     *
     */
    public function slugThemAll()
    {
        $posts = $this->Post->where('slug', '=', null)->get();
        foreach ($posts as $post) {
            $slug = Str::slug(strip_tags($post->title));
            $iteration = $this->testSlug($slug);
            if ($iteration > 0) {
                $slug = $slug . '-' . $iteration;
            }

            $post->update(array('slug' => $slug));
        }

    }

    /**
     * @param $slug
     * @param int $i
     * @return int
     */
    private function testSlug($slug, $i = 0)
    {
        if ($this->Post->whereSlug($slug . '-' . $i)->count() > 0) {
            $j = $i + 1;
            $i = $this->testSlug($slug, $j);
        }

        return $i;
    }

}