<?php

use Tms\ThingmsModel;

class Post extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new car
	 * @var array
	 */
	public $storeRules = array(
		'slug'  => 'required|unique:tms_pages,slug',
	);

	/**
	 * Validation rules when updating a car
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation rules when updating a car
	 * @var array
	 */
	public $updateFieldsRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
		'slug' => 'Your page must have a unique slug and cannot be empty.',
	);

    public static function boot()
    {
        parent::boot();

        Post::saving(function($post)
        {
            if (isset($post->tags)) {

                $tagIds = array();
                $tags = explode(',', $post->tags);
                $post->updated_at = date('Y-m-d H:i:s');

                foreach($tags as $t) {
                    if (trim($t) !== '') {
                        $tag = Tag::firstOrNew(array('name' => trim($t)));
                        $tag->save();
                        $tagIds[] = $tag->id;
                    }
                }

                Config::set('post.tagids', $tagIds);
                unset($post->tags);
            }
        });

        Post::saved(function($post)
        {
            $tagIds = Config::get('post.tagids');

            if($tagIds) {
                $post->tags()->sync($tagIds);
            }
        });
    }

	public function category(){
		return $this->belongsTo('Category');
	}

	public function author(){
		return $this->belongsTo('Author');
	}

    public function tags(){
        return $this->belongsToMany('Tag');
    }

	public function getMainImageAttribute($value)
	{
		return $this->getImageAttr($value);
	}

	public function getImageThumbAttribute($value)
	{
		return $this->getImageAttr($value);
	}

}