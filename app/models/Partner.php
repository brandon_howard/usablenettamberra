<?php

use Tms\ThingmsModel;

class Partner extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'partners';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new car
	 * @var array
	 */
	public $storeRules = array(
	);

	/**
	 * Validation rules when updating a car
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation rules when updating a car
	 * @var array
	 */
	public $updateFieldsRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(

	);

	public function getLogoAttribute($value)
	{
		return $this->getImageAttr($value);
	}

}