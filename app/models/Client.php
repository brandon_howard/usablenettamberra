<?php

use Tms\ThingmsModel;

class Client extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'case_studies';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation rules when updating a car
	 * @var array
	 */
	public $updateFieldsRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
	);

	public function getImage1Attribute($value)
	{
		return $this->getImageAttr($value);
	}

	public function getImage2Attribute($value)
	{
		return $this->getImageAttr($value);
	}

	public function getImage3Attribute($value)
	{
		return $this->getImageAttr($value);
	}

	public function getLargeLogoAttribute($value)
	{
		return $this->getImageAttr($value);
	}

	public function getSmallLogoAttribute($value)
	{
		return $this->getImageAttr($value);
	}

	public function getSectionOneLinkOneAttribute($value)
	{
		return json_decode($value);
	}

	public function getSectionOneLinkTwoAttribute($value)
	{
		return json_decode($value);
	}

	public function getSectionOneLinkThreeAttribute($value)
	{
		return json_decode($value);
	}

	public function getSectionOneLinkFourAttribute($value)
	{
		return json_decode($value);
	}

	public function getMainColumnVideoImageAttribute($value)
	{
		return $this->getImageAttr($value);
	}

}