<?php

use Tms\ThingmsModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Product extends ThingmsModel {

	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation rules when updating a car
	 * @var array
	 */
	public $updateFieldsRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
	);

	public function caseStudies(){
		return $this->belongsTo('Client');
	}

	public function getSideColumnVideoImageAttribute($value)
	{
		return $this->getImageAttr($value);
	}

	public function getMainColumnVideoImageAttribute($value)
	{
		return $this->getImageAttr($value);
	}

}