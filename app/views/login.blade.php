<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Usablenet Login</title>

	<link rel="stylesheet" href="{{ URL::asset('css/login.min.css') }}">

	<style>

		body {
			padding-top: 40px;
			padding-bottom: 40px;
			background-color: #eee;
			text-align: center;
		}

		h3 {
			text-transform: uppercase;
			color:#4c4c4c;
			font-size:14px;
			text-align: left;
		}

		.form-signin {
			max-width: 330px;
			padding: 15px;
			margin: 0 auto;
		}
		.form-signin .form-signin-heading,
		.form-signin .checkbox {
			margin-bottom: 10px;
		}
		.form-signin .checkbox {
			font-weight: normal;
		}
		.form-signin .form-control {
			position: relative;
			font-size: 16px;
			height: auto;
			padding: 10px;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}
		.form-signin .form-control:focus {
			z-index: 2;
		}
		.form-signin input[type="text"] {
			margin-bottom: -1px;
			border-bottom-left-radius: 0;
			border-bottom-right-radius: 0;
		}
		.form-signin input[type="password"] {
			margin-bottom: 10px;
			border-top-left-radius: 0;
			border-top-right-radius: 0;
		}
	</style>

</head>

<div class="container">

	<img src="{{ URL::asset('images/usablenet-header-logo.png') }}">

	{{ Form::open([
		"route"        => "loginAction",
		"autocomplete" => "off",
		"role"         => "form",
		'class'        => "form-signin"
		]) }}
		<h3 class="form-signin-heading">Please sign in</h3>
		<input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
		<input type="password" name="password" class="form-control" placeholder="Password" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	</form>

</div> <!-- /container -->

</body>
</html>