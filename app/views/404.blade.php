@extends('layouts.404')

@section('main')
	<section class="section full-width" style="height:935px;">
		<div class="container">
			<p style="font-size:1rem; margin: 0 0 0.2rem">404 PAGE NOT FOUND</p>
			<h2 style="font-size:2rem;margin:0">Sorry, this page isn't available</h2>
			<div style="margin-top:3rem">
				<a href="{{ URL::to('/') }}" ><img src="{{ URL::asset('images/404-home.png') }}" alt="Go to Home Page"></a>
			</div>
		</div> <!-- /container -->
	</section>
@stop
