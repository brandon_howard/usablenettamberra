
	<div class="tms-section-title">Create New Page</div>
	{{ Form::open(array('route' => 'store-page', 'id'=>'tms-page-manager-form')) }}
		{{ Form::label('Template') }}
		{{ Form::select('template_id', $templates, null, array('class'=>'chosen-selector')) }}
		<div class="tms-two-column">
			<div>
				{{ Form::label('Name') }}
				{{ Form::text('name') }}
			</div>
			<div>
				{{ Form::label('Slug') }}
				{{ Form::text('slug') }}
			</div>
			<div>
				{{ Form::label('Meta Title') }}
				{{ Form::text('meta_title') }}
			</div>
		</div>

		{{ Form::label('Meta Description') }}
		{{ Form::textarea('meta_description') }}
		{{ Form::label('Meta Keywords') }}
		{{ Form::textarea('meta_keywords') }}
		{{ Form::label('Extra &lt;head&gt; Code') }}
		{{ Form::textarea('head') }}
		{{ Form::label('Extra Footer Code') }}
		{{ Form::textarea('footer') }}
		<hr>
		{{ Form::select('published', array(0 => 'Not Published', 1 => 'Published'), null, array('class'=>'chosen-selector')) }}
		<div class="tms-actions">
			<button type="button" class="tms-btn tms-fr tms-admin" data-tms-admin='{"action":"cancel-slider"}' id="tms-cancel-edit">Cancel</button>
			<button type="button" class="tms-btn-success tms-fr tms-mr tms-admin" data-tms-admin='{"action":"update-page", "pageManagerUrl":"{{ URL::action('PageController@pageManager') }}"}' id="tms-save-edit">Save Changes</button>
		</div>

	{{ Form::close() }}