
	<div class="tms-section-title">Edit Menu</div>

		<button type="button" class="tms-btn-info tms-fr tms-ml tms-admin" data-tms-admin='{"action":"create-menu-item-break","contentUrl":"{{ URL::action('MenuItemController@storeBreak', $menu->id) }}","menuUrl":"{{URL::action('MenuController@edit', $menu->id)}}", "tier":"2"}'>Create Break</button>

		<button type="button" class="tms-btn-info tms-fr tms-ml tms-admin" data-tms-admin='{"action":"create-menu-item","contentUrl":"{{ URL::route('create-menu-item', $menu->id) }}", "menuUrl":"{{URL::action('MenuController@edit', $menu->id)}}", "tier":"2"}'>Add New</button>

	{{ Form::model($menu, array('action' => array('MenuController@update', $menu->id), 'method'=>'PUT', 'id'=>'tms-menu-manager-form')) }}
		<div>
			{{ Form::label('name', 'Menu Name') }}
			{{ Form::text('name') }}
		</div>
		<hr>
		<ul id="tms-menu-items-sortable" class="tms-item-list">
			@foreach($menu->menuItems as $menuItem)
				<li id="menuitem_{{$menuItem->id}}" class="cursor<?php if($menuItem->label == "(Break)") echo ' tms-break' ?>">
					<div>
					<?php if($menuItem->label !== "(Break)") echo $menuItem->label; else echo "&nbsp;"; ?>
					<button type="button" class="tms-btn tms-fr tms-ml tms-admin" data-tms-admin='{"action":"destroy-menu-item","contentUrl":"{{ URL::action('MenuItemController@destroy', $menuItem->id) }}", "data": {"_method":"DELETE"}, "method":"POST", "menuUrl":"{{URL::action('MenuController@edit', $menu->id)}}", "tier":"2"}'>Remove</button>
				<?php if($menuItem->label != "(Break)") { ?>
					<button type="button" class="tms-btn tms-fr tms-ml tms-admin" data-tms-admin='{"action":"edit-menu-item","contentUrl":"{{ URL::action('MenuItemController@edit', $menuItem->id) }}", "tier":"2"}'>Edit</button>
				<?php } ?>
					</div>
					<ul>

					@foreach($menuItem->children as $childItem)
						<li id="menuitem_{{$childItem->id}}" class="cursor<?php if($childItem->label == "(Break)") echo ' tms-break' ?>">
							<div>
							<?php if($childItem->label !== "(Break)") echo $childItem->label; else echo "&nbsp;"; ?>
							<button type="button" class="tms-btn tms-fr tms-ml tms-admin" data-tms-admin='{"action":"destroy-menu-item","contentUrl":"{{ URL::action('MenuItemController@destroy', $childItem->id) }}", "data": {"_method":"DELETE"}, "method":"POST", "menuUrl":"{{URL::action('MenuItemController@edit', $menu->id)}}", "tier":"2"}'>Remove</button>
						<?php if($childItem->label != "(Break)") { ?>
							<button type="button" class="tms-btn tms-fr tms-ml tms-admin" data-tms-admin='{"action":"edit-menu-item","contentUrl":"{{ URL::action('MenuItemController@edit', $childItem->id) }}", "tier":"2"}'>Edit</button>
						<?php } ?>
							</div>
						</li>
					@endforeach

					</ul>
				</li>
			@endforeach
		</ul>

		<div class="tms-actions">
			<button type="button" class="tms-btn tms-fr tms-admin" data-tms-admin='{"action":"cancel-slider"}' id="tms-cancel-edit">Cancel</button>
			<button type="button" class="tms-btn-success tms-fr tms-mr tms-admin" data-tms-admin='{"action":"update-menu"}' id="tms-edit-menu">Save Changes</button>
		</div>

	{{ Form::close() }}

	<script type="application/javascript">
			$( "#tms-menu-items-sortable" ).nestedSortable(
					{
						handle:           'div',
						items:            'li',
						toleranceElement: '> div'
					}
				);
			$( "#tms-menu-items-sortable" ).disableSelection();
	</script>