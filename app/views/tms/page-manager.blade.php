		<div id="tms-manager-wrapper">
			<div id="tms-admin-tabs">
				<div class="tms-admin" data-tms-admin='{"action":"change-tabs"}' data-tms-tab="tms-page-manager">Pages</div>
				<div class="tms-admin" data-tms-admin='{"action":"change-tabs"}' data-tms-tab="tms-menu-manager">Menus</div>
			</div>

			<div id="tms-page-manager" class="tms-tab active">
				<div class="tms-manager-head">
					<img src="{{ URL::asset('img/tms/admin_logo.png') }}" width="115" height="31">
					<button class="tms-btn tms-fr tms-admin" data-tms-admin='{"action":"close-manager"}'>Close</button>
				</div>

				<div class="tms-manager-body">
					<ul class="tms-page-manager-nav tms-list-unstyled">
						<li class="tms-tar">
							<div class="tms-fl tms-section-title">Page Manager</div>
							<button type="button" class="tms-btn-success tms-admin" data-tms-admin='{"action":"new-page","contentUrl":"{{ URL::action('PageController@create') }}"}'>New Page</button>
						</li>
					</ul>
					<hr>

					<ul class="tms-item-list">
						@foreach($pages as $page)
						<li>
							<div>
								{{ $page->name }}
								<button class="tms-btn tms-fr tms-ml tms-admin" data-tms-admin='{"action":"edit-page","contentUrl":"{{ URL::action('PageController@edit', $page->id) }}"}'>Edit</button>
								<button class="tms-btn tms-fr tms-admin" data-tms-admin='{"action":"view-page","contentUrl":"{{URL::to($page->encodedSlug)}}"}'>View</button>
							</div>
						</li>
						@endforeach
					</ul>
				</div>
			</div>

			<div id="tms-menu-manager" class="tms-tab">
				<div class="tms-manager-head">
					<img src="{{ URL::asset('img/tms/admin_logo.png') }}" width="115" height="31">
					<button class="tms-btn tms-fr tms-admin" data-tms-admin='{"action":"close-manager"}'>Close</button>
				</div>

				<div class="tms-manager-body">
					<ul class="tms-page-manager-nav tms-list-unstyled">
						<li class="tms-tar">
							<div class="tms-fl tms-section-title">Menu Manager</div>
						</li>
					</ul>
					<hr>
					<ul class="tms-item-list">
						@foreach($menus as $menuName => $menu)
						<li>
							<div>
								{{ $menuName }}
								<button class="tms-btn tms-fr tms-ml tms-admin" data-tms-admin='{"action":"edit-menu","contentUrl":"{{ URL::action('MenuController@edit', $menu->id) }}"}'>Edit</button>
							</div>
						</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
