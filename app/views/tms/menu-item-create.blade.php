
	<div class="tms-section-title">Create Menu Item</div>
	{{ Form::open(array('action' => array('MenuItemController@store'), 'method'=>'POST', 'id'=>'tms-menu-item-manager-form')) }}
		<div>
			{{ Form::hidden('menu_id', $menu_id) }}

			{{ Form::label('label', 'Menu Item Label') }}
			{{ Form::text('label') }}

			{{ Form::label('Page Link Type') }}
			{{ Form::select('type', array('page_link' => 'Page Link', 'url' => 'URL'), null, array('class'=>'chosen-selector', 'id'=>'tms-page-link-select')) }}

			<hr>

			<div id="tms-page-link" class="tms-link-type active">
				{{ Form::label('page_id', 'Select a Page to Link to') }}
				{{ Form::select('type', $pages, null, array('class'=>'chosen-selector')) }}
			</div>
			<div id="tms-url-link" class="tms-link-type">
				{{ Form::label('url', 'Fully qualified URL to link to') }}
				{{ Form::text('url') }}
			</div>

		</div>
		<div class="tms-actions">
			<button type="button" class="tms-btn tms-fr tms-admin" data-tms-admin='{"action":"cancel-slider", "tier":"2"}' id="tms-cancel-edit">Cancel</button>
			<button type="button" class="tms-btn-success tms-fr tms-mr tms-admin" data-tms-admin='{"action":"store-menu-item", "menuUrl":"{{URL::action('MenuController@edit', $menu_id)}}", "tier":"2"}' id="tms-edit-menu">Save Changes</button>
		</div>

	{{ Form::close() }}

	<script>
		$(function() {
			$( "#tms-page-link-select" ).change(function(){
				if ($(this).val() === 'page_link')
				{
					$('.tms-link-type').removeClass('active');
					$('#tms-page-link').addClass('active');
				}
				else
				{
					$('.tms-link-type').removeClass('active');
					$('#tms-url-link').addClass('active');
				}
			});

			$('#tms-url-link').keydown(function(){
				$('#tms-page-link').val(0);
			});
		});
	</script>