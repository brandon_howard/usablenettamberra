@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')

	<section class="section full-width breadcrumbs">
		<div class="container">
			<div class="row-fluid">
				<div class="span2 section-name">
					<p>Usablenet.com</p>
				</div>
				<div class="span10 crumbs mz">
					<ul class="mz">
						{{ Usablenet::buildcrumbs(URL::current()) }}
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="section full-width">
		<div class="container">
			<div class="row-fluid">
				<div class="span12 span-dt-10 span-tab-10 mb">
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<button class="tms-editor tms-btn-success mb btn btn-success h20" {{ $page->editor('main_body', '#main_body', 'main_body') }}>Edit This Page</button>
					<?php } ?>

					<div id="main_body">{{ $page->main_body }}</div>

				</div>
			</div>
		</div>
	</section>
@stop