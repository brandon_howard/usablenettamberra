<div class="inline-anchor"><div id="ux-form"></div></div>

{{ Form::open(array('action' => 'ContactController@email')) }}
						{{Form::hidden('program_name', 'Website UX Review Request')}}
						{{Form::hidden('from', 'ux-form')}}

@if(Session::has('message-fail-ux-form'))
	<h4 class="text-error title mb h20">{{ Session::get('message-fail-ux-form') }}</h4>
@elseif(Session::has('message-success-ux-form'))
	<h3 class="title mb h20" id="contact">Thank You. We will contact you shortly.</h3>
@endif

<div class="row-fluid">

	<div class="span6">
     <label for="FirstName" style="color: #ffffff;">First Name</label>
		{{ Form::text('FirstName', null, array('class' => 'input-block-level', 'placeholder' => '')) }}
		{{ $errors->first('FirstName'); }}
	</div>

	<div class="span6">
     <label for="LastName" style="color: #ffffff;">Last Name</label>
		{{ Form::text('LastName', null, array('class' => 'input-block-level', 'placeholder' => '')) }}
		{{ $errors->first('LastName'); }}
	</div>
</div>

<div class="row-fluid mt h20">
	<div class="span6">
     <label for="Email" style="color: #ffffff;">Email</label>
		{{ Form::text('Email', null, array('class' => 'input-block-level', 'placeholder' => '')) }}
		{{ $errors->first('Email'); }}
	</div>

	<div class="span6">
     <label for="Phone" style="color: #ffffff;">Phone</label>
		{{ Form::text('Phone', null, array('class' => 'input-block-level', 'placeholder' => '')) }}
	</div>
</div>

<div class="row-fluid mt h20">
	<div class="span6">
     <label for="Company" style="color: #ffffff;">Company</label>
		{{ Form::text('Company', null, array('class' => 'input-block-level', 'placeholder' => '')) }}
		{{ $errors->first('Company'); }}
	</div>
</div>

<div class="row-fluid">
	<div class="span12 text-right">
		<button type="submit" class="slider-button mainHeader" style="text-align: center; font-size: 16px; width: 170px;">Contact Us</button>
	</div>
</div>
{{ Form::close() }}