@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')

	<section class="section full-width page-title-slice" style="background-color:{{$page->title_background_color}};background-image:url({{ URL::asset($page->title_background->src) }});color:{{ $page->title_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">
					<h1 class="sub-title light-gray mbz tms-editor tms-inline" {{ $page->editor('title_subtitle', 'inline') }}>{{ $page->title_subtitle }}</h1>
					<h2 class="title tms-editor tms-inline" {{ $page->editor('title_title', 'inline') }}>{{ $page->title_title }}</h2>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background', '.page-title-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_text_color', '.page-title-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background_color', '.page-title-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	@include('usablenet.contact-form-paragraph')

<section class="section full-width">
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<h3 style="text-align: center;font-size: 42px;" class="tms-editor tms-inline">Our Offices</h3>
				</div>
			</div>
			<div class="row-fluid">
				<div class="top-offices">
				<div class="location one-fifth">
					<img src="/images/office-london.png" alt="London building" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;
">
						<h4 style="
    text-transform: uppercase; font-size: 21px;     font-weight: 500; margin: auto;
    margin-bottom: -20px;
" class="fs18 tms-editor tms-inline">LONDON</h4><br>
						11A Curtain Road, London, UK EC2A 3LT
												<br>
						<a class="location-address-link" href="https://www.google.com/maps/place/Curtain+Rd,+London+EC2A+3LT,+UK/@51.5225496,-0.0832159,17z/data=!3m1!4b1!4m2!3m1!1s0x48761cb1cbff9755:0xce41dcb6fe157767?hl=en" style="
    font-weight: 700;
">Map It</a><br>
																		+44.20.3617.3200
										</div>
				</div>
				<div class="location one-fifth">
					<img src="/images/office-losangeles.png" alt="Los Angeles downtown" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;
">
						<h4 style="
    text-transform: uppercase; font-size: 21px;     font-weight: 500; 
    margin: auto;
    margin-bottom: -20px;
" class="fs18 tms-editor tms-inline">LOS ANGELES</h4><br>
						1546 7th Street, Suite 300 Santa Monica, CA 90401
												<br>
						<a class="location-address-link" href="https://www.google.com/maps/place/1546+7th+St+%23300,+Santa+Monica,+CA+90401/@34.0161505,-118.4920906,17z/data=!3m1!4b1!4m2!3m1!1s0x80c2a4d27a33be11:0xf4be0f70a1fdd50?hl=en" style="
    font-weight: 700;
">Map It</a><br>
																		+1.310.260.9900
																	</div>
				</div>
				<div class="location one-fifth">
					<img src="/images/office-newyork.png" alt="New York bridge" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;
">
						<h4 style="
    text-transform: uppercase; font-size: 21px;     font-weight: 500; 
    margin: auto;
    margin-bottom: -20px;
" class="fs18 tms-editor tms-inline">NEW YORK</h4><br>
						142 W 57th Street, 7th Floor New York, NY 10019
												<br>
						<a class="location-address-link" href="https://www.google.com/maps/place/142+W+57th+St,+New+York,+NY+10019/@40.7648242,-73.9814178,17z/data=!3m1!4b1!4m2!3m1!1s0x89c258f765a3c243:0x2dbeea963c561d09?hl=en" style="
    font-weight: 700;
">Map It</a><br>
																		+1.212.965.5388
																	</div>
				</div>
					</div>

			</div>
		</div></section>

@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop