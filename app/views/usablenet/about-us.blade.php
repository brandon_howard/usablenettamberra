@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')
	<section class="section full-width page-title-slice" tabindex="0" style="background-color:{{$page->title_background_color}};background-image:url({{ URL::asset($page->title_background->src) }});color:{{ $page->title_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">
					<h1 class="sub-title tms-editor light-gray mbz tms-inline" {{ $page->editor('title_subtitle', 'inline') }}>{{ $page->title_subtitle }}</h1>
					<h2 class="title tms-editor tms-inline" {{ $page->editor('title_title', 'inline') }}>{{ $page->title_title }}</h2>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background', '.page-title-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_text_color', '.page-title-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background_color', '.page-title-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width" tabindex="0">
		<div class="container">
			<div class="row-fluid">
				<div class="span7">
					@if($page->two_column_subtitle !== '' || TmsAuth::isInGroup('Administration'))
					<h3 class="title tms-editor tms-inline" {{ $page->editor('two_column_subtitle', 'inline') }}>{{ $page->two_column_subtitle }}</h3>
					@endif
					@if($page->two_column_title !== '' || TmsAuth::isInGroup('Administration'))
					<h3 class="fs24 lh15 tms-editor tms-inline" {{ $page->editor('two_column_title', 'inline') }}>{{ $page->two_column_title }}</h3>
					@endif
				</div>
			</div>
			<div class="row-fluid mt h40">
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_1_title', 'inline') }}><strong>{{ $page->two_column_column_1_title }}</strong></div>
				</div>
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_title', 'inline') }}><strong>{{ $page->two_column_column_2_title }}</strong></div>
				</div>
			</div>
			<div class="row-fluid mt">
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_1_description', 'inline') }}>{{ $page->two_column_column_1_description }}</div>
				</div>
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_description', 'inline') }}>{{ $page->two_column_column_2_description }}</div>
				</div>
			</div>
		</div>
	</section>

	<section tabindex="0" class="section full-width page-three_column-slice"  style="background-color:{{$page->three_column_background_color}};color:{{ $page->three_column_text_color }}">
		<div class="container color white mb">
			<div class="row-fluid mt h40">
				<div class="span12">
					<h3 class="tms-editor title tms-inline" {{ $page->editor('three_column_title', 'inline') }}>{{ $page->three_column_title }}</h3>
				</div>
			</div>
			<div class="row-fluid mt h10">
				<div class="span4">
					<div class="tms-editor tms-inline" {{ $page->editor('three_column_column_1_description', 'inline') }}>{{ $page->three_column_column_1_description }}
					</div>
				</div>
				<div class="span4">
					<div class="tms-editor tms-inline" {{ $page->editor('three_column_column_2_description', 'inline') }}>{{ $page->three_column_column_2_description }}
					</div>
				</div>
				<div class="span4">
					<div class="tms-editor tms-inline" {{ $page->editor('three_column_column_3_description', 'inline') }}>{{ $page->three_column_column_3_description }}
					</div>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('three_column_background', '.page-three_column-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('three_column_text_color', '.page-three_column-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('three_column_background_color', '.page-three_column-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>


	</section>

@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
		<div class="modal-body">
			<video id="video-player" autoplay="true" controls="true" preload="none" width="100%">
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video>
		</div>
	</div>
@stop
