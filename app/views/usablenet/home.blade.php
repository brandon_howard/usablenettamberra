@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')
	<!--<section class="section full-width text-center main-hero">
		<ul class="mz pz">
			<li style="background-color:{{$page->homehero_background_1}};" class="homehero_background_1">
				<a href="{{ $page->homehero_link_1->href }}" class="homehero_link_4"><img src="{{ $page->homehero_image_1->src }}" alt="{{ $page->homehero_image_1->alt }}" class="homehero_image_1 responsive" width="1481" height="450"></a>
			</li>
		</ul>
	</section>
	<section class="section full-width text-center sub-heros" style="padding: inherit;">
		<div class="half left">
			<a href="{{ $page->homehero_link_2->href }}" class="homehero_link_4"><img src="{{ $page->homehero_image_2->src }}" alt="{{ $page->homehero_image_2->alt }}" class="homehero_image_2 responsive" width="1481" height="450"></a>
		</div>
		<div class="half right">
			<a href="{{ $page->homehero_link_4->href }}" class="homehero_link_4"><img src="{{ $page->homehero_image_4->src }}" alt="{{ $page->homehero_image_4->alt }}" class="homehero_image_4 responsive" width="1481" height="450"></a>
		</div>
	</section>-->
	<style>
	.headerText{color:#fff;line-height:56px;font-family:'Gotham A', 'Gotham B';font-style:normal;font-weight:300;font-size:42px;padding-top:18%;     max-width: 1200px;     margin: auto;}

	</style>



	<section class="section full-width text-center main-hero">
		<ul class="mz pz">
			<li style="background-color:{{$page->homehero_background_1}};background-image:url('{{ $page->homehero_image_1->src }}');height:500px; position: relative; background-position: center; background-repeat: no-repeat;" class="homehero_background_1 homehero_image_1 responsive">
				<div class="headerText mainHeader"><h1>{{ $page->homehero_label_1 }}</h1></div>
				<a class="slider-button mainHeader" href="{{ $page->homehero_link_1->href }}" style=" position: absolute;
    bottom: 18%; left: 40%; text-align: center; font-size: 16px; width: 245px; padding-left: 20px;">{{ $page->homehero_link_1->fieldLabel }}</a>
			</li>
		</ul> 
	</section> 
	<section class="section full-width text-center sub-heros" style="padding: inherit; margin-top: -40px;">
		<div class="half left" style="position: relative;">
			<div class="homehero_link_4"><img src="{{ $page->homehero_image_2->src }}" alt="{{ $page->homehero_image_2->alt }}" class="homehero_image_2 responsive" width="1481" height="450"></div>
			<div class="headerText subHeader" style="
    position: absolute;
    width: 390px;
    left: 20%;
    font-size: 34px;
    text-align: left;
	line-height: 45px;
										   font-weight: 300;
"><h2>{{ $page->homehero_label_2 }}</h2></div>
			<a class="slider-button subHeader-btn-left" href="{{ $page->homehero_link_2->href }}" style=" position: absolute;
    bottom: 18%; left: 20%; text-align: center; font-size: 16px;">{{ $page->homehero_link_2->fieldLabel }}</a>
		</div>
		<div class="half right" style="position: relative;">
			<div class="homehero_link_4"><img src="{{ $page->homehero_image_3->src }}" alt="{{ $page->homehero_image_3->alt }}" class="homehero_image_2 responsive" width="1481" height="450"></div>
			<div class="headerText subHeader" style="
    position: absolute;
    width: 412px;
    left: 5%;
    font-size: 34px;
    text-align: left;
	line-height: 45px;
										   font-weight: 300;
"><h2>{{ $page->homehero_label_3 }}</h2></div>
			<a class="slider-button subHeader-btn-right" href="{{ $page->homehero_link_3->href }}" style=" position: absolute;
    bottom: 18%; left: 5%; text-align: center; font-size: 16px; width: 160px; padding-left: 25px;">{{ $page->homehero_link_3->fieldLabel }}</a>
		</div>
	</section>


	<?php if(TmsAuth::isInGroup('Administration')) { ?>
	<section class="section">
		<div class="container">

			<span class="tms-editor" {{ $page->editor('homehero_label_1', '#two_column_column2_link', 'homehero_1') }}></span>
			<span class="tms-editor" {{ $page->editor('homehero_link_1', '.homehero_link_1', 'homehero_1') }}></span>
			<span class="tms-editor" {{ $page->editor('homehero_background_1', '.homehero_background_1', 'homehero_1', array('updateAttribute'=>'style[background-color]')) }}></span>
			<button class="tms-editor tms-btn-success" {{ $page->editor('homehero_image_1', '.homehero_image_1', 'homehero_1', array('updateAttribute' => 'src')) }}>Manage Hero 1</button>

			<span class="tms-editor" {{ $page->editor('homehero_label_2', '.homehero_label_2', 'homehero_2') }}></span>
			<span class="tms-editor" {{ $page->editor('homehero_link_2', '.homehero_link_2', 'homehero_2') }}></span>
			<span class="tms-editor" {{ $page->editor('homehero_background_2', '.homehero_background_2', 'homehero_2', array('updateAttribute'=>'style[background-color]')) }}></span>
			<button class="tms-editor tms-btn-success" {{ $page->editor('homehero_image_2', '.homehero_image_2', 'homehero_2', array('updateAttribute' => 'src')) }}>Manage Hero 2</button>

			<span class="tms-editor" {{ $page->editor('homehero_label_3', '.homehero_label_3', 'homehero_3') }}></span>
			<span class="tms-editor" {{ $page->editor('homehero_link_3', '.homehero_link_3', 'homehero_3') }}></span>
			<span class="tms-editor" {{ $page->editor('homehero_background_3', '.homehero_background_3', 'homehero_3', array('updateAttribute'=>'style[background-color]')) }}></span>
			<button class="tms-editor tms-btn-success" {{ $page->editor('homehero_image_3', '.homehero_image_3', 'homehero_3', array('updateAttribute' => 'src')) }}>Manage Hero 3</button>
			
		</div>
	</section>
	<?php } ?>

	<section class="section full-width" style="
    margin-top: 50px;
    margin-bottom: -50px;
">
		<div class="container">
			<div class="row-fluid">
				<div class="span6 icons-item">
					<a class="tms-editor tms-inline" {{ $page->editor('home_icon_1', 'inline') }}><img src="{{ $page->home_icon_1->src }}" alt="multiple users icon"></a>
					<h3 style="
    
    font-weight: normal;
    font-style: normal;
							   "><a href="/capabilities">{{ $page->home_icon_label_1 }}</a></h3>
					<p style="
    
    font-weight: normal;
    font-style: bold;
							  "><a href="/capabilities" style="
    color: initial;
">{{ $page->home_icon_description_1 }}</a><br><br><a href="/capabilities" style="
    
    font-weight: bold;
    font-style: normal;
	color: #0276CE;
">Learn More <img src="{{ URL::to('img/uploads/ICN_ Arrow.png')}}" alt="arrow icon"></a></p>
					
	<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_label_1', 'home_icon_description_1', 'home_icon_label_1') }}> Edit Icon Label 1</button>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_description_1', 'home_icon_description_1', 'home_icon_description_1') }}> Edit Icon Description 1</button>
	<?php } ?>
					
				</div>
				<div class="span6 icons-item">
					<a class="tms-editor tms-inline" {{ $page->editor('home_icon_2', 'inline') }}><img src="{{ $page->home_icon_2->src }}" alt="hand clicking icon"></a>
					<h3 style="
    
    font-weight: normal;
    font-style: normal;
							   "><a href="/capabilities">{{ $page->home_icon_label_2 }}</a></h3>
					<p style="
    
    font-weight: normal;
    font-style: bold;
							  "><a href="/capabilities" style="
    color: initial;
">{{ $page->home_icon_description_2 }}</a><br><br><a href="/capabilities"  style="
    
    font-weight: bold;
    font-style: normal;
	color: #0276CE;
">Learn More <img src="{{ URL::to('img/uploads/ICN_ Arrow.png')}}" alt="arrow icon"></a></p>
					
	<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_label_2', 'home_icon_label_2', 'home_icon_label_2') }}> Edit Icon Label 2</button>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_description_2', 'home_icon_description_2', 'home_icon_description_2') }}> Edit Icon Description 2</button>
	<?php } ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 icons-item">
					<a class="tms-editor tms-inline" {{ $page->editor('home_icon_3', 'inline') }}><img src="{{ $page->home_icon_3->src }}" alt="link icon"></a>
					<h3 style="
    
    font-weight: normal;
    font-style: normal;
"><a href="/capabilities" >{{ $page->home_icon_label_3 }}</a></h3>
					<p style="
    
    font-weight: normal;
    font-style: bold;
"><a href="/capabilities" style="
    color: initial;
						">{{ $page->home_icon_description_3 }}</a><br><br><a href="/capabilities"  style="
    
    font-weight: bold;
    font-style: normal;
	color: #0276CE;
">Learn More <img src="{{ URL::to('img/uploads/ICN_ Arrow.png')}}" alt="arrow icon"></a></p>
					
	<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_label_3', 'home_icon_description_3', 'home_icon_label_3') }}> Edit Icon Label 3</button>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_description_3', 'home_icon_description_3', 'home_icon_description_3') }}> Edit Icon Description 3</button>
	<?php } ?>
				</div>
				<div class="span6 icons-item">
					<a class="tms-editor tms-inline" {{ $page->editor('home_icon_4', 'inline') }}><img src="{{ $page->home_icon_4->src }}" alt="platform icon"></a>
					<h3 style="
    
    font-weight: normal;
    font-style: normal;
"><a href="/capabilities">{{ $page->home_icon_label_4 }}</a></h3>
					<p style="
    
    font-weight: normal;
    font-style: bold;
"><a href="/capabilities" style="
    color: initial;
						">{{ $page->home_icon_description_4 }}</a><br><br><a href="/capabilities"  style="
    
    font-weight: bold;
    font-style: normal;
	color: #0276CE;
">Learn More <img src="{{ URL::to('img/uploads/ICN_ Arrow.png')}}" alt="learn more icon"></a></p>
					
	<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_label_4', 'home_icon_description_4', 'home_icon_label_4') }}> Edit Icon Label 4</button>
		<button class="tms-editor tms-btn-success" {{ $page->editor('home_icon_description_4', 'home_icon_description_4', 'home_icon_description_4') }}> Edit Icon Description 4</button>
	<?php } ?>
				</div>
			</div>

			<!--<div class="row-fluid mt h40" style="clear:both;">
				<div class="gap"></div>
			</div>-->
		</div>
	</section>

<section class="section full-width" style="
    padding-top: 65px;
">
			<ul class="mz pz" id="slider-case-studies">
				<li>
				<section class="section full-width two-column-phone" style="height:463px;background-image:url({{ URL::asset($page->two_column_phone_background->src) }});background-color:{{$page->two_column_phone_background_color}};color:{{ $page->two_column_phone_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span7">
					<h3 class="title small tms-editor tms-inline" style="
    font-size: 15px;
    letter-spacing: 0px;
" {{ $page->editor('two_column_phone_title', 'inline') }}><strong>{{ $page->two_column_phone_title }}</strong></h3>
					<h4 class="title tms-editor tms-inline" style="
    font-size: 44px;
    font-weight: 100;
    line-height: 1em;
" {{ $page->editor('two_column_phone_subtitle', 'inline') }}>{{ $page->two_column_phone_subtitle }}</h4>
				</div>
			</div>
			<div class="row-fluid mt">
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_1_title', 'inline') }}><strong>{{ $page->two_column_phone_column_1_title }}</strong></div>
				</div> 
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_2_title', 'inline') }}><strong>{{ $page->two_column_phone_column_2_title }}</strong></div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 col-left" style="
    width: 53%;
"> 
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_1_description', 'inline') }}>{{ $page->two_column_phone_column_1_description }}</div>
				</div>
				<div class="span6 col-right">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_2_description', 'inline') }}>{{ $page->two_column_phone_column_2_description }}</div>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_background', '.two-column-phone', null, array('updateAttribute' => 'style[background-image]')) }}>Edit Background Image</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_text_color', '.two-column-phone', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_background_color', '.two-column-phone', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>

			<div class="row-fluid">
				<div class="span12">
				@if($page->two_column_phone_link->fieldLabel != '' || TmsAuth::isInGroup('Administration'))
				<a class="circle-arrow-link blue blueLight-text" id="two_column_phone_link" href="{{ $page->two_column_phone_link->href }}" target="{{ $page->two_column_phone_link->target }}"><span class="ml" style="
    font-weight: 700;
">{{ $page->two_column_phone_link->fieldLabel }}</span></a>
				@endif
					
				<a class="circle-arrow-link blue blueLight-text" id="two_column_phone_link" style="margin-left: 70px;" href="http://knowledge.usablenet.com/h/c/218079-case-studies" target="_self"><span class="ml" style="
    font-weight: 700;
">View All Case Studies</span></a>
				
				<?php if(TmsAuth::isInGroup('Administration')) { ?>
				<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_link', '#two_column_phone_link') }}>Edit Link</button>
				<?php } ?>
				</div>
			</div>
		</div>
	</section>
				</li>
			</ul>
</section>


	<section class="section full-width">
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<h3 style="text-align: center;font-size: 42px;" class="tms-editor tms-inline" {{ $page->editor('offices_header', 'inline') }}>{{ $page->offices_header }}</h3>
				</div>
			</div>
			<div class="row-fluid">
				<div class="top-offices">
				<div class="location one-fifth">
					<img src="/images/office-london.png" alt="London building" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;
">
						<h4 style="
    text-transform: uppercase; font-size: 21px;     font-weight: 500; margin: auto;
    margin-bottom: -20px;
" class="fs18 tms-editor tms-inline" {{ $page->editor('location_header_one', 'inline') }}>{{ $page->location_header_one }}</h4><br>
						{{ $page->location_address_one }}
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_address_one') }}>Edit Address</button>
						<?php } ?>
						<br>
						<a class="location-address-link" href="{{ $page->location_map_link_one }}" style="
    font-weight: 700;
">Map It</a><br>
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_map_link_one') }}>Edit Map Link</button>
						<?php } ?>
												{{ $page->location_number_one }}
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_number_one') }}>Edit Number</button>
						<?php } ?>
				</div>
				</div>
				<div class="location one-fifth">
					<img src="/images/office-losangeles.png" alt="Los Angeles downtown" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;
">
						<h4 style="
    text-transform: uppercase; font-size: 21px;     font-weight: 500; 
    margin: auto;
    margin-bottom: -20px;
" class="fs18 tms-editor tms-inline" {{ $page->editor('location_header_two', 'inline') }}>{{ $page->location_header_two }}</h4><br>
						{{ $page->location_address_two }}
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_address_two') }}>Edit Address</button>
						<?php } ?>
						<br>
						<a class="location-address-link" href="{{ $page->location_map_link_two }}" style="
    font-weight: 700;
">Map It</a><br>
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_map_link_two') }}>Edit Map Link</button>
						<?php } ?>
												{{ $page->location_number_two }}
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_number_two') }}>Edit Number</button>
						<?php } ?>
											</div>
				</div>
				<div class="location one-fifth">
					<img src="/images/office-newyork.png" alt="New York bridge" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;
">
						<h4 style="
    text-transform: uppercase; font-size: 21px;     font-weight: 500; 
    margin: auto;
    margin-bottom: -20px;
" class="fs18 tms-editor tms-inline" {{ $page->editor('location_header_three', 'inline') }}>{{ $page->location_header_three }}</h4><br>
						{{ $page->location_address_three }}
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_address_three') }}>Edit Address</button>
						<?php } ?>
						<br>
						<a class="location-address-link" href="{{ $page->location_map_link_three }}" style="
    font-weight: 700;
">Map It</a><br>
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_map_link_three') }}>Edit Map Link</button>
						<?php } ?>
												{{ $page->location_number_three }}
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('location_number_three') }}>Edit Number</button>
						<?php } ?>
											</div>
				</div>
					</div>

			</div>
		</section> 
		
		
	<section class="section full-width hr-slice" style="max-width: none; background-color: #d53c3c; position: relative;">
		<img src="/img/uploads/footer-image.png" alt="business men and women collaborating" style="
    position: absolute;
    bottom: 0;
">
		<div>
			<div class="row-fluid">
				<div class="span6" style="
    width: 43%;
">
					&nbsp; 
				</div>
				<div class="span6 footer-text-careers">
					<h3 class="tms-editor tms-inline" style="
    font-size: 1rem;
    font-style: normal;
    font-weight: 700;
    font-weight: bold;
    text-transform: uppercase;
    color: #fff;
    margin-top: 1rem;
    margin-bottom: 0.5rem;
" {{ $page->editor('careers_top', 'inline') }}>{{ $page->careers_top }}</h3>
					<h4 class="tms-editor tms-inline" style="font-size: 48px !important;color: #fff;font-size: 2.7rem;font-weight: 100;margin-bottom: 2rem;line-height: 40px;" {{ $page->editor('careers_title', 'inline') }} >{{ $page->careers_title }}</h4>
					<div class="btn-holder">
						
						
						
						@if($page->careers_link_one->fieldLabel != '' || TmsAuth::isInGroup('Administration'))
						<a class="circle-arrow-link white" id="careers_link_one" href="{{ $page->careers_link_one->href }}" target="{{ $page->careers_link_one->target }}"><span class="ml">{{ $page->careers_link_one->fieldLabel }}</span></a> 
						@endif
						
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('careers_link_one', '#careers_link_one') }}>Edit Link</button>
						<?php } ?>
						
						
						@if($page->careers_link_two->fieldLabel != '' || TmsAuth::isInGroup('Administration'))
						<a class="circle-arrow-link white" id="careers_link_two" href="{{ $page->careers_link_two->href }}" target="{{ $page->careers_link_two->target }}"><span class="ml">{{ $page->careers_link_two->fieldLabel }}</span></a> 
						@endif
						
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<button class="tms-editor tms-btn" {{ $page->editor('careers_link_two', '#careers_link_two') }}>Edit Link</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
@stop
		
		

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
		<div class="modal-body">
			<video id="video-player" autoplay="true" controls="true" preload="none" width="100%">
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video>
		</div>
	</div>
@stop