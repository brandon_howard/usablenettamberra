@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')

	<section class="section full-width breadcrumbs">
		<div class="container">
			<div class="row-fluid">
				<div class="span2 section-name">
					<p>Partnerships</p>
				</div>
				<div class="span10 crumbs mz">
					<ul class="mz">
						{{ Usablenet::buildcrumbs(URL::current()) }}
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="section full-width relative two-column">
		<div class="container">
			<div class="row-fluid pt">
				<div class="span8 mb">
					<h1 class="title ptz mb large">{{$partner->name}}</h1>
					<h2 class="ptz mb">{{$partner->headline}}</h2>
					<div class="mt h40">
						{{$partner->description}}
					</div>
				</div>
				<div class="span4 text-center mt h60">
					<div class="bt bb">

						@if($partner->logo !== null && $partner->logo !== '')

						<img class="mt mb" src="{{ URL::asset($partner->logo->src)}}" alt="{{ $partner->logo->alt }}">
						<hr class="mz" />
						@endif

						@if($partner->website !== null && $partner->website !== '')
							<a href="{{ $partner->website }}" target="_blank" />{{ $partner->website }}</a>
						</p>
						@endif

						<?php if(TmsAuth::isInGroup('Administration')) { ?>
							<span class="tms-editor" {{ $partner->editor('name', '#usablenet-partner-list', 'edit_partner')}}></span>
							<span class="tms-editor" {{ $partner->editor('slug', '#usablenet-partner-list', 'edit_partner')}}></span>
							<span class="tms-editor" {{ $partner->editor('meta_title', '#usablenet-partner-list', 'edit_partner')}}></span>
							<span class="tms-editor" {{ $partner->editor('meta_description', '#usablenet-partner-list', 'edit_partner')}}></span>
							<span class="tms-editor" {{ $partner->editor('meta_keywords', '#usablenet-partner-list', 'edit_partner', array('type' => 'textarea'))}}></span>
							<span class="tms-editor" {{ $partner->editor('headline', '#usablenet-partner-list', 'edit_partner')}}></span>
							<span class="tms-editor" {{ $partner->editor('description', '#usablenet-partner-list', 'edit_partner', array('type'=>'wysiwyg'))}}></span>
							<span class="tms-editor" {{ $partner->editor('logo', '#usablenet-partner-list', 'edit_partner', array('type'=>'image', 'fieldLabel' => 'Logo: 130 x 100'))}}></span>
							<span class="tms-editor" {{ $partner->editor('website', '#usablenet-partner-list', 'edit_partner')}}></span>
							<span class="tms-editor" {{ $partner->editor('sort', '#usablenet-partner-list', 'edit_partner', array('fieldLabel' => 'Appearance Order'))}}></span>
							<button class="tms-editor tms-btn-success" {{ $partner->editor('fully_published', '#usablenet-partner-list', 'edit_partner', array('type'=>'select', 'options'=>array(0 => 'No', 1 => 'Yes'), 'model'=>'Partner')) }}>Edit Partner</button><br><br>

							<button class="tms-editor tms-btn-danger" {{ $partner->deleteThing(null) }}>Delete <i class="icon-trash icon-white"></i></button>
						<?php } ?>

					</div>
				</div>
			</div>
		</div>
	</section>

	<?php if(TmsAuth::isInGroup('Administration')) { ?>
	<section class="section full-width relative two-column">
		<div class="container">
			<div class="alert alert-block">
				<h3>Attention Administrator</h3>
				<h4>By editing the partner contact form below you will edit all other partners and the partnerships page in the same place</h4>
			</div>
		</div>
	</section>
	<?php } ?>

	@include('usablenet.contact-form-partner')
@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop