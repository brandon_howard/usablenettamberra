@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop



@section('main')


<?php
	if($product->hero_slider_text_one || $product->hero_slider_text_two || $product->hero_slider_text_three){
?>
<section class="section full-width" style="
    padding-top: 0;
"> 
			<ul class="mz pz" id="slider-products-page">
<?php
	if($product->hero_slider_text_one){
?>
				<li class="slider-case-study" style="position: relative;">

	<section class="section full-width two-column-phone" id="sliderOne" style="height:450px;background-image:url(/<?php echo json_decode($product->hero_slider_image_one)->src; ?>);
																background-color:{{ $product->hero_slider_background_one }};
																color:#ffffff">
		
		<div class="container" style="position: relative;">
					<div class="row-fluid">
						<div class="span12" style="text-align: center;">
							<h1 class="title small" id="sliderOneText" style="max-width: 70%; margin: auto; font-size: 55px; margin-top: 60px;">{{ $product->hero_slider_text_one }}</h1>
						</div>
					</div>
		</div>
	</section>
		
				</li>
				
				<?php } ?>
<?php
	if($product->hero_slider_text_two){
?>
				<li class="slider-case-study" style="position: relative;">

	<section class="section full-width two-column-phone" id="sliderTwo" style="height:450px;background-image:url(/<?php echo json_decode($product->hero_slider_image_two)->src; ?>);background-color:{{ $product->hero_slider_background_two }};color:#ffffff">
		<div class="container" style="position: relative;">
					<div class="row-fluid">
						<div class="span12" style="text-align: center;">
							<h1 id="SliderTextTwo" class="title small" style="max-width: 70%; margin: auto; font-size: 55px; margin-top: 60px;">{{ $product->hero_slider_text_two }}</h1>
						</div>
					</div>
		</div>
	</section>
		
				</li>
				
				<?php } ?>
<?php
	if($product->hero_slider_text_three){
?>
				<li class="slider-case-study" style="position: relative;">

	<section class="section full-width two-column-phone" id="sliderThree" style="height:450px;background-image:url(/<?php echo json_decode($product->hero_slider_image_three)->src; ?>);background-color:{{ $product->hero_slider_background_three }};color:#ffffff">
		<div class="container" style="position: relative;">
					<div class="row-fluid">
						<div class="span12" style="text-align: center;">
							<h1 id="SliderTextThree" class="title small" style="max-width: 70%; margin: auto; font-size: 55px; margin-top: 60px;">{{ $product->hero_slider_text_three }}</h1>
						</div>
					</div>
		</div>
	</section>
		
				</li>
				
				<?php } ?>
			</ul>
</section>

<?php } ?>



	<?php if(TmsAuth::isInGroup('Administration')) { ?>
	<section class="section">
		<div class="container">

			<span class="tms-editor" {{ $product->editor('hero_slider_text_one', '#sliderOneText', 'homehero_1') }}></span>
			<button class="tms-editor tms-btn-success" {{ $product->editor('hero_slider_background_one', '#sliderOne', 'homehero_1', array('type'=>'color', 'updateAttribute'=>'style[background-color]')) }}>Manage Hero 1</button>
			<button class="tms-editor tms-btn" {{ $product->editor('hero_slider_image_one', '#sliderOne', 'homehero_1_image', array('type'=>'image', 'updateAttribute' => 'style[background-image]')) }}>Manage Hero 1 Image</button>

			<span class="tms-editor" {{ $product->editor('hero_slider_text_two', '#SliderTextTwo', 'homehero_2') }}></span>
			<button class="tms-editor tms-btn-success" {{ $product->editor('hero_slider_background_two', '#sliderTwo', 'homehero_2', array('type'=>'color', 'updateAttribute'=>'style[background-color]')) }}>Manage Hero 2</button>
			<button class="tms-editor tms-btn" {{ $product->editor('hero_slider_image_two', '#sliderTwo', 'homehero_2_image', array('type'=>'image', 'updateAttribute' =>  'style[background-image]')) }}>Manage Hero 2 Image</button>

			<span class="tms-editor" {{ $product->editor('hero_slider_text_three', '#SliderTextThree', 'homehero_3') }}></span>
			<button class="tms-editor tms-btn-success" {{ $product->editor('hero_slider_background_three', '#sliderThree', 'homehero_3', array('type'=>'color', 'updateAttribute'=>'style[background-color]')) }}>Manage Hero 3</button>
			<button class="tms-editor tms-btn" {{ $product->editor('hero_slider_image_three', '#sliderThree', 'homehero_3_image', array('type'=>'image', 'updateAttribute' =>  'style[background-image]')) }}>Manage Hero 3 Image</button>
			
							<button class="tms-editor tms-btn-info mb h20" {{ $product->editor('title', null, 'edit_product') }}>Edit This Product</button>
							<span class="tms-editor" {{ $product->editor('slug', null, 'edit_product') }}></span>
							<span class="tms-editor" {{ $product->editor('meta_title', null, 'edit_product') }}></span>
							<span class="tms-editor" {{ $product->editor('meta_keywords', null, 'edit_product', array('type' => 'textarea')) }}></span>
							<span class="tms-editor" {{ $product->editor('meta_description', null, 'edit_product') }}></span>
							<span class="tms-editor" {{ $product->editor('thumb_alt', null, 'edit_product') }}></span>
						</div>
	</section>
	<?php } ?>

	<section class="section full-width relative two-column">
		<div class="container">
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
							<button class="tms-editor tms-btn-success" {{ $product->editor('icon', null, 'edit_product_icon', array('type'=>'select', 'options' => array(
									'phone' => 'Phone',
									'computer' => 'Computer',
									'expand' => 'Expand',
									'files' => 'Files',
									'flow' => 'Flow',
									'pages' => 'Pages',
									'tablet' => 'Tablet',
									'touch' => 'Touch',
									'window' => 'Window'
								))) }}>Change Icon</button>
						<?php } ?>
			<div class="row-fluid">
				<div class="span9 mb">
					<div class="row-fluid">
						<div class="span12">
							

					<h2 class="title ptz mb product-title fl" style="
    font-size: 40px;
">@foreach($products as $p)
						@if($p->id == $product->id)
							@if($p->icon == 'pages')
						<img class="ml responsive" src="{{URL::asset('images/icons/'.$p->icon.'.png')}}" width="75" height="75" alt="{{ $product->thumb_alt }}" style="
    margin-right: 30px;
						"/>
							@endif
							@if($p->icon != 'pages')
						<img class="ml responsive" src="{{URL::asset('images/icons/'.$p->icon.'-white.gif')}}" width="75" height="75" alt="{{ $product->thumb_alt }}" />
							@endif

						@endif
					@endforeach<strong class="tms-editor tms-inline" {{ $product->editor('title', 'inline') }}>{{ $product->title }}</strong> </h2></div></div>
					<h3 class="tms-editor tms-inline" style="
    line-height: 1em;
    font-size: 21px;
" {{ $product->editor('headline', 'inline') }}><strong>{{ $product->headline }}</strong></h3>
					<div class="mt h20" id="long_description">
							<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn-success" {{ $product->editor('long_description', '#long_description', 'long_description', array('type'=>'wysiwyg')) }}> Edit Description First Section</button>
	<?php } ?>
						{{ $product->long_description }}
						
					</div>
					
				<?php
					
					if(!$product->box_content_title_one == ''){
						$totalBoxes[] = $product->box_content_title_one;
					}
					
					if(!$product->box_content_title_two == ''){
						$totalBoxes[] = $product->box_content_title_two;
					}
					
					if(!$product->box_content_title_three == ''){
						$totalBoxes[] = $product->box_content_title_three;
					}
					
					if(count($totalBoxes) != 0){
					?>
			<div class="row-fluid">
				<div class="span9 mb">
					<div class="mt h20">
						<h3><strong>RESOURCES</strong></h3>
					</div>
				<?php if(TmsAuth::isInGroup('Administration')) { ?>
						</div>
			<div class="row-fluid">
				<div class="span4 mb">
		<button class="tms-editor tms-btn-success" {{ $product->editor('box_content_title_one', '#box_content_title_one', 'edit_box_content_one') }}> Edit Resource Box 1</button>
		<button class="tms-editor tms-btn" {{ $product->editor('box_content_image_one', '#box_content_image_one', 'edit_box_content_one_image', array('type' => 'image')) }}>Edit Resource Box 1 Image</button>
		<span class="tms-editor" {{ $product->editor('box_content_description_one', '#box_content_description_one', 'edit_box_content_one') }}></span>
				</div>
				<?php } ?>
	<?php if(TmsAuth::isInGroup('Administration')) { ?>
				<div class="span4 mb">
			<button class="tms-editor tms-btn-success" {{ $product->editor('box_content_title_two', '#box_content_title_two', 'edit_box_content_two') }}> Edit Resource Box 2</button>
			<button class="tms-editor tms-btn" {{ $product->editor('box_content_image_two', '#box_content_image_two', 'edit_box_content_two_image', array('type' => 'image')) }}>Edit Resource Box 2 Image</button>
			<span class="tms-editor" {{ $product->editor('box_content_description_two', '#box_content_description_two', 'edit_box_content_two') }}></span>
				</div>
				<?php } ?>
	<?php if(TmsAuth::isInGroup('Administration')) { ?>
				<div class="span4 mb">
			<button class="tms-editor tms-btn-success" {{ $product->editor('box_content_title_three', '#box_content_title_three', 'edit_box_content_three') }}> Edit Resource Box 3</button>
			<button class="tms-editor tms-btn" {{ $product->editor('box_content_image_three', '#box_content_image_three', 'edit_box_content_three_image', array('type' => 'image')) }}>Edit Resource Box 3 Image</button>
			<span class="tms-editor" {{ $product->editor('box_content_description_three', '#box_content_description_three', 'edit_box_content_three') }}></span>
				</div>
				<?php } ?>
			</div>
				<div class="top-offices" style="margin-left: 10px;">
				<?php if(!$product->box_content_title_one == ''){ ?>
				<div class="product-info-box"   style="<?php if(count($totalBoxes) == 1){ echo 'height: 207px; width: 808px;';}else if(count($totalBoxes) == 2){ echo 'width: 377px;'; }else if(count($totalBoxes) == 3){ echo 'width: 245px;'; }  ?>">
					<img style="<?php if(count($totalBoxes) == 1){ echo 'width: 388px; height: 206px';}else if(count($totalBoxes) == 2){ echo 'width: 377px; height: 200px'; }else if(count($totalBoxes) == 3){ echo 'width: 245px; height: 206px'; }  ?>" id="box_content_image_one" src="/<?php echo json_decode($product->box_content_image_one)->src; ?>" alt="<?php echo json_decode($product->box_content_image_one)->alt; ?>" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0; <?php if(count($totalBoxes) == 1){ echo 'float: right; width: 50%;     margin-top: 4%;'; }  ?>
">
						<p style="
    text-transform: uppercase;
    font-size: 14px;
    margin: auto;
    margin-bottom: -20px;
    color: #0276CE;
" class="fs18 location-address-link" id="box_content_title_one">{{ $product->box_content_title_one }}</p><br>
<p style="<?php if(count($totalBoxes) == 1 || count($totalBoxes) == 2){ echo 'font-weight: 700;'; }  ?>" id="box_content_description_one">{{ $product->box_content_description_one }}</p>
																	</div>
				</div>
					<?php } ?>
				<?php if(!$product->box_content_title_two == ''){  ?>	
				<div class="product-info-box"  style="<?php if(count($totalBoxes) == 1){ echo 'height: 207px; width: 808px;';}else if(count($totalBoxes) == 2){ echo 'width: 377px;'; }else if(count($totalBoxes) == 3){ echo 'width: 245px;'; }  ?>">
					<img style="<?php if(count($totalBoxes) == 1){ echo 'width: 388px; height: 206px';}else if(count($totalBoxes) == 2){ echo 'width: 377px; height: 200px'; }else if(count($totalBoxes) == 3){ echo 'width: 245px; height: 206px'; }  ?>" id="box_content_image_two" src="/<?php echo json_decode($product->box_content_image_two)->src; ?>" alt="<?php echo json_decode($product->box_content_image_two)->alt; ?>" class="responsive locaton-image">
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;<?php if(count($totalBoxes) == 1){ echo 'float: right; width: 50%;     margin-top: 4%;'; }  ?>
">
						<p style="
    text-transform: uppercase;
    font-size: 14px;
    margin: auto;
    margin-bottom: -20px;
    color: #0276CE;
" class="fs18 location-address-link" id="box_content_title_two">{{ $product->box_content_title_two }}</p><br>
<p id="box_content_description_two" style="<?php if(count($totalBoxes) == 1 || count($totalBoxes) == 2){ echo 'font-weight: 700;'; }  ?>">{{ $product->box_content_description_two }}</p>
																	</div>
				</div>
					<?php } ?>
				<?php if(!$product->box_content_title_three == ''){  ?>	
				<div class="product-info-box"  style="<?php if(count($totalBoxes) == 1){ echo 'height: 207px; width: 808px;';}else if(count($totalBoxes) == 2){ echo 'width: 377px;'; }else if(count($totalBoxes) == 3){ echo 'width: 245px;'; }  ?>">
						<img id="box_content_image_three" style="<?php if(count($totalBoxes) == 1){ echo 'width: 388px; height: 206px';}else if(count($totalBoxes) == 2){ echo 'width: 377px; height: 200px'; }else if(count($totalBoxes) == 3){ echo 'width: 245px; height: 206px'; }  ?>" src="/<?php echo json_decode($product->box_content_image_three)->src; ?>" alt="<?php echo json_decode($product->box_content_image_three)->alt; ?>" class="responsive locaton-image"></a>
					<div class="location-text" style="
padding: 15px 15px;
    text-align: center;
    line-height: 29px;
    font-style: normal;
    margin-bottom: 0;<?php if(count($totalBoxes) == 1){ echo 'float: right; width: 50%;     margin-top: 4%;'; }  ?>
">
						<p id="box_content_title_three" style="
    text-transform: uppercase;
    font-size: 14px;
    margin: auto;
    margin-bottom: -20px;
    color: #0276CE;
" class="fs18 location-address-link">{{ $product->box_content_title_three }}</p><br>
<p id="box_content_description_three"  style="<?php if(count($totalBoxes) == 1 || count($totalBoxes) == 2){ echo 'font-weight: 700;'; }  ?>">{{ $product->box_content_description_three }}</p>
																	</div>
				</div>
				<?php } ?>
					</div>

			</div>
				<?php  } ?>
			<div class="row-fluid">
				<div class="span9 mb">
					
	<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn-success" {{ $product->editor('long_description_two', '#long_description_two', 'long_description_two', array('type'=>'wysiwyg')) }}> Edit Description Second Section</button>
	<?php } ?>
					<div class="mt h20 long_description_two" id="long_description_two">
						{{ $product->long_description_two }}
					</div>
 
				</div>
			</div>
				</div> 
				@if($product)
				<div class="span3 text-center">
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<div class="container">
							<span class="tms-editor" {{ $product->editor('caseStudy_headline', '#caseStudy_headline', 'edit_case_study') }}></span>
							<span class="tms-editor" {{ $product->editor('caseStudy_body', '#caseStudy_body', 'edit_case_study', array('type'=>'wysiwyg')) }}></span>
							<button class="tms-editor tms-btn-success mb h20" {{ $product->editor('caseStudy_download', '#caseStudy_download', 'edit_case_study', array('updateAttribute' => 'href')) }}>Edit Case Study</button>
						</div>
					<?php } ?>
					<div class="row-fluid">
						<div class="span12 text-center" style="
    background-color: #efecec;
    width: 245px;
">
					<a href="{{ $product->caseStudy_download }}" target="_blank"><img alt="infographic" height="172" src="/img/uploads/thumbnail_usablenet_accessibility_infographic_final.jpg" width="150" style="/* float:left; */margin-top: 35px;"></a>
						</div>
					</div>
					<div class="row-fluid" style="
    background-color: #efecec;
    width: 245px;
">
						<div class="span12" style="text-align: left;">
					<h3 style="
    text-align: left;     text-transform: uppercase;
" id="caseStudy_headline"><strong>{{ $product->caseStudy_headline }}</strong></h3>
							<p style="text-align: left;" id="caseStudy_body">{{ $product->caseStudy_body }}</p>
							<br/>
							<a id="caseStudy_download" href="{{ $product->caseStudy_download }}" target="_blank" class="btn btn-success btn-flat" style="
    font-size: 18px;
    margin-bottom: 25px;
    width: 200px;
    line-height: 40px;
    height: 50px;     font-weight: 700;
">Download</a>
							
					</div>
					</div>
				</div>
				@endif
			</div>
		</div>
	</section>

	@include('usablenet.case-studies')


@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" id="pause-video" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
		<div class="modal-body">
			<video id="video-player" autoplay="false" controls="true" preload="none" width="100%">
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video
		</div>
	</div>
@stop