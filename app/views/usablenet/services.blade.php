<!doctype html>
<html lang="en">
<head>
	<script type="text/javascript" src="http://www.bae5tracker.com/js/60918.js" ></script>
	<noscript><img src="http://www.bae5tracker.com/60918.png" style="display:none;" /></noscript>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	@if(isset($meta) && isset($meta['meta_title']))
		<title>{{ $meta['meta_title'] }}</title>
	@else
		<title>Usablenet</title>
	@endif

	@if(isset($meta) && isset($meta['meta_description']))
		<meta name="description" content="{{ $meta['meta_description'] }}">
	@endif

	@if(isset($meta) && isset($meta['meta_keywords']))
		<meta name="keywords" content="{{$meta['meta_keywords'] }}">
	@endif 

	@if(isset($page->head))
		{{$page->head}}
	@endif

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">

	<!-- compiled stylesheet -->
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
	<link rel="stylesheet" type="text/css" 
href="//cloud.typography.com/7951294/7261352/css/fonts.css" />
	<link rel="stylesheet" href="{{ URL::asset('css/menu.css') }}" type="text/css" />
	
	
	<link rel="stylesheet" href="{{ URL::asset('css/css.css') }}" type="text/css" />
	@if (App::environment('stag'))
		<link rel="stylesheet" href="{{ URL::asset('css/debug.css') }}" type="text/css" />
	@endif


<?php if(TmsAuth::isInGroup('Administration')) { ?>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/smoothness/jquery-ui-1.10.3.custom.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('js/spectrum/spectrum.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('js/chosen/chosen.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/tms.css') }}">
	

<?php } ?>
	<link rel="stylesheet" href="{{ URL::asset('css/fonts.css') }}">
	<!--[if lt IE 9]>
		<script src="{{ URL::asset('js/html5shiv.js') }}"></script>
		<script src="{{ URL::asset('js/respond.js') }}"></script>
	<![endif]-->

	<!--[if IE]>
		<link rel="stylesheet" href="{{ URL::asset('css/ie.css') }}" type="text/css" />
	<![endif]-->

<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=145046,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body class="responsive">

	
	
	
<!-- Google Tag Manager-->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M47VWD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M47VWD');</script>
<!-- End Google Tag Manager-->

	<div id="header">
		<section id="main_header_container">
			<div id="main_navigation" class="section full-width">
				<div class="container">
					<div class="row-fluid">
						<div class="span12">
							<div class="row-fluid">

								<div class="span2">
									<a href="{{URL::to('/')}}" id="logo" class="clearfix">
										<img style="max-width:165px;" src="{{URL::asset('images/usablenet-header-logo.png')}}" width="218" height="65" class="responsive" alt="Usablenet Logo"/>
									</a>
								</div>

								<ul id="main_menu" class="main_menu">
									
									<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2815 never-mobile dropdown dropdown-products" id="menu-item-2815">
										<a href="{{ URL::to('products')}}">Products <img src="/images/icons/down.png"></a>
										<ul class="dropdown-content dropdown-products-content">
			<li><a class="menu-group-item active" href="http://usablenet.com/products/mobile">Mobile Sites</a></li>
			<li><a class="menu-group-item" href="http://usablenet.com/products/apps">Apps</a></li>
			<li><a class="menu-group-item"  href="http://usablenet.com/products/web-accessibility">Web Accessibility</a></li>
			<li><a class="menu-group-item" href="/products/u-campaign">U-Campaign</a></li>
			<li><a class="menu-group-item" href="/products/qa-services">QA Services</a></li>
			<li><a class="menu-group-item"  href="http://usablenet.com/products/ux-services">UX Services</a></li></ul>
									</li>
									<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2971 never-mobile dropdown dropdown-resources" id="menu-item-2971">
										<a href="http://knowledge.usablenet.com/">Resources <img src="/images/icons/down.png"></a>
										<ul class="dropdown-content dropdown-resources-content">
			<li><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/211675-b2b">Featured Content</a></li>
			<li><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/218079-case-studies" >Case Studies</a></li>
			<li><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/218082-industry-bites" >Industry Newsletter</a></li>
			<li><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/228315-whitepapers" >White Papers</a></li>
			<li><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/228321-e-books">E-Books</a></li>
			<li><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/218106-videos">Videos</a></li></ul>
									</li>
									<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9512 never-mobile" id="menu-item-9512">
										<a href="{{ URL::to('clients')}}">Clients</a>
									</li>
									<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7613 never-mobile" id="menu-item-7613">
										<a href="{{ URL::to('about-us')}}">Company</a>
									</li>
									<li class="menu-item menu-item-type-custom menu-item-object-custom never-mobile">
										<a href="http://knowledge.usablenet.com/h/c/218682-blog">Blog</a>
									</li>
									
									
									
									<li>
										<a id="requestDemoFormClick"  class="btn btn-success btn-flat" style="
    font-weight: 700 !important;
">Request A Demo</a>
									</li>
									<li style="vertical-align:text-top;padding-left:20px;">

										<?php 

										$parsed = parse_url(URL::current()); 
										$host  = isset($parsed['host']) ? $parsed['host'] : '';
										$path  = isset($parsed['path']) ? $parsed['path'] : '';
										$query = isset($parsed['query']) ? '?' . $parsed['query'] : '';

										$full_url = 'http://access.usablenet.com/h5/r/'.$host.$path.$query;

										?>
										<a style="width:100px;line-height:14px;text-align:center;color:#fff;margin-top:-5px; width: 128px !important;" href="<?= $full_url ?>">Accessible View</a>
									</li>
									<li class="only-mobile">
										<a style="
    cursor: pointer;
" id="sidebar-menu" class="hamburger"><img src="{{URL::asset('images/hamburger.png')}}" ></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	
	<section class="section full-width page-title-slice" style="background-color:#f2f1ed;color:#ffffff; position: relative;    min-height: 220px;">
		<div class="container">
			<div class="row-fluid" style="    bottom: 0;
    position: absolute;">
				<div class="span6" style="    padding-bottom: 25px;
    padding-left: 15px;">
					<h2 class="title tms-editor tms-inline page-title" style="
    font-weight: 300;
	font-size: 55px;
																			  color: #000000;
" {{ $page->editor('page_title', 'inline') }}>{{ $page->page_title }}</h2>
				</div>
			</div>
					</div>
	</section>
	<section class="section full-width"  style="
    margin-top: -10px;
">
		<div class="container"  style="border-bottom: 1px solid #cccccc;">
			<div class="row-fluid">
				<div class="span7">
														</div>
			</div>
			<div class="row-fluid mt h40" style="
    margin-bottom: 40px;
">
				<div class="span4">
					<div class="tms-editor tms-inline" {{ $page->editor('services_image_1', 'inline') }}><img  src="{{ $page->services_image_1->src }}" class="responsive"></div>
				</div>
				<div class="span8" style="
    margin-left: -5px;
">
				<h2 style="
    font-weight: 700;
" class="tms-editor tms-inline" {{ $page->editor('title_one', 'inline') }}>{{ $page->title_one }}</h2>
					<br>
				<p class="tms-editor tms-inline" {{ $page->editor('description_one', 'inline') }}>{{ $page->description_one }}</p>
					<p style="
    font-size: 20px;
    font-weight: 700;
">@if($page->section_one_link_one->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_one_link_one" href="{{ $page->section_one_link_one->href }}" target="{{ $page->section_one_link_one->target }}">{{ $page->section_one_link_one->fieldLabel }}</a></span>@endif @if($page->section_one_link_two->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_one_link_two" href="{{ $page->section_one_link_two->href }}" target="{{ $page->section_one_link_two->target }}">{{ $page->section_one_link_two->fieldLabel }}</a></span>@endif @if($page->section_one_link_three->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_one_link_three" href="{{ $page->section_one_link_three->href }}" target="{{ $page->section_one_link_three->target }}">{{ $page->section_one_link_three->fieldLabel }}</a></span>@endif @if($page->section_one_link_four->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_one_link_four" href="{{ $page->section_one_link_four->href }}" target="{{ $page->section_one_link_four->target }}">{{ $page->section_one_link_four->fieldLabel }}</a></span>@endif /</p>
					
					
					
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<p style="color:red;">Leave labels blank to not show link.</p>
						<button class="tms-editor tms-btn" {{ $page->editor('section_one_link_one', '#section_one_link_one') }}>Edit Link One</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_one_link_two', '#section_one_link_two') }}>Edit Link Two</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_one_link_three', '#section_one_link_three') }}>Edit Link Three</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_one_link_four', '#section_one_link_four') }}>Edit Link Four</button>
					<?php } ?> 
				</div>
			</div>
			</div>
	</section>
	<section class="section full-width" style="
    padding-top: 0;
    margin-top: -40px;
">
		<div class="container"  style="border-bottom: 1px solid #cccccc;">
			<div class="row-fluid">
				<div class="span7">
														</div>
			</div>
			<div class="row-fluid mt h40" style="
    margin-bottom: 40px;
">
				<div class="span4">
					<div class="tms-editor tms-inline" {{ $page->editor('services_image_2', 'inline') }}><img  src="{{ $page->services_image_2->src }}" class="responsive"></div>
				</div>
				<div class="span8" style="
    margin-left: -5px;
">
					<h2 style="
    font-weight: 700;
" class="tms-editor tms-inline" {{ $page->editor('title_two', 'inline') }}>{{ $page->title_two }}</h2>
					<br>
				<p class="tms-editor tms-inline" {{ $page->editor('description_two', 'inline') }}>{{ $page->description_two }}</p>
					<p style="
    font-size: 20px;
    font-weight: 700;
">@if($page->section_two_link_one->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_two_link_one" href="{{ $page->section_two_link_one->href }}" target="{{ $page->section_two_link_one->target }}">{{ $page->section_two_link_one->fieldLabel }}</a></span>@endif @if($page->section_two_link_two->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_two_link_two" href="{{ $page->section_two_link_two->href }}" target="{{ $page->section_two_link_two->target }}">{{ $page->section_two_link_two->fieldLabel }}</a></span>@endif @if($page->section_two_link_three->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_two_link_three" href="{{ $page->section_two_link_three->href }}" target="{{ $page->section_two_link_three->target }}">{{ $page->section_two_link_three->fieldLabel }}</a></span>@endif @if($page->section_two_link_four->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_two_link_four" href="{{ $page->section_two_link_four->href }}" target="{{ $page->section_two_link_four->target }}">{{ $page->section_two_link_four->fieldLabel }}</a></span>@endif /</p>
					
					
					
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<p style="color:red;">Leave labels blank to not show link.</p>
						<button class="tms-editor tms-btn" {{ $page->editor('section_two_link_one', '#section_two_link_one') }}>Edit Link One</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_two_link_two', '#section_two_link_two') }}>Edit Link Two</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_two_link_three', '#section_two_link_three') }}>Edit Link Three</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_two_link_four', '#section_two_link_four') }}>Edit Link Four</button>
					<?php } ?>
				</div>
			</div>
			</div>
	</section>
	<section class="section full-width" style="
    padding-top: 0;
    margin-top: -40px;
">
		<div class="container"  style="border-bottom: 1px solid #cccccc;">
			<div class="row-fluid">
				<div class="span7">
														</div>
			</div>
			<div class="row-fluid mt h40" style="
    margin-bottom: 40px;
">
				<div class="span4">
					<div class="tms-editor tms-inline" {{ $page->editor('services_image_3', 'inline') }}><img  src="{{ $page->services_image_3->src }}" class="responsive"></div>
				</div>
				<div class="span8" style="
    margin-left: -5px;
">
					<h2 style="
    font-weight: 700;
" class="tms-editor tms-inline" {{ $page->editor('title_three', 'inline') }}>{{ $page->title_three }}</h2>
					<br>
				<p class="tms-editor tms-inline" {{ $page->editor('description_three', 'inline') }}>{{ $page->description_three }}</p>
					<p style="
    font-size: 20px;
    font-weight: 700;
">@if($page->section_three_link_one->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_three_link_one" href="{{ $page->section_three_link_one->href }}" target="{{ $page->section_three_link_one->target }}">{{ $page->section_three_link_one->fieldLabel }}</a></span>@endif @if($page->section_three_link_two->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_three_link_two" href="{{ $page->section_three_link_two->href }}" target="{{ $page->section_three_link_two->target }}">{{ $page->section_three_link_two->fieldLabel }}</a></span>@endif @if($page->section_three_link_three->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_three_link_three" href="{{ $page->section_three_link_three->href }}" target="{{ $page->section_three_link_three->target }}">{{ $page->section_three_link_three->fieldLabel }}</a></span>@endif @if($page->section_three_link_four->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_three_link_four" href="{{ $page->section_three_link_four->href }}" target="{{ $page->section_three_link_four->target }}">{{ $page->section_three_link_four->fieldLabel }}</a></span>@endif /</p>
					
					
					
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<p style="color:red;">Leave labels blank to not show link.</p>
						<button class="tms-editor tms-btn" {{ $page->editor('section_three_link_one', '#section_three_link_one') }}>Edit Link One</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_three_link_two', '#section_three_link_two') }}>Edit Link Two</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_three_link_three', '#section_three_link_three') }}>Edit Link Three</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_three_link_four', '#section_three_link_four') }}>Edit Link Four</button>
					<?php } ?>
				</div>
			</div>
			</div>
	</section>
	<section class="section full-width" style="
    padding-top: 0;
    margin-top: -40px;
">
		<div class="container">
			<div class="row-fluid">
				<div class="span7">
														</div>
			</div>
			<div class="row-fluid mt h40">
				<div class="span4">
					<div class="tms-editor tms-inline" {{ $page->editor('services_image_4', 'inline') }}><img  src="{{ $page->services_image_4->src }}" class="responsive"></div>
				</div>
				<div class="span8" style="
    margin-left: -5px;
">
					<h2 style="
    font-weight: 700;
" class="tms-editor tms-inline" {{ $page->editor('title_four', 'inline') }}>{{ $page->title_four }}</h2>
					<br>
				<p class="tms-editor tms-inline" {{ $page->editor('description_four', 'inline') }}>{{ $page->description_four }}</p>
					<p style="
    font-size: 20px;
    font-weight: 700;
">@if($page->section_four_link_one->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_four_link_one" href="{{ $page->section_four_link_one->href }}" target="{{ $page->section_four_link_one->target }}">{{ $page->section_four_link_one->fieldLabel }}</a></span>@endif @if($page->section_four_link_two->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_four_link_two" href="{{ $page->section_four_link_two->href }}" target="{{ $page->section_four_link_two->target }}">{{ $page->section_four_link_two->fieldLabel }}</a></span>@endif @if($page->section_four_link_three->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_four_link_three" href="{{ $page->section_four_link_three->href }}" target="{{ $page->section_four_link_three->target }}">{{ $page->section_four_link_three->fieldLabel }}</a></span>@endif @if($page->section_four_link_four->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_four_link_four" href="{{ $page->section_four_link_four->href }}" target="{{ $page->section_four_link_four->target }}">{{ $page->section_four_link_four->fieldLabel }}</a></span>@endif /</p>
					
					
					
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<p style="color:red;">Leave labels blank to not show link.</p>
						<button class="tms-editor tms-btn" {{ $page->editor('section_four_link_one', '#section_four_link_one') }}>Edit Link One</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_four_link_two', '#section_four_link_two') }}>Edit Link Two</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_four_link_three', '#section_four_link_three') }}>Edit Link Three</button>
						<button class="tms-editor tms-btn" {{ $page->editor('section_four_link_four', '#section_four_link_four') }}>Edit Link Four</button>
					<?php } ?>
				</div>
			</div>
			</div>
	</section>
	 <a id='sticky'>Contact Us</a>

	<div id="footer-wrapper">
		<div class="footer-section never-mobile">
			<div class="container">
				<div class="row-fluid">
					<div class="span3 span-dt-3 span-tab-3 footer-menu">
						<img class="footer-logo" alt="Usablenet Logo" src="http://usablenet.com/images/usablenet-footer-logo.png" style="
    position: absolute;
    left: -100px;
">
						<h4 style="
    font-weight: 300;
    text-transform: uppercase;
">About</h4>
						<ul>
							<li><a href="http://usablenet.com/why-usablenet">Why Usablenet?</a></li>
							<li><a href="http://usablenet.com/capabilities">Capabilities</a></li>
							<li><a href="http://usablenet.com/platform">Our Platform</a></li>
							<li><a href="http://usablenet.com/partnerships">Partnerships</a></li>
							<li><a href="http://usablenet.com/careers">Careers</a></li>
							<li><a href="http://usablenet.com/contact-us">Contact Us</a></li>
						</ul>
					</div>
					<div class="span3 span-dt-3 span-tab-3 footer-menu">
						<h4 style="
    font-weight: 300;
    text-transform: uppercase;
">Products</h4>
						<ul>
							<li><a href="http://usablenet.com/products/mobile">Mobile Sites</a></li>
							<li><a href="http://usablenet.com/products/apps">Apps</a></li>
							<li><a href="http://usablenet.com/products/web-accessibility">Web Accessibility</a></li>
							<li><a href="/products/qa-services">QA Services</a></li>
							<li><a href="http://usablenet.com/products/ux-services">UX Services</a></li>
							<li><a href="/capabilities">API Services</a></li>
							
						</ul>
					</div>
					<div class="span3 span-dt-3 span-tab-3 footer-menu">
						<h4 style="
    font-weight: 300;
    text-transform: uppercase;
">Resources</h4>
						<ul>
							<li><a href="http://knowledge.usablenet.com/h/c/218682-blog?_ga=1.73365911.547432105.1459343597">Blog</a></li>
							<li><a href="http://knowledge.usablenet.com/h/c/211675-b2b">Featured Content</a></li>
							<li><a href="http://knowledge.usablenet.com/h/c/218079-case-studies">Case Studies</a></li>
							<li><a href="http://knowledge.usablenet.com/h/c/218082-industry-bites">Industry Newsletter</a></li>
							<li><a href="http://knowledge.usablenet.com/h/c/228315-whitepapers">Whitepapers</a></li>
							<li><a href="http://knowledge.usablenet.com/h/c/228321-e-books">E-Books</a></li>
							<li><a href="http://knowledge.usablenet.com/h/c/218106-videos">Videos</a></li>
							
						</ul>
					</div>
					<div class="span3 span-dt-3 span-tab-3 footer-menu" style="
    width: 21%;
">
						<h4 style="
    font-weight: 300;
    text-transform: uppercase; margin-left: -2px; 
">Connect</h4>
						
						<ul>
							<li><a class="social-links" href="http://www.linkedin.com/company/156593?trk=tyah&trkInfo=tas%3Ausablenet%2Cidx%3A4-1-5" target="_blank"><img src="{{URL::asset('img/uploads/linkedin.png')}}" style="
    margin-right: 13px;
"> LinkedIn</a></li>
							<li><a class="social-links" href="https://twitter.com/Usablenet" target="_blank"><img src="{{URL::asset('img/uploads/twitter.png')}}" style="
    margin-right: 13px;
"> Twitter</a></li>
							<li><a class="social-links" href="https://plus.google.com/u/2/b/103635202693633675970/103635202693633675970/about" target="_blank"><img src="{{URL::asset('img/uploads/google.png')}}" style="
    margin-right: 11px;     margin-left: 2px;
"> Google+</a></li>
							<li><a class="social-links" href="https://www.facebook.com/usablenet" target="_blank"><img src="{{URL::asset('img/uploads/facebook.png')}}" style="
    margin-right: 19px;     margin-left: 2px;
"> Facebook</a></li>
							<!--<li><a class="share-btn pinterest" href="http://www.pinterest.com/usablenet/" target="_blank">Pinterest</a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="copyright" class="never-mobile">
			<div class="container">
				<div class="row-fluid">
					<div class="span6 span-dt-4 span-tab-4"><img class="footer-logo only-mobile" alt="Usablenet Logo" src="http://usablenet.com/images/usablenet-footer-logo.png">
					
					<span class="color white tms-editor tms-inline">© 2000-2016 Usablenet, Inc. All Rights Reserved</span>  <a class="never-mobile" href="/privacy"> Privacy Policy and Terms of Use</a></div>

					<div class="span6 span-dt-2 span-tab-5 tar tms-editor tms-inline never-mobile">
						24 Hour Support Line For Usablenet Clients +1.888.730.4086
					</div>

				</div>
			</div>
		</div>
		<div id="copyright" class="only-mobile">
			<div class="container">
				<div class="row-fluid">
					<div class="span1 span-dt-1 span-tab-1 only-mobile">
						<img class="footer-logo" alt="Usablenet Logo" src="http://usablenet.com/images/usablenet-footer-logo.png">
					</div>
					<div class="span6 span-dt-4 span-tab-4 support-text">
					<span class="color white tms-editor tms-inline">© 2000-2016 Usablenet, Inc. All Rights Reserved</span>  <a class="never-mobile" href="/privacy"> Privacy Policy and Terms of Use</a></div>

					<div class="span6 span-dt-5 span-tab-5 tar tms-editor tms-inline support-text never-mobile">
						24 Hour Support Line For Usablenet Clients +1.888.730.4086
					</div>

				</div>
			</div>
		</div>
	</div>
	
	<form id="mktoForm_1346"></form>
	<form id="mktoForm_1338"></form>
	
	@yield('modals')
	<div id="contact-us" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
		<div class="modal-body">
			@include('usablenet.contact-form-modal')
		</div>
	</div>
	<div id="partnership-m" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
		<div class="modal-body">
			@include('usablenet.partnership-form-modal')
		</div>
	</div>
	<script src="{{ URL::asset('js/script.min.js') }}"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
	<script src="{{ URL::asset('js/modal-forms.js') }}"></script>
	<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52bd959625ce24b2"></script>
	<!--<script type="text/javascript" src="//use.typekit.net/ipu0vzs.js"></script>-->
	<!--<script type="text/javascript">try{Typekit.load();}catch(e){}</script>-->
	<!--[if (gte IE 6)&(lte IE 8)]>
		<script src="{{ URL::asset('js/selectivizr-min.js') }}"></script>
	<![endif]-->
	<!--[if IE]>
		<script src="{{ URL::asset('js/jquery.placeholder.js') }}"></script>
		<script type="text/javascript">$(document).ready(function(){ $('input, textarea').placeholder(); });</script>
	<![endif]-->
	<script src="{{ URL::asset('js/usablenet.js') }}"></script>
	<script src="http://app-sj04.marketo.com/js/forms2/js/forms2.js"></script>			
	
	<script>
		var DELAY = 300, clicks = 0, timer = null;

		$("#sticky").click(function(e) {
			clicks++;  
			if(clicks === 1) {
				timer = setTimeout(function() {
	
					MktoForms2.loadForm("http://app-sj01.marketo.com", "644-RZH-969", 1346, function (form){
						var lightbox = MktoForms2.lightbox(form).show();
						form.onSuccess(function(){
							lightbox.hide();
							return false;
						});
				});  //perform single-click action    
                clicks = 0;             //after action performed, reset counter

            }, DELAY);

        } else {

            clearTimeout(timer);    //prevent single-click action
            clicks = 0;             //after action performed, reset counter
        }

    });

	$("#requestDemoFormClick").click(function(e) {
			clicks++;  
			if(clicks === 1) {
				timer = setTimeout(function() {
	
					MktoForms2.loadForm("http://app-sj01.marketo.com", "644-RZH-969", 1338, function (form){
						var lightbox = MktoForms2.lightbox(form).show();
						form.onSuccess(function(){
							lightbox.hide();
							return false;
						});
				});  //perform single-click action    
                clicks = 0;             //after action performed, reset counter

            }, DELAY);

        } else {

            clearTimeout(timer);    //prevent single-click action
            clicks = 0;             //after action performed, reset counter
        }

    })
    .on("dblclick", function(e){
        e.preventDefault();  //cancel system double-click event
    });




 


		/*$( "#contactUsFormClick" ).click(function() {
			if(window.formopen == false){
		 if($('#MktoForms2XDIframe:visible').length  == 0){
			 window.formopen = true;
			MktoForms2.loadForm("http://app-sj01.marketo.com", "644-RZH-969", 1346, function (form){
				var lightbox = MktoForms2.lightbox(form).show();
				form.onSuccess(function(){ lightbox.hide(); window.formopen = false; return false; });
			});	
			}else if($('#MktoForms2XDIframe:visible').length  == 0){
				$('#MktoForms2XDIframe').show();
			}}
		});

		$( "#requestDemoFormClick" ).click(function() {
			if(window.formopen == false){
			if($('#MktoForms2XDIframe:visible').length  == 0){
				window.formopen = true;
			MktoForms2.loadForm("http://app-sj01.marketo.com", "644-RZH-969", 1338, function (form){
				var lightbox = MktoForms2.lightbox(form).show();
				form.onSuccess(function(){ lightbox.hide(); window.formopen = false; return false; });
			});	
			}else if($('#MktoForms2XDIframe:visible').length  == 0){
				$('#MktoForms2XDIframe').show();
			}
			}
		});*/

		$('.mktoModalClose').click(function() {
			window.formopen = false;
			
		_finish();
		return false;
	});
	</script>
	
	<script src="//cdn.jsdelivr.net/jquery/2.2.0/jquery.min.js"></script>
    <!-- Include the Sidr JS -->
    <script src="//cdn.jsdelivr.net/jquery.sidr/2.2.1/jquery.sidr.min.js"></script>
	<script src="{{ URL::asset('js/bxslider/jquery.bxslider.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/caseStudySlider.js') }}"></script>

<?php if(TmsAuth::isInGroup('Administration')) { ?>

	<script src="{{ URL::asset('js/spectrum/spectrum.js') }}"></script>
	<script src="{{ URL::asset('js/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ URL::asset('js/chosen/chosen.jquery.min.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.mjs.nestedSortable.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.admin.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.network.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.inline.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.popup.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.color.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.date.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.image.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.select.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.text.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.textarea.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.video.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.tms.editors.link.js') }}"></script>
	<script type="text/javascript">
		$(".tms-editor").tmsEditor();
		$("#tms-admin").tmsAdmin();
	</script>

<?php } ?>
	
	@if(isset($page->footer))
		{{$page->footer}} 
	@endif

	@yield('scripts')
	
<div id="sidr">
	<div class="row-fluid">
		<div class="span12">
			<p class="current-menu-page">Home</p>
			<a class="menu-close-button" id="sidebar-menu-close" style="
    cursor: pointer;
"><img src="{{URL::asset('images/close.png')}}" ></a>
		</div>
		<div class="span12">
			<p class="menu-group"><a href="http://usablenet.com/products">Products</a></p>
			<p><a class="menu-group-item" href="http://usablenet.com/products/mobile">Mobile</a></p>
			<p><a class="menu-group-item" href="http://usablenet.com/products/apps">Apps</a></p>
			<p><a class="menu-group-item"  href="http://usablenet.com/products/web-accessibility">Web Accessibility</a></p>
			<p><a class="menu-group-item" href="/products/u-campaign">U-Campaign</a></p>
			<p><a class="menu-group-item" href="/products/qa-services">QA Services</a></p>
			<p><a class="menu-group-item"  href="http://usablenet.com/products/ux-services">UX Services</a></p>
		</div>
		<div class="span12">
			<p class="menu-group"><a href="http://knowledge.usablenet.com/?_ga=1.7221942.547432105.1459343597">RESOURCES</a></p>
			<p><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/218682-blog">Blog</a></p>
			<p><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/211675-b2b">Featured Content</a></p>
			<p><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/218079-case-studies">Case Studies</a></p>
			<p><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/218082-industry-bites">Industry Newsletter</a></p>
			<p><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/228315-whitepapers">White Papers</a></p>
			<p><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/228321-e-books">E-Books</a></p>
			<p><a class="menu-group-item" href="http://knowledge.usablenet.com/h/c/218106-videos">Videos</a></p>
		</div>
		<div class="span12">
			<p class="menu-group"><a href="http://usablenet.com/about-us">COMPANY</a></p>
			<p><a class="menu-group-item" href="http://usablenet.com/why-usablenet">Why Usablenet?</a></p>
			<p><a class="menu-group-item" href="http://usablenet.com/capabilities">Capabilities</a></p>
			<p><a class="menu-group-item" href="http://usablenet.com/platform">Our Platform</a></p>
			<p><a class="menu-group-item" href="http://usablenet.com/partnerships">Partnerships</a></p>
			<p><a class="menu-group-item" href="http://usablenet.com/careers">Careers</a></p>
			<p><a class="menu-group-item" id="contactUsFormClick">Contact Us</a></p>
		</div>
		<div class="span12">
			<p class="menu-group"></p>
			<p><a class="menu-group-item" href="http://www.linkedin.com/company/156593?trk=tyah&trkInfo=tas%3Ausablenet%2Cidx%3A4-1-5" target="_blank"><img style="
    margin-right: 13px;
" src="{{URL::asset('img/uploads/linkedin.png')}}"> Linkedin</a></p>
			<p><a class="menu-group-item"  href="https://twitter.com/Usablenet" target="_blank"><img src="{{URL::asset('img/uploads/twitter.png')}}" style="
    margin-right: 13px;
"> Twitter</a></p>
			<p><a class="menu-group-item"  href="https://plus.google.com/u/2/b/103635202693633675970/103635202693633675970/about" target="_blank"><img src="{{URL::asset('img/uploads/google.png')}}" style="
    margin-right: 11px;     margin-left: 2px;
"> Google+</a></p>
			<p><a class="menu-group-item"  href="https://www.facebook.com/usablenet" target="_blank"><img src="{{URL::asset('img/uploads/facebook.png')}}" style="
    margin-right: 19px;     margin-left: 2px;
"> Facebook</a></p>
		</div>
		<div class="span12">
			<p class="menu-group"></p>
			<p style="
    font-size: 13px;
    margin-top: 25px;
    color: #999999;
					  margin-bottom: -15px;
">24 Hour Client Support:</p>
			<p style="
    font-size: 13px;
    margin-top: 25px;
    color: #999999;
">+1.888.730.4086</p>
		</div>
		<div class="span12">
			<p class="menu-group"></p>
			<p><a class="menu-group-item" href="/privacy" style="
    font-size: 13px;
">Privacy Policy</a></p>
			<p><a class="menu-group-item" href="/privacy" style="
    font-size: 13px;
">Terms of Use</a></p>
		</div>
	</div>
	
	
</div>

<script>
$(document).ready(function() {
  $('#sidebar-menu').sidr({side: 'right'});
  //$('#sidebar-menu-close').sidr('toggle', {side: 'right'});
	
 $('#sidebar-menu-close').click(function () {
 	$.sidr('close', 'sidr');
 });
});
</script>
		<script>

MktoForms2.onFormRender(function(form){

  MktoForms2.$(window).resize();

});

</script>
	
</body>
</html>

