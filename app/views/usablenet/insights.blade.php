@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')
	<section class="section full-width breadcrumbs">
		<div class="container">
			<div class="row-fluid">
				<div class="span2 section-name">
					<p>Usablenet.com</p>
				</div>
				<div class="span10 crumbs mz">
					<ul class="mz">
						{{ Usablenet::buildcrumbs(URL::current()) }}
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="section full-width news-subnav">
		<div class="container">
			<div class="row-fluid">
				<div class="span9">
					<ul class="list-unstyled news-archives">
						<li><a href="{{ URL::action('PostController@index') }}">Most Current News</a></li>
						@if(date('Y') > 2014)
						<li><a href="{{ URL::action('PostController@archive', 2014) }}">2014 Archives</a></li>
						@endif
						<li><a href="{{ URL::action('PostController@archive', 2013) }}">2013 Archives</a></li>
					</ul>
				</div>
				<div class="span3 tar">
					<a href="{{ URL::route('rss', '2.0') }}"><img src="{{ URL::asset('images/rss-news.png') }}" width="142" height="25" class="rss-news" alt="Usablenet RSS News Feed"></a>
				</div>
			</div>
		</div>
	</section>

	<section class="section full-width relative two-column">
		<div class="container">
			<div class="row-fluid">

				<div class="span4 span-tab-12 span-dt-12 mb h60  fl-wide fr">

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<div class="container">
							<span class="tms-editor" {{ Tms::editor('title', '#usablenet-blog-posts', 'edit_post', array('model'=>'Post'))}}></span>
							<span class="tms-editor" {{ Tms::editor('slug', '#usablenet-blog-posts', 'edit_post', array('model'=>'Post'))}}></span>
							<span class="tms-editor" {{ Tms::editor('category_id', '#usablenet-blog-posts', 'edit_post', array('type'=>'select', 'options'=>$categoryList, 'model'=>'Post')) }}></span>
							<span class="tms-editor" {{ Tms::editor('author_id', '#usablenet-blog-posts', 'edit_post', array('type'=>'select', 'options'=>$authors, 'model'=>'Post')) }}></span>
							<span class="tms-editor" {{ Tms::editor('main_image', '#usablenet-blog-posts', 'edit_post', array('type'=>'image', 'model'=>'Post')) }}></span>
							<span class="tms-editor" {{ Tms::editor('posted_on', '#usablenet-blog-posts', 'edit_post', array('type'=>'date', 'model'=>'Post')) }}></span>
							<span class="tms-editor" {{ Tms::editor('headline', '#usablenet-blog-posts', 'edit_post', array('type'=>'wysiwyg', 'model'=>'Post')) }}></span>
							<button class="tms-editor tms-btn-success" {{ Tms::editor('article', '#usablenet-blog-posts', 'edit_post', array('type'=>'wysiwyg', 'model'=>'Post')) }}>Create New Post</button>
						</div>
					<?php } ?>

					<div class="panel" style="background-color:#f8f8f8;">
						<iframe height="160" style="width: 100%; border:0px none;" src="http://pages.usablenet.com/Industry-Bites-Sign-up_registration-news-blog.html" scrolling="no" id="Newsletter_Signup"></iframe>
					</div>

					<div class="panel">
						<div class="panel-title">
							Featured Product Resources
						</div>
						<div class="panel-body">
							<ul>
								@foreach($products as $product)
									<li>
										<a href="{{ URL::action('ProductController@show', $product->slug) }}">{{ $product->title }}</a>
									</li>
								@endforeach
							</ul>

							<a class="circle-arrow-link blue" href="{{ URL::to('products') }}"><span class="ml">View All Products</span></a>
						</div>
					</div>


					<div class="panel">
						<div class="panel-title">
							Featured Case Studies
						</div>
						<div class="panel-body">
							<ul>
								@foreach($clients as $client)
									<li>
										<a href="{{ URL::action('ClientController@show', $client->slug) }}">{{ $client->company_name }}</a>
									</li>
								@endforeach
							</ul>

							<a class="circle-arrow-link blue" href="{{ URL::to('clients') }}"><span class="ml">View All Case Studies</span></a>
						</div>
					</div>
				</div>
				<div class="span8 span-dt-12 span-tab-12 fr-tab">

					<div class="row-fluid">
						<h1 class="mb h20 tk-proxima-nova">{{$title}}</h1>
					</div>

					@foreach ($posts as $i => $post)
					<div class="row-fluid mb h40">

						<div class="row-fluid">

							<div class="span3">
								@if($post->image_thumb->src != '' && $post->image_thumb->src != null)
								<img src="{{ URL::asset($post->image_thumb->src) }}" alt="{{ $post->image_thumb->alt }}" class="responsive" />
								@endif
							</div>

							<div class="span9 tk-proxima-nova">
								<div>
									<div class="news-index-date ">{{ strtoupper(date('M d Y', strtotime($post->posted_on))) }}</div>
									<div class="news-index-title">
										<?php

											$title = preg_match_all("/<p>(.*)<\/p>/",$post->title,$matches);

										if (isset($matches[1]) && isset($matches[1][0]))
										echo strip_tags($matches[1][0]);

										if (isset($matches[1]) && isset($matches[1][1]))
										echo '<h1 class="news-title">'.  strip_tags($matches[1][1]) . '</h1>';
										?>
									</div>
									<p>
										{{$post->headline}}<br>
										<a href="{{ URL::action('PostController@show', $post->slug) }}"><strong>Read Article</strong></a>
									</p>
									<div></div>
									<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<div class="container">
											<span class="tms-editor" {{ $post->editor('title', 'null', 'edit_post_'.$post->id, array('type'=>'wysiwyg'))}}></span>
											<span class="tms-editor" {{ $post->editor('slug', 'null', 'edit_post_'.$post->id)}}></span>
											<span class="tms-editor" {{ $post->editor('category_id', 'null', 'edit_post_'.$post->id, array('type'=>'select', 'options'=>$categoryList)) }}></span>
											<span class="tms-editor" {{ $post->editor('author_id', 'null', 'edit_post_'.$post->id, array('type'=>'select', 'options'=>$authors)) }}></span>
											<span class="tms-editor" {{ $post->editor('image_thumb', 'null', 'edit_post_'.$post->id, array('type'=>'image', 'fieldLabel' => 'Image Thumb: 150x100')) }}></span>
											<span class="tms-editor" {{ $post->editor('main_image', 'null', 'edit_post_'.$post->id, array('type'=>'image')) }}></span>
											<span class="tms-editor" {{ $post->editor('posted_on', 'null', 'edit_post_'.$post->id, array('type'=>'date')) }}></span>
											<span class="tms-editor" {{ $post->editor('meta_title', null, 'edit_post_'.$post->id) }}></span>
											<span class="tms-editor" {{ $post->editor('meta_keywords', null, 'edit_post_'.$post->id, array('type' => 'text')) }}></span>
											<span class="tms-editor" {{ $post->editor('meta_description', null, 'edit_post_'.$post->id) }}></span>
											<span class="tms-editor" {{ $post->editor('headline', 'null', 'edit_post_'.$post->id)}}></span>
											<button class="tms-editor tms-btn-success" {{ $post->editor('article', 'null', 'edit_post_'.$post->id, array('type'=>'wysiwyg')) }}>Edit This Post</button>

											<button class="tms-editor tms-btn-danger" {{ $post->deleteThing(null) }}>Remove Post</button>
										</div>
									<?php } ?>
								</div>

							</div>
						</div>
					</div>

					@endforeach

					<div class="row-fluid">
						<div class="span12">
							{{ $posts->links('usablenet.paginator') }}
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop