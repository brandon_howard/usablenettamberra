<section class="section full-width padding bottom contact-us">
	<div class="container">
		<div class="row-fluid">
			<div class="span6 left-col">
				<h3 class="title mbz tms-editor tms-inline" {{ $page->editor('contact_us_title_left', 'inline') }}>{{ $page->contact_us_title_left }}</h3>

				<div class="row-fluid">
					<ul class="small-case-study-grid">
						@for ($i=0; $i < 4; $i++)

						<li class="span6 mlz span-tab-12 span-dt-6">
							@if(isset($locations[$i]))
							<div>
								<h3>{{ $locations[$i]->name }}</h3>
								<p>
									<img src="{{ URL::asset($locations[$i]->map->src) }}" alt="{{ $locations[$i]->map->alt }}" width="200" class="responsive"><br>
									{{ $locations[$i]->street_address }} {{ $locations[$i]->street_address_2 }}<br>
									{{ $locations[$i]->city }}, {{ $locations[$i]->state }} {{ $locations[$i]->zipcode }}
								</p>
							</div>
							@endif
						</li>

						@endfor
					</ul>
				</div>
			</div>

			<div class="span6 right-col">

				@if(Session::has('message-success-contact-form-offices'))
					<h3 class="title mb h20" id="contact">Thank You. We will contact you shortly</h3>
				@else
					<h3 class="title mbz" id="contact">Contact Us</h3>
				@endif

				@if(Session::has('message-fail-contact-form-offices'))
					<h4 class="text-error title mb h20">{{ Session::get('message-fail-contact-form-offices') }}</h4>
				@endif

				<div class="mt h20">
					<div class="inline-anchor"><div id="contact-form-offices"></div></div>

					{{ Form::open(array('action' => 'ContactController@email')) }}
						{{Form::hidden('program_name', 'Website Contact Us')}}
						{{Form::hidden('from', 'contact-form-offices')}}

						<div class="clear h1">&nbsp;</div>

						<div class="row-fluid">
							<div class="span12">
								{{ Form::text('FirstName', null, array('class' => 'input-block-level', 'placeholder' => 'First Name')) }}
								{{ $errors->first('FirstName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('LastName', null, array('class' => 'input-block-level', 'placeholder' => 'Last Name')) }}
								{{ $errors->first('LastName'); }}

							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Email', null, array('class' => 'input-block-level', 'placeholder' => 'Email Address')) }}
								{{ $errors->first('Email'); }}

							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Company', null, array('class' => 'input-block-level', 'placeholder' => 'Organization\'s Name')) }}
								{{ $errors->first('Company'); }}

							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::textarea('ContactUs_Message__c', null, array('class' => 'input-block-level', 'placeholder' => 'Message', 'rows' => '2')) }}

							</div>
						</div>

						<div class="row-fluid">
							<div class="span4 fr">
								<button type="submit" class="ptl pbl btn btn-success btn-flat btn-block mt h20">CONTACT US</button>
							</div>
						</div>
					{{Form::close()}}
				</div>
			</div>

		</div>
	</div>
</section>