@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')

	<section class="section full-width breadcrumbs">
		<div class="container">
			<div class="row-fluid">
				<div class="span2 section-name">
					<p>News &amp; Events</p>
				</div>
				<div class="span10 crumbs mz">
					<ul class="mz">
						{{ Usablenet::buildcrumbs(URL::current()) }}
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section id="usablenet-blog-post" class="section full-width post">
		<div class="container">
			<div class="row-fluid">
				<?php if ( ! is_null($post) && ! empty($post) ) : ?>
					<div class="span2 text-right post-date">
						<p><strong>{{ date('M j', strtotime($post->posted_on)) }}</strong><br />
						{{ date('Y', strtotime($post->posted_on)) }}
					</div>
					<div class="span7 span-dt-10 span-tab-10 mb">
						<div class="title mtz mb h40 post-title">
							<?php

												$title = preg_match_all("/<p>(.*)<\/p>/",$post->title,$matches);

											if (isset($matches[1]) && isset($matches[1][0]))
											echo '<h2>'.strip_tags($matches[1][0]) . '</h2>';

											if (isset($matches[1]) && isset($matches[1][1]))
											echo '<h1>'.  strip_tags($matches[1][1]) . '</h1>';
											?>
						</div>

						@if($post->main_image->src !== '' && $post->main_image->src !== null)
						<img src="{{ URL::asset($post->main_image->src) }}" alt="{{ $post->main_image->alt }}" class="responsive">
						@endif

						<div class="mt h20"><!-- WYSIWYG -->
							{{ $post->article }}
						</div>
					</div>
				<?php else : ?>
					<div class="span2 text-right">
						There is no news article with this title
					</div>
				<?php endif; ?>
				<div class="span3 span-dt-12 span-tab-12 text-center">

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<div class="container">
							<span class="tms-editor" {{ $post->editor('title', 'null', 'edit_post', array('type'=>'wysiwyg'))}}></span>
							<span class="tms-editor" {{ $post->editor('slug', 'null', 'edit_post')}}></span>
							<span class="tms-editor" {{ $post->editor('category_id', 'null', 'edit_post', array('type'=>'select', 'options'=>$categoryList)) }}></span>
							<span class="tms-editor" {{ $post->editor('author_id', 'null', 'edit_post', array('type'=>'select', 'options'=>$authors)) }}></span>
							<span class="tms-editor" {{ $post->editor('image_thumb', 'null', 'edit_post', array('type'=>'image', 'fieldLabel' => 'Image Thumb: 150x100')) }}></span>
							<span class="tms-editor" {{ $post->editor('main_image', 'null', 'edit_post', array('type'=>'image')) }}></span>
							<span class="tms-editor" {{ $post->editor('posted_on', 'null', 'edit_post', array('type'=>'date')) }}></span>
							<span class="tms-editor" {{ $post->editor('meta_title', null, 'edit_post') }}></span>
							<span class="tms-editor" {{ $post->editor('meta_keywords', null, 'edit_post', array('type' => 'text')) }}></span>
							<span class="tms-editor" {{ $post->editor('meta_description', null, 'edit_post') }}></span>
							<span class="tms-editor" {{ $post->editor('headline', 'null', 'edit_post')}}></span>
							<button class="tms-editor tms-btn-success" {{ $post->editor('article', 'null', 'edit_post', array('type'=>'wysiwyg')) }}>Edit This Post</button>

							<button class="tms-editor tms-btn-danger" {{ $post->deleteThing(null) }}>Remove Post</button>
						</div>
					<?php } ?>

					@if(isset($post->author) && $post->author != null && $post->author != '')
					<div class="bt bb">
						<img class="mt h20" src="{{ URL::asset('images/profile-'.str_replace(' ', '-', $post->author->name).'.jpg') }}">
						<p class="mt h20">By</p>
						<p class="sub-title mb">{{ $post->author->name }}</p>
						<p>{{ $post->author->title }}</p>
						<p class="sub-title mt h20">Share</p>
						<div class="addthis_toolbox addthis_default_style addthis_32x32_style" style="width:200px;margin:0 auto 20px;">
							<a class="addthis_button_facebook"></a>
							<a class="addthis_button_twitter"></a>
							<a class="addthis_button_google_plusone_share"></a>
							<a class="addthis_button_pinterest_share"></a>
							<a class="addthis_button_linkedin"></a>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</section>

@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop