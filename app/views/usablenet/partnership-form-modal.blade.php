<section class="full-width bottom contact-us">
		<div class="form-body">
			<h3 class="title mbz" id="contact">Partnership Request</h3>
			<div class="form-wrap">
				{{ Form::open(array('action' => 'ContactController@email', 'id'=>'partnership-m-form', 'class'=>'modal-form')) }}
					{{Form::hidden('program_name', 'Website Partnership Request')}}
					{{Form::hidden('from', 'contact-form-partner')}}
					<div class="clear h1">&nbsp;</div>
					<div class="row-fluid">
						<div class="span12">
							I'm interested in:
						</div>
						<div class="span12">
							<label for="p-interest1">
								<input type="radio" name="interest" id="p-interest1" value="Product Information" checked>
								Product Information
							</label>
							<label for="p-interest2">
								<input type="radio" name="interest" id="p-interest2" value="Becoming A Partner">
								Becoming a Partner
							</label>
							<label for="p-interest3">
								<input type="radio" name="interest" id="p-interest3" value="Free Consultation">
								Free Consultation
							</label>
						</div>
					</div>
					<div class="row-fluid mt h20">
						<div class="span12">
							{{ Form::text('FirstName', null, array('class' => 'input-block-level', 'placeholder' => 'First Name')) }}
							{{ $errors->first('FirstName'); }}
						</div>
					</div>
					<div class="row-fluid mt h20">
						<div class="span12">
							{{ Form::text('LastName', null, array('class' => 'input-block-level', 'placeholder' => 'Last Name')) }}
							{{ $errors->first('LastName'); }}
						</div>
					</div>
					<div class="row-fluid mt h20">
						<div class="span12">
							{{ Form::text('Email', null, array('class' => 'input-block-level', 'placeholder' => 'Email Address')) }}
							{{ $errors->first('Email'); }}
						</div>
					</div>
					<div class="row-fluid mt h20">
						<div class="span12">
							{{ Form::text('Company', null, array('class' => 'input-block-level', 'placeholder' => 'Organization\'s Name')) }}
							{{ $errors->first('Company'); }}
						</div>
					</div>
					<div class="row-fluid mt h20">
						<div class="span12">
							{{ Form::textarea('ContactUs_Message__c', null, array('class' => 'input-block-level', 'placeholder' => 'Message', 'rows' => '2')) }}
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4 fr">
							<button id="partnership-m-submit" type="button" class="ptl pbl btn btn-success btn-flat btn-block mt h20">SEND</button>
							<div class="hide submitting" style="margin-top:1.5rem;">Submitting...</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
</section>
<a href="#" class="modal-close-btn" data-modal-close>&times;</a>
