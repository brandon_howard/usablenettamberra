<section class="section full-width padding bottom">
	<div class="container" style="
    border-top: 1px solid #e5e5e5;
">
		<div class="row-fluid">
			<div class="span12 left-col">
				<h3 class="title mbz" style="
    margin-bottom: 30px;
    margin-top: 40px;
">Case Studies</h3>
 
				<div class="row-fluid">
					<div class="span4">
						<a href="/clients/fedex">
						<img class="responsive" alt="fedex logo" src="{{ URL::to('images/casestudy-image-1.png')}}">
						</a>
					</div>
					<div class="span4">
						<a href="/clients/fedex">
						<img class="responsive" alt="Hyatt logo" src="{{ URL::to('images/casestudy-image-2.png')}}">
						</a>
					</div>
					<div class="span4"><a href="/clients/fedex">
						<img class="responsive" alt="Dell logo" src="{{ URL::to('images/casestudy-image-3.png')}}">
						</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>