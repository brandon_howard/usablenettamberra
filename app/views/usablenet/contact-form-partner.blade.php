<section class="section full-width padding bottom contact-us-partner"  style="background-color:{{$page->contact_us_partner_background_color}};background-image:url({{ URL::asset($page->contact_us_partner_background->src) }});color:{{ $page->contact_us_partner_text_color }}">
	<div class="container">

		<div class="row-fluid">
			<div class="span6 left-col">
				<h3 class="title mbz tms-editor tms-inline" {{ $page->editor('contact_us_partner_title_left', 'inline') }}>{{ $page->contact_us_partner_title_left }}</h3>

				<div class="mt h20 tms-editor tms-inline" {{ $page->editor('contact_us_partner_description_1', 'inline') }}>{{ $page->contact_us_partner_description_1 }}</div>
			</div>

			<div class="span6 right-col">
				<div class="inline-anchor"><div id="contact-form-partner"></div></div>
				<div class="mt h20">
					@if(Session::has('message-fail-contact-form-partner'))
						<h4 class="text-error title mb h20">{{ Session::get('message-fail-contact-form-partner') }}</h4>
					@elseif(Session::has('message-success-contact-form-partner'))
						<h3 class="title mb h20" id="contact">Thank You. We will contact you shortly.</h3>
					@endif

					{{ Form::open(array('action' => 'ContactController@email')) }}
						{{Form::hidden('program_name', 'Website Partnership Request')}}
						{{Form::hidden('from', 'contact-form-partner')}}

						<div class="clear h1">&nbsp;</div>

						<div class="row-fluid">
							<div class="span12">
								{{ Form::text('FirstName', null, array('class' => 'input-block-level', 'placeholder' => 'First Name')) }}
								{{ $errors->first('FirstName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('LastName', null, array('class' => 'input-block-level', 'placeholder' => 'Last Name')) }}
								{{ $errors->first('LastName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Email', null, array('class' => 'input-block-level', 'placeholder' => 'Email Address')) }}
								{{ $errors->first('Email'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Company', null, array('class' => 'input-block-level', 'placeholder' => 'Organization\'s Name')) }}
								{{ $errors->first('Company'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::textarea('ContactUs_Message__c', null, array('class' => 'input-block-level', 'placeholder' => 'Message', 'rows' => '2')) }}
							</div>
						</div>

						<div class="row-fluid">
							<div class="span4 fr">
								<button type="submit" class="ptl pbl btn btn-success btn-flat btn-block mt h20">Send</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>

		</div>

		<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn" {{ $page->editor('contact_us_partner_background', '.contact-us-partner', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
		<button class="tms-editor tms-btn" {{ $page->editor('contact_us_partner_text_color', '.contact-us-partner', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
		<button class="tms-editor tms-btn" {{ $page->editor('contact_us_partner_background_color', '.contact-us-partner', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
		<?php } ?>
	</div>
</section>