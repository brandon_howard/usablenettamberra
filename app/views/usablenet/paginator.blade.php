@if ($paginator->getLastPage() > 1)
<div class="btn-group pages">
	<a href="{{ $paginator->getUrl(1) }}"
		class="btn item{{ ($paginator->getCurrentPage() == 1) ? ' disabled' : '' }}">
		<i class="icon left arrow"></i> Previous
	</a>
	<?php
		$max = 8;
		if ($paginator->getLastPage() > $max && ($paginator->getCurrentPage() - $max) > 1 )
		{
			$start = $paginator->getCurrentPage() - ($max / 2);
		}
		else
		{
			$start = 1;
		}


		if (($paginator->getCurrentPage() + $max) > $paginator->getLastPage() )
		{
			$end = $paginator->getLastPage();
		}
		else
		{
			$end = $paginator->getCurrentPage() + ($max / 2);
		}
	?>

	@for ($i = $start; $i <= $end; $i++)
		<a href="{{ $paginator->getUrl($i) }}" class="btn item{{ ($paginator->getCurrentPage() == $i) ? ' current' : '' }}">
				{{ $i }}
		</a>
	@endfor
	<a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}" class="btn item{{ ($paginator->getCurrentPage() == $paginator->getLastPage()) ? ' disabled' : '' }}">
				Next <i class="icon right arrow"></i>
			</a>
</div>
@endif