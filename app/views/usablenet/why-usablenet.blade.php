@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')
	<section tabindex="0" class="section full-width page-title-slice" style="background-color:{{$page->title_background_color}};background-image:url({{ URL::asset($page->title_background->src) }});color:{{ $page->title_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">
					<h1 class="sub-title tms-editor mbz tms-inline" {{ $page->editor('title_subtitle', 'inline') }}>{{ $page->title_subtitle }}</h1>
					<h2 class="title tms-editor tms-inline" {{ $page->editor('title_title', 'inline') }}>{{ $page->title_title }}</h2>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background', '.page-title-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_text_color', '.page-title-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background_color', '.page-title-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section tabindex="0" class="section full-width page-two_column-slice" style="background-color:{{$page->two_column_background_color}};background-image:url({{ URL::asset($page->two_column_background->src) }});color:{{ $page->two_column_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<h3 class="title tms-editor tms-inline" {{ $page->editor('two_column_subtitle', 'inline') }}>{{ $page->two_column_subtitle }}</h3> 

				</div>
			</div>
			<div class="row-fluid mt h20">
				<div class="span6">
					<p class="fs24 tms-editor tms-inline" {{ $page->editor('two_column_title', 'inline') }}>{{ $page->two_column_title }}</p>
				</div>
				<div class="span6 col-right">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_title', 'inline') }}><strong>{{ $page->two_column_column_2_title }}</strong></div>
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_description', 'inline') }}>{{ $page->two_column_column_2_description }}</div>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_background', '.page-two_column-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_text_color', '.page-two_column-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_background_color', '.page-two_column-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>


	<section tabindex="0" class="section full-width two-column-phone" style="background-image:url({{ URL::asset($page->two_column_phone_background->src) }});background-color:{{$page->two_column_phone_background_color}};color:{{ $page->two_column_phone_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span7">
					@if($page->two_column_phone_title !== '' || TmsAuth::isInGroup('Administration'))
					<h4 class="title small tms-editor tms-inline" {{ $page->editor('two_column_phone_title', 'inline') }}><strong>{{ $page->two_column_phone_title }}</strong></h4>
					@endif
					@if($page->two_column_phone_subtitle !== '' || TmsAuth::isInGroup('Administration'))
					<h3 class="title tms-editor tms-inline" {{ $page->editor('two_column_phone_subtitle', 'inline') }}>{{ $page->two_column_phone_subtitle }}</h3>
					@endif
				</div>
			</div>
			<div class="row-fluid mt">
				<div class="span6">
					<img class="tms-editor tms-inline" src="{{ URL::asset($page->two_column_phone_column_1_image->src) }}" alt="{{ $page->two_column_phone_column_1_image->alt }}" {{ $page->editor('two_column_phone_column_1_image', 'inline', null, array('updateAttribute' => 'src', 'fieldLabel'=>'Image Size: 525x274')) }}>
					<div class="fs24 mt h40"><span class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_1_title', 'inline') }}>{{ $page->two_column_phone_column_1_title }}</span></div>
				</div>
				<div class="span6">
					<img class="tms-editor tms-inline" src="{{ URL::asset($page->two_column_phone_column_2_image->src) }}" alt="{{ $page->two_column_phone_column_2_image->alt }}" {{ $page->editor('two_column_phone_column_2_image', 'inline', null, array('updateAttribute' => 'src', 'fieldLabel'=>'Image Size: 525x274')) }}>
					<div class="fs24 mt h40"><span class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_2_title', 'inline') }}>{{ $page->two_column_phone_column_2_title }}</span></div>
				</div>
			</div>
			<div class="row-fluid mt h20">
				<div class="span6 col-left">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_1_description', 'inline') }}>{{ $page->two_column_phone_column_1_description }}</div>
				</div>
				<div class="span6 col-right">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_phone_column_2_description', 'inline') }}>{{ $page->two_column_phone_column_2_description }}</div>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_background', '.two-column-phone', null, array('updateAttribute' => 'style[background-image]')) }}>Edit Background Image</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_text_color', '.two-column-phone', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_background_color', '.two-column-phone', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>

			<div>
				@if($page->two_column_phone_column1_link->fieldLabel != '' || TmsAuth::isInGroup('Administration'))
				<a class="circle-arrow-link blue" id="two_column_phone_column1_link" href="{{ $page->two_column_phone_column1_link->href }}" target="{{ $page->two_column_phone_column1_link->target }}"><span class="ml">{{ $page->two_column_phone_column1_link->fieldLabel }}</span></a>
				@endif

				<?php if(TmsAuth::isInGroup('Administration')) { ?>
				<button class="tms-editor tms-btn" {{ $page->editor('two_column_phone_column1_link', '#two_column_phone_column1_link') }}>Edit Link</button>
				<?php } ?>
			</div>
		</div>
	</section>

	<a name="contact"></a>
@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop