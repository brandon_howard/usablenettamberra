@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')

	<section class="section full-width breadcrumbs">
		<div class="container">
			<div class="row-fluid">
				<div class="span2 section-name">
					<p>Partnerships</p>
				</div>
				<div class="span10 crumbs mz">
					<ul class="mz">
						{{ Usablenet::buildcrumbs(URL::current()) }}
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="section full-width four-column">
		<div class="container">
			<div class="row-fluid">
				<div class="span12 mb">
					<h3 class="sub-title ptz mz">Complete Partner List</h3>
				</div>
			</div>

			<?php
				$numberOfPartners = count($partners);
				$partnersPerColumn = $numberOfPartners/4;
			?>

			<div class="row-fluid mt h40">
				<div class="span3 br">
					<ul>
						@for ($i=(int)floor(($partnersPerColumn*1)-$partnersPerColumn); $i < (int)floor($partnersPerColumn * 1) + 1; $i++)
							@if (isset($partners[$i]))
								@if ($partners[$i]->fully_published)
									<li>
										<a class="blue" href="{{ URL::action('PartnerController@show', $partners[$i]->id) }}">{{$partners[$i]->name}}</a>
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@else
									<li>
										{{$partners[$i]->name}}
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@endif
							@endif
						@endfor
					</ul>
				</div>
				<div class="span3 br">
					<ul>
						@for ($i=(int)floor(($partnersPerColumn*2)-$partnersPerColumn+1); $i < (int)floor($partnersPerColumn * 2) + 1; $i++)
							@if (isset($partners[$i]))
								@if ($partners[$i]->fully_published)
									<li>
										<a class="blue" href="{{ URL::action('PartnerController@show', $partners[$i]->id) }}">{{$partners[$i]->name}}</a>
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@else
									<li>
										{{$partners[$i]->name}}
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@endif
							@endif
						@endfor
					</ul>
				</div>
				<div class="span3 br">
					<ul>
						@for ($i=(int)floor(($partnersPerColumn*3)-$partnersPerColumn+1); $i < (int)floor($partnersPerColumn * 3) + 1; $i++)
							@if (isset($partners[$i]))
								@if ($partners[$i]->fully_published)
									<li>
										<a class="blue" href="{{ URL::action('PartnerController@show', $partners[$i]->id) }}">{{$partners[$i]->name}}</a>
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@else
									<li>
										{{$partners[$i]->name}}
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@endif
							@endif
						@endfor
					</ul>
				</div>
				<div class="span3">
					<ul>
						@for ($i=(int)floor(($partnersPerColumn*4)-$partnersPerColumn+1); $i < (int)floor($partnersPerColumn * 4) + 1; $i++)
							@if (isset($partners[$i]))
								@if ($partners[$i]->fully_published)
									<li>
										<a class="blue" href="{{ URL::action('PartnerController@show', $partners[$i]->id) }}">{{$partners[$i]->name}}</a>
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@else
									<li>
										{{$partners[$i]->name}}
										<?php if(TmsAuth::isInGroup('Administration')) { ?>
										<button class="tms-editor tms-btn-danger" {{ $partners[$i]->deleteThing(null) }}><i class="icon-trash icon-white"></i></button>
										<?php } ?>
									</li>
								@endif
							@endif
						@endfor
					</ul>
				</div>
			</div>
		</div>
	</section>

	<?php if(TmsAuth::isInGroup('Administration')) { ?>
	<section class="section full-width relative two-column">
		<div class="container">
			<div class="alert alert-block">
				<h3>Attention Administrator</h3>
				<h4>By editing the partner contact form below you will edit all other partners and the partnerships page in the same place</h4>
			</div>
		</div>
	</section>
	<?php } ?>

	@include('usablenet.contact-form-partner')
@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop