@extends('layouts.master')

@section('main')

	<section class="section full-width breadcrumbs">
		<div class="container">
			<div class="row-fluid">
				<div class="span2 section-name">
					<p>Usablenet.com</p>
				</div>
				<div class="span10 crumbs mz">

				</div>
			</div>
		</div>
	</section>
	<section class="section full-width missing-slice">
		<div class="container">
			<div class="row-fluid">
				<div class="span12 span-dt-10 span-tab-10 mb">
					<h4>404 PAGE NOT FOUND</h4>
					<h3 class="title"><strong>Sorry, this page isn't available.</strong></h3>
					<div class="h40 mt">
						<a class="circle-arrow-link blue" href="javascript:window.history.back(-1);return false;"><span class="ml">Back to Previous Page</span></a>
						<a class="circle-arrow-link blue ml h40" href="/"><span class="ml">Go To Homepage</span></a>
					</div>
				</div>
			</div>
		</div>
	</section>

@stop