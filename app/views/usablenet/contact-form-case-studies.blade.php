<section class="section full-width padding bottom contact-us">
	<div class="container">
		<div class="row-fluid">
			<div class="span6 left-col">
				<h3 class="title mbz">Select Case Studies</h3>

				<div class="row-fluid">
					<div class="span12">
						<ul class="small-case-study-grid">
							@for ($i=0; $i < 4; $i++)

							<li class="span6 mlz span-tab-12 span-dt-6">
								<div>
									<a href="{{ URL::action('ClientController@show', $caseStudies[$i]->slug) }}"><img src="{{ URL::asset($caseStudies[$i]->small_logo->src) }}" width="124" height="88" alt="{{$caseStudies[$i]->small_logo->alt}}" class="title"></a>
								</div>
							</li>

							@endfor
						</ul>
					</div>
				</div>
			</div>

			<div class="span6 right-col">
				<div class="inline-anchor"><div id="footer-contact"></div></div>


				@if(Session::has('message-success-footer-contact'))
					<h3 class="title mbz" id="contact">Thank You. We will contact you shortly.</h3>
				@else
					<h3 class="title mbz" id="contact">Contact Us</h3>
				@endif

				@if(Session::has('message-fail-footer-contact'))
					<h4 class="text-error title mbz">{{ Session::get('message-fail-footer-contact') }}</h4>
				@endif

				<div class="mt h20">
					{{ Form::open(array('action' => 'ContactController@email')) }}
						{{Form::hidden('program_name', 'Website Contact Us')}}
						{{Form::hidden('from', 'footer-contact')}}
						<div class="clear h1">&nbsp;</div>

						<div class="row-fluid">
							<div class="span12">
								{{ Form::text('FirstName', null, array('class' => 'input-block-level', 'placeholder' => 'First Name')) }}
								{{ $errors->first('FirstName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('LastName', null, array('class' => 'input-block-level', 'placeholder' => 'Last Name')) }}
								{{ $errors->first('LastName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Email', null, array('class' => 'input-block-level', 'placeholder' => 'Email Address')) }}
								{{ $errors->first('Email'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Company', null, array('class' => 'input-block-level', 'placeholder' => 'Organization\'s Name')) }}
								{{ $errors->first('Company'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::textarea('ContactUs_Message__c', null, array('class' => 'input-block-level', 'placeholder' => 'Message', 'rows' => '2')) }}
							</div>
						</div>

						<div class="row-fluid">
							<div class="span4 fr">
								<button type="submit" class="ptl pbl btn btn-success btn-flat btn-block mt h20">Send</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>

		</div>
	</div>
</section>