@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')
	<section class="section full-width page-title-slice" style="background-color:{{$page->title_background_color}};background-image:url({{ URL::asset($page->title_background->src) }});color:{{ $page->title_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">
					<h1 class="sub-title tms-editor tms-inline light-gray mbz" {{ $page->editor('title_subtitle', 'inline') }}>{{ $page->title_subtitle }}</h1>
					<h2 class="title tms-editor tms-inline" {{ $page->editor('title_title', 'inline') }}>{{ $page->title_title }}</h2>

				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background', '.page-title-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_text_color', '.page-title-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background_color', '.page-title-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width" id="case_studies_grid">

		<div class="container text-center">
			<div class="row-fluid">
				<div class="span7 tal">
					<h3 class="title mb h20 tms-editor tms-inline" {{ $page->editor('case_studies_title', 'inline') }}>{{ $page->case_studies_title }}</h3>
					<p class="title tms-editor tms-inline" {{ $page->editor('title_description', 'inline') }}>{{ $page->title_description }}</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 col-left text-left">
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<div class="container">

							<span class="tms-editor" {{ Tms::editor('company_name', '#case_studies_grid', 'new_case_study', array('model' => 'CaseStudy')) }}></span>
							<span class="tms-editor" {{ Tms::editor('slug', '#case_studies_grid', 'new_case_study', array('model' => 'CaseStudy')) }}></span>
							<span class="tms-editor" {{ Tms::editor('large_logo', '#case_studies_grid', 'new_case_study', array('type'=>'image', 'model' => 'CaseStudy', 'fieldLabel' => 'Large Logo: 350 x 233 - Transparent PNG')) }}></span>
							<span class="tms-editor" {{ Tms::editor('small_logo', '#case_studies_grid', 'new_case_study', array('type'=>'image', 'model' => 'CaseStudy', 'fieldLabel' => 'Large Logo: 124 x 88 - Transparent PNG')) }}></span>
							<span class="tms-editor" {{ Tms::editor('sort', '#case_studies_grid', 'new_case_study', array('model' => 'CaseStudy', 'fieldLabel' => 'Order of Appearance (Lower First)')) }}></span>
							<span class="tms-editor" {{ Tms::editor('image_1', '#case_studies_grid', 'new_case_study', array('type'=>'image', 'model' => 'CaseStudy', 'fieldLabel' => 'Large Logo: 350 x Any')) }}></span>
							<span class="tms-editor" {{ Tms::editor('image_2', '#case_studies_grid', 'new_case_study', array('type'=>'image', 'model' => 'CaseStudy', 'fieldLabel' => 'Large Logo: 350 x Any')) }}></span>
							<span class="tms-editor" {{ Tms::editor('image_3', '#case_studies_grid', 'new_case_study', array('type'=>'image', 'model' => 'CaseStudy', 'fieldLabel' => 'Large Logo: 350 x Any')) }}></span>
							<span class="tms-editor" {{ Tms::editor('meta_title', '#case_studies_grid', 'new_case_study', array('type'=>'text', 'model' => 'CaseStudy')) }}></span>
							<span class="tms-editor" {{ Tms::editor('meta_keywords', '#case_studies_grid', 'new_case_study', array('type'=>'textarea', 'model' => 'CaseStudy')) }}></span>
							<span class="tms-editor" {{ Tms::editor('meta_description', '#case_studies_grid', 'new_case_study', array('type'=>'textarea', 'model' => 'CaseStudy')) }}></span>

							<span class="tms-editor" {{ Tms::editor('headline', '#case_studies_grid', 'new_case_study', array('type'=>'text', 'model' => 'CaseStudy')) }}></span>
							<button class="tms-editor tms-btn-success fr" {{ Tms::editor('description', '#case_studies_grid', 'new_case_study', array('type'=>'wysiwyg', 'model' => 'CaseStudy')) }}>New Case Study</button>

						</div>
					<?php } ?>
				</div>
			</div>

			<div class="row-fluid mb h20 clients-grid">

			@for($i=0; $i<count($caseStudies); $i++)

				<div class="span4">
					<div class="border"><a href="{{ URL::action('ClientController@show', $caseStudies[$i]->slug) }}"><img src="{{ URL::asset($caseStudies[$i]->large_logo->src) }}" alt="{{ $caseStudies[$i]->large_logo->alt }}" /></a></div>
				</div>

			@endfor
			</div>
		</div>
	</section>

	<hr />
	<section class="section full-width four-column">
		<div class="container">
			<div class="row-fluid">
				<div class="span12 mb">
					<h3 class="title ptz mz tms-editor tms-inline" {{ $page->editor('client_list_title', 'inline') }}>{{ $page->client_list_title }}</h3>
				</div>
			</div>
			<div class="row-fluid mt h40 client-list">
				<div class="span3 br tms-editor tms-inline" {{ $page->editor('four_column_text_wysiwyg_1', 'inline') }}>{{ $page->four_column_text_wysiwyg_1 }}
				</div>
				<div class="span3 br tms-editor tms-inline" {{ $page->editor('four_column_text_wysiwyg_2', 'inline') }}>{{ $page->four_column_text_wysiwyg_2 }}
				</div>
				<div class="span3 br tms-editor tms-inline" {{ $page->editor('four_column_text_wysiwyg_3', 'inline') }}>{{ $page->four_column_text_wysiwyg_3 }}
				</div>
				<div class="span3 br tms-editor tms-inline" {{ $page->editor('four_column_text_wysiwyg_4', 'inline') }}>{{ $page->four_column_text_wysiwyg_4 }}
				</div>
			</div>
		</div>
	</section>

@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
		<div class="modal-body">
			<video id="video-player" controls="true" preload="none" width="527">
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video>
		</div>
	</div>
@stop