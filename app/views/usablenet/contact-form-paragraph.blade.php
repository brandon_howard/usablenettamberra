<section class="section full-width padding bottom contact-us" style="background-color:{{$page->contact_us_background_color}};background-image:url({{ URL::asset($page->contact_us_background->src) }});color:{{ $page->contact_us_text_color }}">
	<div class="container">
		<div class="row-fluid">
			<div class="span5 left-col">
				<h3 class="title mbz tms-editor tms-inline" {{ $page->editor('contact_us_title_left', 'inline') }}>{{ $page->contact_us_title_left }}</h3>
				<div class="mt h20 tms-editor tms-inline" {{ $page->editor('contact_us_description_1', 'inline') }}>{{ $page->contact_us_description_1 }}</div>
			</div>

			<div class="span7 right-col">

				@if(Session::has('message-success-contact-form-paragraph'))
					<h3 class="title mbz" id="contact">Thank You. We will contact you shortly</h3>
				@else
					<h3 class="title mbz" id="contact">Contact Us</h3>
				@endif

				@if(Session::has('message-fail-contact-form-paragraph'))
					<h4 class="text-error mbz">{{ Session::get('message-fail-contact-form-paragraph') }}</h4>
				@endif

				<div class="mt h20">
					<div class="inline-anchor"><div id="contact-form-paragraph"></div></div>

					{{ Form::open(array('action' => 'ContactController@email')) }}
						{{Form::hidden('program_name', 'Website Contact Us')}}
						{{Form::hidden('from', 'contact-form-paragraph')}}

						<div class="clear h1">&nbsp;</div>

						<div class="row-fluid">
							<div class="span12">
                                <label>First Name</label>
								{{ Form::text('FirstName', null, array('class' => 'input-block-level')) }}
								{{ $errors->first('FirstName'); }}
							</div>
						</div>
						<div class="row-fluid mt h20"> 
							<div class="span12">
                                <label>Last Name</label>
								{{ Form::text('LastName', null, array('class' => 'input-block-level')) }}
								{{ $errors->first('LastName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
                                <label>Email Adress</label>
								{{ Form::text('Email', null, array('class' => 'input-block-level')) }}
								{{ $errors->first('Email'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
                                <label>Organization's Name</label>
								{{ Form::text('Company', null, array('class' => 'input-block-level')) }}
								{{ $errors->first('Company'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
                                <label>Message</label>
								{{ Form::textarea('ContactUs_Message__c', null, array('class' => 'input-block-level', 'rows' => '2')) }}
							</div>
						</div>

						<div class="row-fluid">
							<div class="span4 fr">
								<button type="submit" class="slider-button mainHeader" style="text-align: center; font-size: 16px; width: 170px;">Contact Us</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>

		<?php if(TmsAuth::isInGroup('Administration')) { ?>
		<button class="tms-editor tms-btn" {{ $page->editor('contact_us_background', '.contact-us', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
		<button class="tms-editor tms-btn" {{ $page->editor('contact_us_text_color', '.contact-us', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
		<button class="tms-editor tms-btn" {{ $page->editor('contact_us_background_color', '.contact-us', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
		<?php } ?>
	</div>
</section>