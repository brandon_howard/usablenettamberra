@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')



<?php
	if($client->hero_slider_text_one || $client->hero_slider_text_two || $client->hero_slider_text_three){
?>
<section class="section full-width" style="
    padding-top: 0;
">
			<ul class="mz pz" id="slider-products-page">
<?php
	if($client->hero_slider_text_one){
?>
				<li class="slider-case-study" style="position: relative;">

	<section class="section full-width two-column-phone" id="sliderOne" style="height:500px;background-image:url(/<?php echo json_decode($client->hero_slider_image_one)->src; ?>);
																background-color:{{ $client->hero_slider_background_one }};
																color:#ffffff">

		<div class="container" style="position: relative;">
					<div class="row-fluid">
						<div class="span8" style="text-align: center;">
							<h3 class="title small" id="sliderOneText" style="max-width: 70%; margin: auto; font-size: 45px; margin-top: 60px; line-height: 45px;">{{ $client->hero_slider_text_one }}</h1>
						</div>
					</div>
		</div>
	</section>

				</li>

				<?php } ?>
<?php
	if($client->hero_slider_text_two){
?>
				<li class="slider-case-study" style="position: relative;">

	<section class="section full-width two-column-phone" id="sliderTwo" style="height:500px;background-image:url(/<?php echo json_decode($client->hero_slider_image_two)->src; ?>);background-color:{{ $client->hero_slider_background_two }};color:#ffffff">
		<div class="container" style="position: relative;">
					<div class="row-fluid">
						<div class="span8" style="text-align: center;">
							<h2 id="SliderTextTwo" class="title small" style="max-width: 70%; margin: auto; font-size: 45px; margin-top: 60px; line-height: 45px;">{{ $client->hero_slider_text_two }}</h2>
						</div>
					</div>
		</div>
	</section>

				</li>

				<?php } ?>
<?php
	if($client->hero_slider_text_three){
?>
				<li class="slider-case-study" style="position: relative;">

	<section class="section full-width two-column-phone" id="sliderThree" style="height:500px;background-image:url(/<?php echo json_decode($client->hero_slider_image_three)->src; ?>);background-color:{{ $client->hero_slider_background_three }};color:#ffffff">
		<div class="container" style="position: relative;">
					<div class="row-fluid">
						<div class="span8" style="text-align: center;">
							<h2 id="SliderTextThree" class="title small" style="max-width: 70%; margin: auto; font-size: 45px; margin-top: 60px; line-height: 45px;">{{ $client->hero_slider_text_three }}</h2>
						</div>
					</div>
		</div>
	</section>

				</li>

				<?php } ?>
			</ul>
</section>

<?php } ?>



	<?php if(TmsAuth::isInGroup('Administration')) { ?>
	<section class="section">
		<div class="container">

			<span class="tms-editor" {{ $client->editor('hero_slider_text_one', '#sliderOneText', 'homehero_1') }}></span>
			<button class="tms-editor tms-btn-success" {{ $client->editor('hero_slider_background_one', '#sliderOne', 'homehero_1', array('type'=>'color', 'updateAttribute'=>'style[background-color]')) }}>Manage Hero 1</button>
			<button class="tms-editor tms-btn" {{ $client->editor('hero_slider_image_one', '#sliderOne', 'homehero_1_image', array('type'=>'image', 'updateAttribute' => 'style[background-image]')) }}>Manage Hero 1 Image</button>

			<span class="tms-editor" {{ $client->editor('hero_slider_text_two', '#SliderTextTwo', 'homehero_2') }}></span>
			<button class="tms-editor tms-btn-success" {{ $client->editor('hero_slider_background_two', '#sliderTwo', 'homehero_2', array('type'=>'color', 'updateAttribute'=>'style[background-color]')) }}>Manage Hero 2</button>
			<button class="tms-editor tms-btn" {{ $client->editor('hero_slider_image_two', '#sliderTwo', 'homehero_2_image', array('type'=>'image', 'updateAttribute' =>  'style[background-image]')) }}>Manage Hero 2 Image</button>

			<span class="tms-editor" {{ $client->editor('hero_slider_text_three', '#SliderTextThree', 'homehero_3') }}></span>
			<button class="tms-editor tms-btn-success" {{ $client->editor('hero_slider_background_three', '#sliderThree', 'homehero_3', array('type'=>'color', 'updateAttribute'=>'style[background-color]')) }}>Manage Hero 3</button>
			<button class="tms-editor tms-btn" {{ $client->editor('hero_slider_image_three', '#sliderThree', 'homehero_3_image', array('type'=>'image', 'updateAttribute' =>  'style[background-image]')) }}>Manage Hero 3 Image</button>
						</div>
	</section>
	<?php } ?>
<div id="case_study">
	<section class="section full-width" style="
    padding-top: 0;
    margin-top: -30px;
">
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<?php if(TmsAuth::isInGroup('Administration')) { ?>

						<div class="container">


							<span class="tms-editor" {{ $client->editor('company_name', null, 'new_case_study') }}></span>

							<span class="tms-editor" {{ $client->editor('slug', null, 'new_case_study') }}></span>

							<span class="tms-editor" {{ $client->editor('large_logo', null, 'new_case_study', array('type'=>'image', 'fieldLabel'=>'Large Logo: 350x233 - Transparent PNG')) }}></span>

							<span class="tms-editor" {{ $client->editor('small_logo', null, 'new_case_study', array('type'=>'image', 'fieldLabel'=>'Small Logo: 124x88 - Transparent PNG')) }}></span>

							<span class="tms-editor" {{ $client->editor('sort', null, 'new_case_study', array('fieldLabel' => 'Order of Appearance (Lower First)')) }}></span>

							<span class="tms-editor" {{ $client->editor('image_1', null, 'new_case_study', array('type'=>'image')) }}></span>

							<span class="tms-editor" {{ $client->editor('image_2', null, 'new_case_study', array('type'=>'image')) }}></span>

							<span class="tms-editor" {{ $client->editor('image_3', null, 'new_case_study', array('type'=>'image')) }}></span>

							<span class="tms-editor" {{ $client->editor('meta_title', null, 'new_case_study') }}></span>

							<span class="tms-editor" {{ $client->editor('meta_keywords', null, 'new_case_study', array('type' => 'text')) }}></span>

							<span class="tms-editor" {{ $client->editor('meta_description', null, 'new_case_study') }}></span>

							<span class="tms-editor" {{ $client->editor('services', null, 'new_case_study', array('type'=>'text')) }}></span>

							<span class="tms-editor" {{ $client->editor('headline', null, 'new_case_study', array('type'=>'text')) }}></span>

							<button class="tms-editor tms-btn-success" {{ $client->editor('description', null, 'new_case_study', array('type'=>'wysiwyg')) }}>Edit This Case Study</button> <button class="tms-editor tms-btn-danger mt h10" {{ $client->deleteThing(null) }}>Remove Case Study</button>

						</div>



					<?php } ?>
					<h1 class="title"><strong>{{ $client->company_name }}</strong></h1>
					<p style="
    font-size: 20px;
    font-weight: 700;
">SHOWCASE: <?php if($client->section_one_link_one->fieldLabel != ''){ ?><span style="color: #0276CE;"><a id="section_one_link_one" href="{{ $client->section_one_link_one->url }}" target="{{ $client->section_one_link_one->target }}">{{ $client->section_one_link_one->fieldLabel }}</a></span> <?php } ?> @if($client->section_one_link_two->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_one_link_two" href="{{ $client->section_one_link_two->url }}" target="{{ $client->section_one_link_two->target }}">{{ $client->section_one_link_two->fieldLabel }}</a></span>@endif @if($client->section_one_link_three->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_one_link_three" href="{{ $client->section_one_link_three->url }}" target="{{ $client->section_one_link_three->target }}">{{ $client->section_one_link_three->fieldLabel }}</a></span>@endif @if($client->section_one_link_four->fieldLabel != '') / <span style="color: #0276CE;"><a id="section_one_link_four" href="{{ $client->section_one_link_four->url }}" target="{{ $client->section_one_link_four->target }}">{{ $client->section_one_link_four->fieldLabel }}</a></span>@endif /</p>

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<p style="color:red;">Leave labels blank to not show link.</p>
						<button class="tms-editor tms-btn" {{ $client->editor('section_one_link_one', '#section_one_link_one', 'section_one_link_one', array('type'=>'link')) }}>Edit Link One</button>
						<button class="tms-editor tms-btn" {{ $client->editor('section_one_link_two', '#section_one_link_two', 'section_one_link_two', array('type'=>'link')) }}>Edit Link Two</button>
						<button class="tms-editor tms-btn" {{ $client->editor('section_one_link_three', '#section_one_link_three', 'section_one_link_three', array('type'=>'link')) }}>Edit Link Three</button>
						<button class="tms-editor tms-btn" {{ $client->editor('section_one_link_four', '#section_one_link_four', 'section_one_link_four', array('type'=>'link')) }}>Edit Link Four</button>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<div class="client-description">
						<h2><strong class="tms-editor tms-inline" {{ $client->editor('challengeHeadline', 'inline') }} >{{ $client->challengeHeadline }}</strong></h2>
						<div class="tms-editor tms-inline" id="challenge"  {{ $client->editor('challenge', 'inline', 'challenge', array('type'=>'wysiwyg')) }} >{{ $client->challenge }}</div>
						@if($client->hugeQuote != '')
						<br>
						<h2 class="hugeQuote tms-editor tms-inline" id="hugeQuote"  {{ $client->editor('hugeQuote', 'inline', 'hugeQuote') }} >"{{ $client->hugeQuote }}"</h2>
						@endif
						@if($client->image_1->src != '')
						<br>
						<div class="centerBigImage">
							<img class="responsive tms-editor tms-inline" id="image_1" src="/{{ $client->image_1->src }}" alt="{{ $client->image_1->alt }}" {{ $client->editor('image_1', '#image_1', 'image_1', array('type'=>'image')) }}>
							<p class="tms-editor tms-inline" id="imageQuote"  {{ $client->editor('imageQuote', 'inline', 'imageQuote') }} >{{ $client->imageQuote }}</p>
						</div>
						@endif
						<br/>
						@if($client->main_column_video !== '' && $client->main_column_video !== null)
						<div id="play-video" class="jslink modal-video main_column_video about-us-video" data-video-url='{{ $client->main_column_video }}' style="position: relative;
    max-width: 80%;
    margin: 0 auto;">
							<img class="mt h20 responsive" id="main_column_video_image"  alt="video cover image" src="/{{ $client->main_column_video_image->src }}">
							<div class="video-overlayNew">
								<img class="playButtonInVideo" src="/assets/images/playButtonRectangle.png" alt="video play button icon" style="
    position: absolute;
    top: 27%;
    left: 38%;
" />
							</div>
						</div>
						@endif

						<?php if(TmsAuth::isInGroup('Administration')) { ?>
							<button class="tms-editor tms-btn-success" {{ $client->editor('main_column_video_title', '#main_column_video_title', 'main_column_video_group', array('fieldLabel' => 'Video Title')) }}>Manage Main Column Video Title</button>
		<button class="tms-editor tms-btn" {{ $client->editor('main_column_video_image', '#main_column_video_image', 'main_column_video_image', array('type' => 'image', 'updateAttribute'=>'src', 'fieldLabel' => 'Poster Image Link: (At Least 900px Wide, Any Height, Must be a Direct Link)')) }}>Manage Main Column Video Image</button>


						<button class="tms-editor tms-btn" {{ $client->editor('main_column_video', '.main_column_video', 'main_column_video_group_videofile', array('type' => 'video', 'updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video Link (Direct Link to .mp4 file)')) }}>Manage Main Column File</button>
						<?php } ?>
					</div>
						<h2 style="    font-size: 18px;
    margin-bottom: 0;"><strong class="tms-editor tms-inline"  {{ $client->editor('solutionHeadline', 'inline') }} >{{ $client->solutionHeadline }}</strong></h2>
						<div class="tms-editor tms-inline" id="solution"  {{ $client->editor('solution', 'inline', 'solution', array('type'=>'wysiwyg')) }} >{{ $client->solution }}</div>
						<br>
						<h2 style="    font-size: 18px;
    margin-bottom: 0;"><strong class="tms-editor tms-inline" {{ $client->editor('resultsHeadline', 'inline') }} >{{ $client->resultsHeadline }}</strong></h2>
						<div class="tms-editor tms-inline" id="results"  {{ $client->editor('results', 'inline', 'results', array('type'=>'wysiwyg')) }} >{{ $client->results }}</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section full-width two-column-phone" style="height:463px;background-color:#99cc66;color:#ffffff position: relative;
    overflow: visible;
">
		<div class="container">
			<div class="row-fluid">
				<div class="span7">
					<h4 class="title small"><strong class="tms-editor tms-inline" {{ $client->editor('resultsHeadlineTwo', 'inline') }} >{{ $client->resultsHeadlineTwo }}</strong></h4>
					<h3 style="
    font-size: 44px;
" class="title tms-editor tms-inline" {{ $client->editor('resultsHeadlineThree', 'inline') }}>{{ $client->resultsHeadlineThree }}</h3>
					<br>
					<h3 style="
    font-size: 44px;
" class="title tms-editor tms-inline" {{ $client->editor('resultsHeadlineFour', 'inline') }}>{{ $client->resultsHeadlineFour }}</h3>
					<br>
					<h3 style="
    font-size: 44px;
" class="title tms-editor tms-inline" {{ $client->editor('resultsHeadlineFive', 'inline') }}>{{ $client->resultsHeadlineFive }}</h3> </div>
			</div>
		</div>
		<div class="imageContainer" style="position: relative;"> <img class="case-study-ipad-image  tms-editor tms-inline" id="image_2" src="/{{ $client->image_2->src }}" alt="{{ $client->image_2->alt }}" {{ $client->editor('image_2', '#image_2', 'image_2', array('type'=>'image')) }} > </div>
	</section>
	<section class="section full-width padding bottom">
		<div class="container">
			<div class="row-fluid">
				<div class="span12 left-col">
					<h3 class="title mbz" style="
    margin-bottom: 30px;
    margin-top: 40px;
">More Case Studies</h3>
					<div class="row-fluid">
						<div class="span4">
							<a href="/clients/fedex"> <img class="responsive" src="http://45.55.170.194/images/casestudy-image-1.png"> </a>
						</div>
						<div class="span4">
							<a href="/clients/fedex"> <img class="responsive" src="http://45.55.170.194/images/casestudy-image-2.png"> </a>
						</div>
						<div class="span4">
							<a href="/clients/fedex"> <img class="responsive" src="http://45.55.170.194/images/casestudy-image-3.png"> </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@stop


@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" id="pause-video" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
		<div class="modal-body">
			<video id="video-player" autoplay="false" controls="true" preload="none" width="100%">
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video
		</div>
	</div>
@stop
