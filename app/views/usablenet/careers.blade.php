@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')

	<section class="section full-width page-title-slice" style="background-color:{{$page->title_background_color}};background-image:url({{ URL::asset($page->title_background->src) }});color:{{ $page->title_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">
					<h1 class="sub-title tms-editor light-gray mbz tms-inline" {{ $page->editor('title_subtitle', 'inline') }}>{{ $page->title_subtitle }}</h1>
					<h2 class="title tms-editor tms-inline" {{ $page->editor('title_title', 'inline') }}>{{ $page->title_title }}</h2>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background', '.page-title-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_text_color', '.page-title-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background_color', '.page-title-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width two-column-slice" style="background-color:{{$page->two_column_background_color}};color:{{ $page->two_column_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span8">
					<h3 class="title tms-editor tms-inline" {{ $page->editor('two_column_subtitle', 'inline') }}>{{ $page->two_column_subtitle }}</h3>
					<p class="fs24 lh15 tms-editor tms-inline" {{ $page->editor('two_column_title', 'inline') }}>{{ $page->two_column_title }}</p>
				</div>
			</div>
			<div class="row-fluid mt h40">
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_1_title', 'inline') }}><strong>{{ $page->two_column_column_1_title }}</strong></div>
				</div>
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_title', 'inline') }}><strong>{{ $page->two_column_column_2_title }}</strong></div>
				</div>
			</div>
			<div class="row-fluid mt">
				<div class="span6 col-left">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_1_description', 'inline') }}>{{ $page->two_column_column_1_description }}</div>
				</div>
				<div class="span6 col-right">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_description', 'inline') }}>{{ $page->two_column_column_2_description }}</div>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_text_color', '.two-column-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_background_color', '.two-column-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width two-column-video" style="height:450px;background-image:url({{ URL::asset($page->two_column_video_background->src) }});background-color:{{$page->two_column_video_background_color}};color:{{ $page->two_column_video_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span7">
					<h3 class="title small tms-editor tms-inline" style="
    font-size: 15px;
    letter-spacing: 0px;
" {{ $page->editor('two_column_video_title', 'inline') }}><strong>{{ $page->two_column_video_title }}</strong></h3>
					<h4 style="
    font-size: 36px;
    line-height: 1em; font-weight: 100;
" class="title tms-editor tms-inline" {{ $page->editor('two_column_video_subtitle', 'inline') }}>{{ $page->two_column_video_subtitle }}</h4>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 col-left">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_video_column_1_description', 'inline') }}>{{ $page->two_column_video_column_1_description }}</div>

					@if($page->two_column_video !== '' && $page->two_column_video !== null)
						<a class="jslink modal-video two_column_video circle-arrow-link blue" data-video-url='{{ $page->two_column_video }}'><span class="ml">{{$page->two_column_video_link}}</span></a>
					@endif

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<span class="tms-editor" {{ $page->editor('two_column_video_link', '#two_column_video_link', 'two_column_video_group', array('fieldLabel' => 'Video Link Text')) }}></span>
						<span class="tms-editor" {{ $page->editor('two_column_video_image', '#two_column_video_image', 'two_column_video_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File: Any Size')) }}></span>
						<button class="tms-editor tms-btn-success" {{ $page->editor('two_column_video', '.two_column_video', 'two_column_video_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File: Any Size')) }}>Manage Main Column Video</button>
					<?php } ?>

				</div> 
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_video_background', '.two-column-video', null, array('updateAttribute' => 'style[background-image]')) }}>Edit Background Image</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_video_text_color', '.two-column-video', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_video_background_color', '.two-column-video', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width" id="job_postings">
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<h2><strong>Jobs at Usablenet</strong></h2>
				</div>
			</div>
			<div class="row-fluid mt h40">
				<div class="span4">
					<ul class="mz content-menu list-unstyled">
						@for($i=0; $i < count($careers); $i++)
						<li class="mb h20 @if($i === 0) {{ 'blue active' }} @endif">
							<a class="cursor"><strong>{{$careers[$i]->job_title}}</strong></a>
							<?php if(TmsAuth::isInGroup('Administration')) { ?><br>
								<button class="tms-editor tms-btn-danger" {{ $careers[$i]->deleteThing('#job_postings') }}>Remove Job</button>
							<?php } ?>
						</li>
						@endfor
					</ul>
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<span class="tms-editor" {{ Tms::editor('job_title', '#job_postings', 'new_job_posting', array('model' => 'Career')) }}></span>
					<span class="tms-editor" {{ Tms::editor('description', '#job_postings', 'new_job_posting', array('type'=>'text', 'model' => 'Career')) }}></span>
					<button class="tms-editor tms-btn-success" {{ Tms::editor('responsibilities', '#job_postings', 'new_job_posting', array('type'=>'wysiwyg', 'model' => 'Career')) }}>Add Job Posting</button>
					<?php } ?>
				</div>

				<div class="span8 content-wrapper">
					@for($i=0; $i < count($careers); $i++)
					<div @if($i !== 0) {{ 'style="display:none"' }} @endif>
						<h3 class="tms-editor tms-inline fs24" style="     font-weight: 700;
    letter-spacing: 0px;
" {{ $careers[$i]->editor('job_title', 'inline') }}>{{$careers[$i]->job_title}}</h43>
						<h4 style="
    font-weight: 100;
    font-size: 21px;
" class="tms-editor lh13 h20 mt tms-inline" {{ $careers[$i]->editor('description', 'inline') }}>{{$careers[$i]->description}}</h4>
						<span class="tms-editor tms-inline" style="display:inline-block" {{ $careers[$i]->editor('responsibilities', 'inline', null, array('type'=>'wysiwyg')) }}>&nbsp;{{$careers[$i]->responsibilities}}</span>

						<a href="mailto:careers@usablenet.com?subject=Job Application: {{ $careers[$i]->job_title }}" class="ptl pbl btn btn-primary mt h20" target="_blank">Apply Now</a>
					</div>
					@endfor
				</div>
			</div>
		</div>
	</section>

@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" autoplay="true" controls="true" preload="none" width="100%">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop