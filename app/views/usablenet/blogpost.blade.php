@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')

	<section class="section full-width breadcrumbs">
		<div class="container">
			<div class="row-fluid">
				<div class="span2 section-name">
					<p>Blog</p>
				</div>
				<div class="span10 crumbs mz">
					<ul class="mz fl">
						{{ Usablenet::buildcrumbs(URL::current()) }}
					</ul>
                    <div class="fr">
                        <a href="{{ URL::action('PostController@blogRSS') }}"><img src="{{URL::asset('img/rss-subscribe.gif')}}" width="136" height="29" alt="Subscribe to the Usablenet Blog"></a>
                    </div>
				</div>
			</div>
		</div>
	</section>
	<section id="usablenet-blog-post" class="section full-width post relative two-column">
		<div class="container">
			<div class="row-fluid">

                <div class="span8 span-dt-12 span-tab-12 fr-tab">
                    <div class="row-fluid mb h40 blog-post">
                        <div class="row-fluid">
                            <div class="span12 tk-proxima-nova">
                                <div>
                                    <div class="news-index-date "><h2>{{ strtoupper(date('M j, Y', strtotime($post->posted_on))) }}</h2></div>
                                    <h1>{{ strip_tags($post->title) }}</h1>
                                    <h2>@if(isset($post->category)){{ $post->category->name }}@endif</h2>

                                    @if($post->main_image->src !== '' && $post->main_image->src !== null)
                                    <img src="{{ URL::asset($post->main_image->src) }}" alt="{{ $post->main_image->alt }}" class="responsive">

                                    <div class="tags-and-social row-fluid">
                                        <div class="row-fluid">
                                            <div class="span9 blog-tags">
                                                @if(count($post->tags) > 0)
                                                <img src="{{ URL::asset('img/tag.gif') }}" width="16" height="14" alt="Post Tags">
                                                <ul class="tags">
                                                    @foreach($post->tags as $tag)
                                                    <li><a href="{{ URL::action('PostController@search', array('all', 'all', $tag->name)) }}">{{ $tag->name }}</a></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </div>
                                            <div class="span3 blog-social-links">
                                                <div class="addthis_toolbox" style="width:200px;float:right">
                                                    <a class="add-this-custom addthis_button_facebook"><span class="facebook"></span></a>
                                                    <a class="add-this-custom addthis_button_twitter"><span class="twitter"></span></a>
                                                    <a class="add-this-custom addthis_button_google_plusone_share"><span class="googleplus"></span></a>
                                                    <a class="add-this-custom addthis_button_linkedin"><span class="linkedin"></span></a>
	                                                <a class="add-this-custom addthis_button_email"><span class="email"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(isset($post->author) && $post->author != null && $post->author != '')

                                    <p class="mt h20 strong">By @if(isset($post->author->link) && $post->author->link !== '')<span class="blue"><a href="{{ $post->author->link }}">{{ $post->author->name }}</a></span>@else {{ $post->author->name }} @endif</p>
                                    <?php if(TmsAuth::isInGroup('Administration')) { ?>
                                        <span class="tms-editor" {{ $post->author->editor('name', 'null', 'edit_author')}}></span>
                                        <span class="tms-editor" {{ $post->author->editor('link', 'null', 'edit_author')}}></span>
            							<button class="tms-editor tms-btn-success" {{ $post->author->editor('title', 'null', 'edit_author') }}>Edit This Author</button>


                                        <button class="tms-editor tms-btn-danger" {{ $post->author->deleteThing(null) }}>Remove Author</button>
                                    <?php } ?>

                                    @endif

                                    <p>
                                        {{ $post->article }}
                                    </p>
                                    <div class="tags-and-social row-fluid">
                                        <div class="row-fluid">
                                            <div class="span9 blog-tags">
                                                @if(count($post->tags) > 0)
                                                <img src="{{ URL::asset('img/tag.gif') }}" width="16" height="14" alt="Post Tags">
                                                <ul class="tags">
                                                    @foreach($post->tags as $tag)
                                                    <li><a href="{{ URL::action('PostController@search', array('all', 'all', $tag->name)) }}">{{ $tag->name }}</a></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </div>
                                            <div class="span3 blog-social-links">
                                                <div class="addthis_toolbox" style="width:200px;float:right">
	                                                <a class="add-this-custom addthis_button_linkedin"><span class="linkedin"></span></a>
	                                                <a class="add-this-custom addthis_button_twitter"><span class="twitter"></span></a>
	                                                <a class="add-this-custom addthis_button_google_plusone_share"><span class="googleplus"></span></a>
	                                                <a class="add-this-custom addthis_button_facebook"><span class="facebook"></span></a>
	                                                <a class="add-this-custom addthis_button_email"><span class="email"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div></div>
                                </div>

	                            <div id="disqus_thread"></div>
	                            <script type="text/javascript">
		                            /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
		                            var disqus_shortname = 'usablenetblog'; // required: replace example with your forum shortname

		                            /* * * DON'T EDIT BELOW THIS LINE * * */
		                            (function() {
			                            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			                            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		                            })();
	                            </script>
	                            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	                            <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>

	                            <br>

	                            <a class="circle-arrow-link blue" href="{{ URL::action('PostController@blog') }}">&nbsp;&nbsp;<strong>Back to Blog Main Page</strong></a>

                            </div>
                        </div>
                    </div>
                </div>

				<div class="span4 span-dt-12 span-tab-12">

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<div class="container">
							<span class="tms-editor" {{ $post->editor('title', 'null', 'edit_post', array('type'=>'text'))}}></span>
							<span class="tms-editor" {{ $post->editor('slug', 'null', 'edit_post')}}></span>
							<span class="tms-editor" {{ $post->editor('category_id', 'null', 'edit_post', array('type'=>'select', 'options'=>$categoryList)) }}></span>
							<span class="tms-editor" {{ $post->editor('author_id', 'null', 'edit_post', array('type'=>'select', 'options'=>$authors)) }}></span>
							<span class="tms-editor" {{ $post->editor('image_thumb', 'null', 'edit_post', array('type'=>'image', 'fieldLabel' => 'Image Thumb: 150x100')) }}></span>
							<span class="tms-editor" {{ $post->editor('main_image', 'null', 'edit_post', array('type'=>'image')) }}></span>
							<span class="tms-editor" {{ $post->editor('posted_on', 'null', 'edit_post', array('type'=>'date')) }}></span>
							<span class="tms-editor" {{ $post->editor('meta_title', null, 'edit_post') }}></span>
							<span class="tms-editor" {{ $post->editor('meta_keywords', null, 'edit_post', array('type' => 'text')) }}></span>
							<span class="tms-editor" {{ $post->editor('meta_description', null, 'edit_post') }}></span>
							<span class="tms-editor" {{ $post->editor('headline', 'null', 'edit_post')}}></span>
							<button class="tms-editor tms-btn-success" {{ $post->editor('article', 'null', 'edit_post', array('type'=>'wysiwyg')) }}>Edit This Post</button>

							<button class="tms-editor tms-btn-danger" {{ $post->deleteThing(null) }}>Remove Post</button>
						</div>
					<?php } ?>

                    <div class="blog-search-form form-group">
                        {{ Form::open(array('action' => array('PostController@search'), 'method'=>'get')) }}
                        <div class="has-feedback has-success">
                            {{ Form::text('q', null, array('class'=>'form-control', 'placeholder'=>'Search Blog')) }}
                            <span class="form-control-feedback search-icon"></span>
                        </div>
                        {{ Form::close() }}
                    </div>

                    <div class="side-panel" style="background-color:#f8f8f8;">
                        <iframe height="160" style="width:100%; border:0px none;" src="http://pages.usablenet.com/Industry-Bites-Sign-up_registration-news-blog.html" scrolling="no" id="Newsletter_Signup"></iframe>
                    </div>

                    @if(count($posts) > 2)
					<div class="side-panel">
						<div class="title">Latest Posts</div>

						<ul>
							@for ($i=0; $i < 3; $i++)
							<li>
								<h6>@if(isset($posts[$i]->category)){{ strtoupper($posts[$i]->category->name) }}@endif</h6>
								<h5><a style="line-height:1em" href="{{ URL::action('PostController@showBlog', $posts[$i]->slug) }}"><strong>{{ $posts[$i]->title }}</strong></a></h5>
								<a class="circle-arrow-link blue" href="{{ URL::action('PostController@showBlog', $posts[$i]->slug) }}">&nbsp;&nbsp;<strong>Read Article</strong></a>
							</li>
							@endfor
						</ul>
					</div>
					@endif

                    <div class="side-panel">
                        <div class="title">Featured Video</div>
                        <div class="slider">
                            <div class="jslink modal-video blog_feature_video_1 about-us-video" aspect data-video-url='{{ $page->blog_feature_video_1 }}'>
                                <img class="mt h20 responsive" id="blog_feature_video_1_image" src="{{ URL::asset($page->blog_feature_video_1_image->src) }}" alt="{{ $page->blog_feature_video_1_image->alt }}">
                            </div>
                            <div class="jslink modal-video blog_feature_video_2 about-us-video" aspect data-video-url='{{ $page->blog_feature_video_2 }}'>
                                <img class="mt h20 responsive" id="blog_feature_video_2_image" src="{{ URL::asset($page->blog_feature_video_2_image->src) }}" alt="{{ $page->blog_feature_video_2_image->alt }}">
                            </div>
                            <div class="jslink modal-video blog_feature_video_3 about-us-video" aspect data-video-url='{{ $page->blog_feature_video_3 }}'>
                                <img class="mt h20 responsive" id="blog_feature_video_3_image" src="{{ URL::asset($page->blog_feature_video_3_image->src) }}" alt="{{ $page->blog_feature_video_3_image->alt }}">
                            </div>
                        </div>

                        <?php if(TmsAuth::isInGroup('Administration')) { ?>
                            <span class="tms-editor" {{ $page->editor('blog_feature_video_1_image', '#blog_feature_video_1_image', 'blog_feature_video_1_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File')) }}></span>
                            <button class="tms-editor tms-btn-success" {{ $page->editor('blog_feature_video_1', '.blog_feature_video_1', 'blog_feature_video_1_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File')) }}>Manage Video 1</button>

                            <span class="tms-editor" {{ $page->editor('blog_feature_video_2_image', '#blog_feature_video_2_image', 'blog_feature_video_2_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File')) }}></span>
                            <button class="tms-editor tms-btn-success" {{ $page->editor('blog_feature_video_2', '.blog_feature_video_2', 'blog_feature_video_2_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File')) }}>Manage Video 2</button>

                            <span class="tms-editor" {{ $page->editor('blog_feature_video_3_image', '#blog_feature_video_3_image', 'blog_feature_video_3_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File')) }}></span>
                            <button class="tms-editor tms-btn-success" {{ $page->editor('blog_feature_video_3', '.blog_feature_video_3', 'blog_feature_video_3_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File')) }}>Manage Video 3</button>
                       <?php } ?>
                    </div>

                    <div class="side-panel">
                        <div class="title">Categories</div>
                        <ul>
                            @foreach($categories as $category)
                            <li>
                                <h6 class="caps">{{ $category->name }}</h6>
                                <h5>@if(count($category->posts))<a href="{{ URL::action('PostController@showBlog', array($category->posts[0]->slug)) }}" style="line-height:1em;"><strong>{{ $category->posts[0]->title }}</strong></a> @else No Posts Found @endif</h5>
                                <a class="circle-arrow-link blue" href="{{ URL::action('PostController@search', array('all', $category->name)) }}">&nbsp;&nbsp;<strong>View All {{ $category->name }} Articles</strong></a>
                                <?php if(TmsAuth::isInGroup('Administration')) { ?>
                                    <br><br></b><button class="tms-editor btn btn-primary tms-btn-danger" {{ $category->deleteThing(null) }}>Remove Category</button>
                                <?php } ?>
                            </li>
                            @endforeach
                        </ul>
                    </div>

					<div class="side-panel tag-cloud">
						<div class="title">Popular Topics</div>
						<div class="blog-tags">
							@if(isset($allTags))
							<ul class="tags">
								@foreach($allTags as $tag)
								<li><a href="{{ URL::action('PostController@search', array('all', 'all', $tag->name)) }}">{{ $tag->name }}</a></li>
								@endforeach
							</ul>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>

				</div>
			</div>
		</div>
	</section>

@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop