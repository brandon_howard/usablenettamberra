<section class="section full-width">
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<h2 style="text-align: center;font-size: 42px;">Our Offices</h2>
				</div>
			</div>
			<div class="row-fluid">
				<div class="top-offices">
				<div class="location one-fifth">
					<img src="/images/office-london.png" alt="London map" class="responsive locaton-image">
					<p class="location-text">
						<span class="fs18" style="
    text-transform: uppercase; font-size: 21px;     font-weight: 500;
">London</span><br>
						11A Curtain Road,
						London, UK EC2A 3LT<br>
						<a class="location-address-link" href="https://www.google.com/maps/place/Curtain+Rd,+London+EC2A+3LT,+UK/@51.5225496,-0.0832159,17z/data=!3m1!4b1!4m2!3m1!1s0x48761cb1cbff9755:0xce41dcb6fe157767?hl=en">Map It</a><br>
												+44.20.3617.3200
											</p>
				</div>
				<div class="location one-fifth">
					<img src="/images/office-losangeles.png" alt="London map" class="responsive locaton-image">
					<p class="location-text">
						<span class="fs18" style="
    text-transform: uppercase; font-size: 21px;    font-weight: 500;
">Los Angeles</span><br>
						1546 7th Street, Suite 300
						Santa Monica, CA 90401<br>
						<a class="location-address-link" href="https://www.google.com/maps/place/1546+7th+St+%23300,+Santa+Monica,+CA+90401/@34.0161505,-118.4920906,17z/data=!3m1!4b1!4m2!3m1!1s0x80c2a4d27a33be11:0xf4be0f70a1fdd50?hl=en">Map It</a><br>
												+1.310.260.9900
											</p>
				</div>
				<div class="location one-fifth">
					<img src="/images/office-newyork.png" alt="London map" class="responsive locaton-image">
					<p class="location-text">
						<span class="fs18" style="
    text-transform: uppercase; font-size: 21px;    font-weight: 500;
">New York</span><br>
						142 W 57th Street, 7th Floor
New York, NY 10019<br>
						<a class="location-address-link" href="https://www.google.com/maps/place/142+W+57th+St,+New+York,+NY+10019/@40.7648242,-73.9814178,17z/data=!3m1!4b1!4m2!3m1!1s0x89c258f765a3c243:0x2dbeea963c561d09?hl=en">Map It</a><br>
												+1.212.965.5388
											</p>
				</div>
				<div class="location one-fifth">
					<img src="/images/office-australia.png" alt="London map" class="responsive locaton-image">
					<p class="location-text">
						<span class="fs18" style="
    text-transform: uppercase; font-size: 21px;    font-weight: 500;
">Australia</span><br>
						Warriewood<br>
Australia NSW 2102<br>
						<a class="location-address-link" href="https://www.google.com/maps/place/Sydney+NSW+2102,+Australia/@-33.6889072,151.2797999,14z/data=!3m1!4b1!4m2!3m1!1s0x6b0d54ec7dcaee49:0x1c017d6b3fba9110?hl=en">Map It</a><br>
												+61(0)416.286.984
											</p>
				</div>
					</div>

			</div>
		</section> 