@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop

@section('main')
	<section class="section full-width page-title-slice" style="background-color:{{$page->title_background_color}};background-image:url({{ URL::asset($page->title_background->src) }});color:{{ $page->title_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">
					<p class="sub-title tms-editor mbz light-gray tms-inline" {{ $page->editor('title_subtitle', 'inline') }}>{{ $page->title_subtitle }}</p>
					<h3 class="title tms-editor tms-inline" {{ $page->editor('title_title', 'inline') }}>{{ $page->title_title }}</h3>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background', '.page-title-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_text_color', '.page-title-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background_color', '.page-title-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width two-column-slice" style="background-color:{{$page->two_column_background_color}};background-image:url({{ URL::asset($page->two_column_background->src) }});color:{{ $page->two_column_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span7">
					<h3 class="title tms-editor tms-inline" {{ $page->editor('two_column_subtitle', 'inline') }}>{{ $page->two_column_subtitle }}</h3>
					<h3 class="title small tms-editor tms-inline" {{ $page->editor('two_column_title', 'inline') }}>{{ $page->two_column_title }}</h3>
				</div>
			</div>
			<div class="row-fluid mt h10">
				<div class="span6">
					<div class="fs24 lh15 tms-editor tms-inline" {{ $page->editor('two_column_column_1_title', 'inline') }}><strong>{{ $page->two_column_column_1_title }}</strong></div>
				</div>
				<div class="span6">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_title', 'inline') }}><strong>{{ $page->two_column_column_2_title }}</strong></div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 col-left">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_1_description', 'inline') }}>{{ $page->two_column_column_1_description }}</div>
				</div>
				<div class="span6 col-right">
					<div class="tms-editor tms-inline" {{ $page->editor('two_column_column_2_description', 'inline') }}>{{ $page->two_column_column_2_description }}</div>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_background', '.two-column-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_text_color', '.two-column-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('two_column_background_color', '.two-column-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width relative">
		<div class="container">
			<div class="row-fluid">
				<div class="span12 mb">
					<h3 class="title ptz mz">Meet The Leadership Team</h3>

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<div class="container">
							<span class="tms-editor" {{ Tms::editor('name', '#usablenet-leader-list', 'new_leader', array('model'=>'Leader'))}}></span>
							<span class="tms-editor" {{ Tms::editor('title', '#usablenet-leader-list', 'new_leader', array('model'=>'Leader'))}}></span>
							<span class="tms-editor" {{ Tms::editor('description', '#usablenet-leader-list', 'new_leader', array('model'=>'Leader', 'type'=>'wysiwyg'))}}></span>
							<span class="tms-editor" {{ Tms::editor('headshot', '#usablenet-leader-list', 'new_leader', array('model'=>'Leader', 'type'=>'image'))}}></span>
							<button class="tms-editor tms-btn-success" {{ Tms::editor('sort', '#usablenet-leader-list', 'new_leader', array('model'=>'Leader', 'fieldLabel' => 'Appearance Order'))}}>Create New Leader</button>
						</div>
					<?php } ?>

				</div>
			</div>



			@foreach($leaders as $leader)
			<div class="row-fluid mt h40">
				<div class="span5">
					<img src="{{ ($leader->headshot->src == '') ? 'holder.js/430x360' : URL::asset($leader->headshot->src) }}" alt="{{$leader->headshot->alt}}" width="430" class="responsive">
				</div>
				<div class="span7">
					<p class="sub-title mt h60">{{ $leader->name }}</p>
					<P class="mt mb h30">{{ strtoupper($leader->title) }}</P>
					<div class="mlz mt"><!-- WYSIWYG -->
						{{ $leader->description }}
					</div>

			<?php if(TmsAuth::isInGroup('Administration')) { ?>
				<span class="tms-editor" {{ $leader->editor('name', null, 'edit_partner'.$leader->id)}}></span>
				<span class="tms-editor" {{ $leader->editor('title', null, 'edit_partner'.$leader->id)}}></span>
				<span class="tms-editor" {{ $leader->editor('description', null, 'edit_partner'.$leader->id, array('type'=>'wysiwyg'))}}></span>
				<span class="tms-editor" {{ $leader->editor('headshot', null, 'edit_partner'.$leader->id, array('type'=>'image'))}}></span>
				<button class="tms-editor tms-btn-success" {{ $leader->editor('sort', null, 'edit_partner'.$leader->id, array('fieldLabel' => 'Appearance Order'))}}>Edit this Leader</button>

				<button class="tms-editor tms-btn-danger" {{ $leader->deleteThing(null) }}>Remove this Leader <i class="icon-trash icon-white"></i></button>
			<?php } ?>
				</div>
			</div>

			<hr class="mt h30" />
			@endforeach

		</div>
	</section>

		<section class="section full-width ux-slice" style="background-image:url({{ URL::asset($page->ux_background_image->src) }});background-color:{{$page->ux_background_color}};color:{{ $page->ux_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span5 color">
					<h3 class="title tms-editor tms-inline" {{ $page->editor('ux_title', 'inline') }}>{{ $page->ux_title }}</h3>
					<p class="fs18 tms-editor tms-inline" {{ $page->editor('ux_subtitle', 'inline') }}>{{ $page->ux_subtitle }}</p>
				</div>
				<div class="span7 mt h20">
					@if(isset($_GET['message']) && $_GET['message'] == 'sent')
						<h3>Message Sent!</h3>
					@endif

					@include('usablenet.ux-form')

				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('ux_background_image', '.ux-slice', null, array('updateAttribute' => 'style[background-image]')) }}>Edit Background Image</button>
			<button class="tms-editor tms-btn" {{ $page->editor('ux_text_color', '.ux-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('ux_background_color', '.ux-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	@include('usablenet.contact-form-offices')
@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
	  <div class="modal-body">
	    <video id="video-player" controls="true" preload="none" width="527">
	      <p>Your user agent does not support the HTML5 Video element.</p>
	    </video>
	  </div>
	</div>
@stop