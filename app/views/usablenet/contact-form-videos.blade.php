<section class="section full-width padding bottom contact-us">
	<div class="container">
		<div class="row-fluid">
			<div class="span6 left-col">
				@if($page->contact_us_video_title_left !== '' || TmsAuth::isInGroup('Administration'))
				<h3 class="title mbz tms-editor tms-inline" {{ $page->editor('contact_us_video_title_left', 'inline') }}>{{ $page->contact_us_video_title_left }}</h3>
				@endif

				<div class="row-fluid">
					<div class="span12">
						<ul class="row-fluid portfolio-items portfolio-style1 style2" data-columns="2">
							<li class="span6 mlz jslink modal-video" data-video-url='{{ $page->contact_us_video_1 }}'>
								<div class="inner-content">
									<div class="image"><img src="{{ URL::asset($page->contact_us_video_1_image->src) }}" alt="{{ $page->contact_us_video_1_image->alt }}"></div>
								</div>
							</li>
							<li class="span6 mlz jslink modal-video" data-video-url='{{ $page->contact_us_video_2 }}'>
								<div class="inner-content">
									<div class="image"><img src="{{ URL::asset($page->contact_us_video_2_image->src) }}" alt="{{ $page->contact_us_video_2_image->alt }}"></div>
								</div>
							</li>
						</ul>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span12">
						<ul class="row-fluid portfolio-items portfolio-style1 style2" data-columns="2">
							<li class="span6 mlz bottom-row jslink modal-video" data-video-url='{{ $page->contact_us_video_3 }}'>
								<div class="inner-content">
									<div class="image"><img src="{{ URL::asset($page->contact_us_video_3_image->src) }}" alt="{{ $page->contact_us_video_3_image->alt }}"></div>
								</div>
							</li>
							<li class="span6 mlz bottom-row jslink modal-video" data-video-url='{{ $page->contact_us_video_4 }}'>
								<div class="inner-content">
									<div class="image"><img src="{{ URL::asset($page->contact_us_video_4_image->src) }}" alt="{{ $page->contact_us_video_4_image->alt }}"></div>
								</div>
							</li>
						</ul>
					</div>
					<?php if(TmsAuth::isInGroup('Administration')) { ?>
						<div class="mt h20 span12">
							<span class="tms-editor" {{ $page->editor('contact_us_video_1_image', '#contact_us_video_1_image', 'contact_us_video_1_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File')) }}></span>
							<button class="tms-editor tms-btn-success" {{ $page->editor('contact_us_video_1', '.contact_us_video_1', 'contact_us_video_1_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File')) }}>Manage Video 1</button>

							<span class="tms-editor" {{ $page->editor('contact_us_video_2_image', '#contact_us_video_2_image', 'contact_us_video_2_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File')) }}></span>
							<button class="tms-editor tms-btn-success" {{ $page->editor('contact_us_video_2', '.contact_us_video_2', 'contact_us_video_2_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File')) }}>Manage Video 2</button>

							<span class="tms-editor" {{ $page->editor('contact_us_video_3_image', '#contact_us_video_3_image', 'contact_us_video_3_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File')) }}></span>
							<button class="tms-editor tms-btn-success" {{ $page->editor('contact_us_video_3', '.contact_us_video_3', 'contact_us_video_3_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File')) }}>Manage Video 2</button>

							<span class="tms-editor" {{ $page->editor('contact_us_video_4_image', '#contact_us_video_4_image', 'contact_us_video_4_group', array('updateAttribute'=>'src', 'fieldLabel' => 'Poster Image File')) }}></span>
							<button class="tms-editor tms-btn-success" {{ $page->editor('contact_us_video_4', '.contact_us_video_4', 'contact_us_video_4_group', array('updateAttribute'=>'data-video-url', 'fieldLabel' => 'Video File')) }}>Manage Video 2</button>
						</div>
					<?php } ?>
				</div>

			</div>

			<div class="span6 right-col">

				@if(Session::has('message-success-contact-form-videos'))
					<h3 class="title mbz" id="contact">Thank You. We will contact you shortly.</h3>
				@else
					<h3 class="title mbz" id="contact">Contact Us</h3>
				@endif

				@if(Session::has('message-fail-contact-form-videos'))
					<h4 class="text-error title mbz">{{ Session::get('message-fail-contact-form-videos') }}</h4>
				@endif

				<div class="mt h20">
					<div class="inline-anchor"><div id="contact-form-videos"></div></div>

					{{ Form::open(array('action' => 'ContactController@email')) }}
						{{Form::hidden('program_name', 'Website Contact Us')}}
						{{Form::hidden('from', 'contact-form-videos')}}

						<div class="clear h1">&nbsp;</div>

						<div class="row-fluid">
							<div class="span12">
								{{ Form::text('FirstName', null, array('class' => 'input-block-level', 'placeholder' => 'First Name')) }}
								{{ $errors->first('FirstName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('LastName', null, array('class' => 'input-block-level', 'placeholder' => 'Last Name')) }}
								{{ $errors->first('LastName'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Email', null, array('class' => 'input-block-level', 'placeholder' => 'Email Address')) }}
								{{ $errors->first('Email'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::text('Company', null, array('class' => 'input-block-level', 'placeholder' => 'Organization\'s Name')) }}
								{{ $errors->first('Company'); }}
							</div>
						</div>

						<div class="row-fluid mt h20">
							<div class="span12">
								{{ Form::textarea('ContactUs_Message__c', null, array('class' => 'input-block-level', 'placeholder' => 'Message', 'rows' => '2')) }}
							</div>
						</div>

						<div class="row-fluid">
							<div class="span4 fr">
								<button type="submit" class="ptl pbl btn btn-success btn-flat btn-block mt h20">Send</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>

		</div>
	</div>
</section>