@extends('layouts.master')

@section('topform')
	<div class="topform">
		<div class="container">&nbsp;</div>
	</div>
@stop
 
@section('main')
	<section class="section full-width page-title-slice" style="background-color:{{$page->title_background_color}};background-image:url({{ URL::asset($page->title_background->src) }});color:{{ $page->title_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">
					<h1 class="sub-title tms-editor tms-inline mbz light-gray" {{ $page->editor('title_subtitle', 'inline') }}>{{ $page->title_subtitle }}</h1>
					<h2 class="title tms-editor tms-inline" {{ $page->editor('title_title', 'inline') }}>{{ $page->title_title }}</h2>
				</div>
			</div>
			<?php if(TmsAuth::isInGroup('Administration')) { ?>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background', '.page-title-slice', null, array('updateAttribute' => 'style[background-image]', 'type'=>'image')) }}>Edit Section Background</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_text_color', '.page-title-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
			<button class="tms-editor tms-btn" {{ $page->editor('title_background_color', '.page-title-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
			<?php } ?>
		</div>
	</section>

	<section class="section full-width" style="
    margin-bottom: 55px;
">
		<div class="container products-grid">

				<div class="title tms-editor tms-inline" {{ $page->editor('grid_header', 'inline') }}>{{ $page->grid_header }}</div>

				<div class="row-fluid mt h40 text-center">

				<?php $row = 1; ?>
				@foreach($products as $i => $product)
					@if ($i > 0 && $i%3 == 0 )
						<?php $row++ ?>
						</div>
						<div class="row-fluid mt 20 text-center">
					@endif

					<?php
						if ($i%3 == 0)
						{
							$leftBorder = '';
						}
						else
						{
							$leftBorder = '<div class="left-border">&nbsp;</div>';
						}
					?>

					<div class="span4 feature_box">
						{{$leftBorder}}
						<div class="inner-wrapper mt h20">
							<a id="three_x_three_link_2" href="{{ URL::action('ProductController@show', $product->slug) }}"><img id="three_x_three_images_2" src="{{ URL::asset( 'images/icons/'.$product->icon.'-white.png' ) }}" alt="{{ $product->title }} Product Page" /></a>
							<p class="mt mbz title"><strong id="three_x_three_title_2">{{$product->title }}</strong></p>
							<p class="mz description" id="three_x_three_description_2" @if($row == 2)
								style="
    height: 110px;
"
							@endif>{{$product->description }}</p>
							<a class="circle-arrow-link blue" id="three_x_three_link_2" href="{{ URL::action('ProductController@show', $product->slug) }}"><span class="ml" style="font-weight: 700;">{{$page->three_x_three_link_2->fieldLabel }}</span></a>
							@if($row < 2)
								<hr class="bottom-seperator mbz">
							@endif
						
							
						</div>
					</div>
				@endforeach

				</div>
			</div>
		</div>
	</section>

	<section class="section full-width two-column-slice" style="background-image:url({{ URL::asset($page->two_column_background->src) }});background-color:{{$page->two_column_background_color}};color:{{ $page->two_column_text_color }}">
		<div class="container">
			<div class="row-fluid">
				<div class="span5">
					<h3 class="title gray tms-editor tms-inline" style="
    font-size: 15px;
    letter-spacing: 0px;
    font-weight: 700;
" {{ $page->editor('two_column_subtitle', 'inline') }}>{{ $page->two_column_subtitle }}</h3>
					<h4  style="
    font-size: 36px;
    line-height: 1em;
    font-weight: 100;
" class="title tms-editor tms-inline" {{ $page->editor('two_column_title', 'inline') }}>{{ $page->two_column_title }}</h4>
					<p class="tms-editor tms-inline" {{ $page->editor('two_column_column_1_description', 'inline') }}>{{ $page->two_column_column_1_description }}</p>
					<a id="two_column_column2_link" style="font-weight: 700;" href="{{$page->two_column_column2_link->href}}">{{$page->two_column_column2_link->fieldLabel }}</a>

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<button class="tms-editor tms-btn" {{ $page->editor('two_column_column2_link', '#two_column_column2_link') }}>Edit Link</button>
					<?php } ?>
				</div>
				<div class="span7 mt h60">
					<div class="video-launcher jslink modal-video" data-video-url='{{ $page->two_column_video }}'></div>

					<?php if(TmsAuth::isInGroup('Administration')) { ?>
					<button class="tms-editor tms-btn" {{ $page->editor('two_column_video', '#two_column_video') }}>Edit Video</button>
					<?php } ?>
				</div> 


			</div>
				<?php if(TmsAuth::isInGroup('Administration')) { ?>
				<div class="mt h30">
					<button class="tms-editor tms-btn" {{ $page->editor('two_column_background', '.two-column-slice', null, array('updateAttribute' => 'style[background-image]')) }}>Edit Background Image</button>
					<button class="tms-editor tms-btn" {{ $page->editor('two_column_text_color', '.two-column-slice', null, array('updateAttribute' => 'style[color]', 'type'=>'color')) }}>Edit Text Color</button>
					<button class="tms-editor tms-btn" {{ $page->editor('two_column_background_color', '.two-column-slice', null, array('updateAttribute' => 'style[background-color]', 'type'=>'color')) }}>Edit Background Color</button>
				</div>
				<?php } ?>
		</div>
	</section>


@stop

@section('modals')
	<div id="modal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Video Modal" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
		<div class="modal-body">
			<video id="video-player" autoplay="true" controls="true" preload="none" width="100%">
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video>
		</div>
	</div>
@stop