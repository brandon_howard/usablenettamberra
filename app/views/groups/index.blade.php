@extends('layouts.master')

{{-- Web site Title --}}
@section('title')
@parent
Groups
@stop

{{-- Content --}}
@section('content')
<h4>Available Groups</h4>
<div class="row">
  <div class="col-md-12">
	<div class="table-responsive">
		<table class="table table-striped table-hover">
			<thead>
				<th>Name</th>
				<th>Permissions</th>
				<th>Options</th>
			</thead>
			<tbody>
			@foreach ($groups as $group)
				<tr>
					<td><a href="groups/{{ $group->id }}">{{ $group->name }}</a></td>
					<td>{{ (isset($group['permissions']['admin'])) ? '<i class="icon-ok"></i> Admin' : ''}} {{ (isset($group['permissions']['users'])) ? '<i class="icon-ok"></i> Users' : ''}}</td>
					<td>
						{{ Form::open(array('route' => array('groups.destroy', $group->id), 'method' => 'delete')) }}
							<a class="btn btn-default" href="{{ URL::to('groups') }}/{{ $group->id }}/edit/">Edit</a>
							<button type="submit" href="{{ URL::route('groups.destroy', $group->id) }}" class="btn btn-default {{ ($group->id == 2) ? 'disabled' : '' }}">Delete</button>
						{{ Form::close() }}
					 </td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
		<button class="btn btn-primary" onClick="location.href='{{ URL::to('groups/create') }}'">New Group</button>
	</div>
</div>

@stop

