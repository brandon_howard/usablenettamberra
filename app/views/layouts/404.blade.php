<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	@if(isset($meta) && isset($meta['meta_title']))
		<title>{{ $meta['meta_title'] }}</title>
	@else
		<title>Usablenet</title>
	@endif

	@if(isset($meta) && isset($meta['meta_description']))
		<meta name="description" content="{{ $meta['meta_description'] }}">
	@endif

	@if(isset($meta) && isset($meta['meta_keywords']))
		<meta name="keywords" content="{{$meta['meta_keywords'] }}">
	@endif

	@if(isset($page->head))
		{{$page->head}}
	@endif

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">

	<!-- compiled stylesheet -->
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />

	@if (App::environment('stag'))
		<link rel="stylesheet" href="{{ URL::asset('css/debug.css') }}" type="text/css" />
	@endif


<?php if(TmsAuth::isInGroup('Administration')) { ?>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/smoothness/jquery-ui-1.10.3.custom.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('js/spectrum/spectrum.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('js/chosen/chosen.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/tms.css') }}">

<?php } ?>
	<!--[if lt IE 9]>
		<script src="{{ URL::asset('js/html5shiv.js') }}"></script>
		<script src="{{ URL::asset('js/respond.js') }}"></script>
	<![endif]-->

	<!--[if IE]>
		<link rel="stylesheet" href="{{ URL::asset('css/ie.css') }}" type="text/css" />
	<![endif]-->
</head>
<body class="responsive" style="background-image:url({{ URL::asset('images/404-background.jpg') }});">

<!-- Google Tag Manager-->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M47VWD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M47VWD');</script>
<!-- End Google Tag Manager-->

	<div id="header">
		<section class="bg gray-light" id="top-wrapper">
			<div class="section full-width pz" id="top-menu">
				<div class="row-fluid">
					<div class="container">
						<ul class="span12 links-list pull-right text-right">
							<li class="hidden-desktop">
								<a href="{{URL::to('/')}}" class="small-logo">
									<img src="{{URL::asset('images/usablenet-header-logo-sm.png')}}" width="113" height="27" alt="Usablenet"/>
								</a>
							</li>
						<?php if(TmsAuth::isInGroup('Administration')) { ?>
							<li id="tms-admin">
								<a class="tms-admin tms-cursor" data-tms-admin='{"action":"load-manager","contentUrl":"{{ URL::action('PageController@pageManager') }}"}'>Manage Pages</a>
							</li>
							<li>
								<a class="tms-cursor" onclick="location.href='{{ URL::route('logout') }}'">Logout</a>
							</li>
						<?php } ?>
						<?php
						foreach($menus['Top Menu']->menuItems as $menu_item)
						{
							if ($menu_item->label !== "(Break)")
							{
								$currentPageClass = (URL::current() == URL::to($menu_item->link)) ? ' current-menu-parent' : '';

								echo '<li class="'. $currentPageClass .'"><a href="'.$menu_item->link.'">'.$menu_item->label.'</a>';

								echo '</li>';
							}
						}
						?>
							<li><a class="share-btn linked-in" href="http://www.linkedin.com/company/156593?trk=tyah&trkInfo=tas%3Ausablenet%2Cidx%3A4-1-5" target="_blank">LinkedIn</a></li>
							<li><a class="share-btn twitter" href="https://twitter.com/Usablenet" target="_blank">Twitter</a></li>
							<li><a class="share-btn gplus" href="https://plus.google.com/u/2/b/103635202693633675970/103635202693633675970/about" target="_blank">Google Plus</a></li>
							<li><a class="share-btn facebook" href="https://www.facebook.com/usablenet" target="_blank">Facebook</a></li>
							<li><a class="share-btn pinterest" href="http://www.pinterest.com/usablenet/" target="_blank">Pinterest</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section id="main_header_container">
			<div id="main_navigation" class="section full-width">
				<div class="container">
					<div class="row-fluid">
						<div class="span12">
							<div class="row-fluid">

								<div class="span3 visible-desktop">
									<a href="{{URL::to('/')}}" id="logo" class="clearfix">
										<img src="{{URL::asset('images/usablenet-header-logo.png')}}" width="218" height="65" class="responsive" alt="Usablenet Logo"/>
									</a>
								</div>

								<ul id="main_menu" class="main_menu span9 span-dt-12">
								<?php
									foreach($menus['Main Menu']->menuItems as $menu_item)
									{
										$random_number = rand(1000, 9999);

										if ($menu_item->label !== "(Break)")
										{
											$currentPageClass = (URL::current() == URL::to($menu_item->link)) ? ' current-menu-parent' : '';

											$hasSubClass = (count($menu_item->children) > 0) ? ' has-sub-menu' : '';

											echo '<li id="menu-item-'.$random_number.'" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-'.$random_number.''. $hasSubClass .''. $currentPageClass .'"><a href="'.$menu_item->link.'">'.$menu_item->label.'</a>';

											if (count($menu_item->children) > 0)
											{
												echo '<ul class="sub-menu">';

												foreach ($menu_item->children as $child) {
													echo '<li id="menu-item-2607" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2607"><a href="'.$child->link.'">'.$child->label.'</a></li>';
												}

												echo '</ul>';
											}
											echo '</li>';
										}
									}
								?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	@yield('main')

</body>
</html>
