<?php

Route::get('news/How-APIs-Accelerate-Business', function() { return Redirect::to('/news/how-apis-accelerate-business', 301); });
Route::get('news/The-Tablet-Experience-Survey-Says-More-Desktop-Less-Smartphone', function() { return Redirect::to('/news/the-tablet-experience-survey-says-more-desktop-less-smartphone', 301); });
Route::get('posts/Tablet-Owners-Content-and-Experience-Preferences-When-Browsing-the-Web', function() { return Redirect::to('/news/tablet-owners-content-and-experience-preferences-when-browsing-the-web', 301); });
Route::get('news/Avenue-32-Brings-Element-Of-Discovery-Back-To-Online-Shopping', function() { return Redirect::to('/news/avenue-32-brings-element-of-discovery-back-to-online-shopping', 301); });
Route::get('news/Retailers-experiencing-bad-side-effects-from-tablets%20', function() { return Redirect::to('/news/retailers-experiencing-bad-side-effects-from-tablets', 301); });
Route::get('news/Brands-Slow-to-Create%20Tablet-Specific-Experiences', function() { return Redirect::to('/news/brands-slow-to-create-tablet-specific-experiences', 301); });
Route::get('news/Avenue-32-sees-mobile-orders-increase-400-pc-after-multichannel-facelift', function() { return Redirect::to('/news/avenue-32-sees-mobile-orders-increase-400-pc-after-multichannel-facelift', 301); });
Route::get('news/10-interesting-digital-marketing-stats-we-ve-seen-this-week', function() { return Redirect::to('/news/10-interesting-digital-marketing-stats-we-have-seen-this-week', 301); });
Route::get('news/Medifast-Enhances-Business-Model-With-Mobile-Weight-Loss-Tools', function() { return Redirect::to('/news/medifast-enhances-business-model-with-mobile-weight-loss-tools', 301); });
Route::get('news/Tablet-Owners-Content-and-Experience-Preferences-When-Browsing-the-Web', function() { return Redirect::to('/news/tablet-owners-content-and-experience-preferences-when-browsing-the-web', 301); });
Route::get('news/chain-store-agenbsp-mobile-shopping-skyrockets-thanksgiving-and-black-friday-1', function() { return Redirect::to('/news/mobile-shopping-skyrockets-thanksgiving-and-black-friday', 301); });
Route::get('news/new-media-knowledge-new-report-reveals-top-tips-for-making-the-most-of-mobile-in-store-1', function() { return Redirect::to('/news/new-report-reveals-top-tips-for-making-the-most-of-mobile-in-store', 301); });
Route::get('news/journaldunetcom-marks-and-spencer-va-deacuteployer-agrave-linternational-son-savoir-faire-multicanal-1', function() { return Redirect::to('/news/marks-and-spencer-va-deployer-a-l-international-son-savoir-faire-multicanal', 301); });
Route::get('news/marketwired-usablenet-holiday-shopping-index-shows-highest-spike-in-mobile-activity-on-cyber-monday-1', function() { return Redirect::to('/news/usablenet-holiday-shopping-index-shows-highest-spike-in-mobile-activity-on-cyber-Monday', 301); });
Route::get('archive', function() { return Redirect::to('/news', 301); });
Route::get('news/marketwired-usablenet-named-in-independent-mobile-commerce-solution-provider-report-1', function() { return Redirect::to('/news/usablenet-named-in-independent-mobile-commerce-solution-provider-report', 301); });
Route::get('news/Is-Tablet-Specific-Better-than-Responsive', function() { return Redirect::to('/news/is-tablet-specific-better-than-responsive', 301); });
Route::get('news/Avenue-32-provides-mobile-design-showcase', function() { return Redirect::to('/news/avenue-32-provides-mobile-design-showcase', 301); });
Route::get('news?page=4', function() { return Redirect::to('/news/page4', 301); });
Route::get('news/new-media-knowledge-cyber-monday-mega-monday-mobile-monday-more-like-1', function() { return Redirect::to('/news/cyber-monday-mega-monday-mobile-monday-more-like', 301); });
Route::get('news/eseller-five-ways-to-close-the-multichannel-loop-1', function() { return Redirect::to('/news/five-ways-to-close-the-multichannel-loop', 301); });
Route::get('news/mediapost-holiday-wrap-mobile-jumps-in-visits-order-size-and-sales-1', function() { return Redirect::to('/news/holiday-wrap-mobile-jumps-in-visits-order-size-and-sales', 301); });
Route::get('news/Usablenet-Experience-important-for-tablet-shoppers', function() { return Redirect::to('/news/usablenet-experience-important-for-tablet-shoppers', 301); });

Route::filter('permissions', 'PermissionsFilter');

Route::group(array('before' => 'permissions'), function()
{
	Route::resource('menus', 'MenuController');
	Route::resource('pages', 'PageController');
	Route::get("/pages/{id}/page-manager", [
		"as"   => "page-manager",
		"uses" => "PageController@pageManager"
	]);
	Route::put("/pages/{id}/update-page", [
		"as"   => "update-page",
		"uses" => "PageController@updatePage"
	]);
	Route::post("/pages/store-page", [
		"as"   => "store-page",
		"uses" => "PageController@storePage"
	]);
	Route::get("get-page-list", [
		"as"   => "page-list",
		"uses" => "PageController@getPageList"
	]);
	Route::get("scan-template/{template_id}", [
		"as"   => "scan-template",
		"uses" => "PageController@scanTemplate"
	]);
	Route::post("/ckeditor/file-upload", [
		"as"   => "file-upload",
		"uses" => "PageController@fileUpload"
	]);

	Route::get("/menu-items/create/{menu_id}", [
		"as"   => "create-menu-item",
		"uses" => "MenuItemController@create"
	]);
	Route::post("/menu-items/store-break/{menu_id}", [
		"as"   => "menu-item-store-break",
		"uses" => "MenuItemController@storeBreak"
	]);
	Route::resource('menu-items', 'MenuItemController');

//	Route::get("slug-them-all", [
//		"as"   => "slug-them-all",
//		"uses" => "PostController@slugThemAll"
//	]);

	Route::resource('products', 'ProductController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('case-studies', 'CaseStudyController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('careers', 'CareerController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('clients', 'ClientController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('news', 'PostController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('cetegories', 'CategoryController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('authors', 'AuthorController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('locations', 'LocationController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('partners', 'PartnerController', array('only' => array('update', 'store', 'destroy')));
	Route::resource('leaders', 'LeaderController', array('only' => array('update', 'store', 'destroy')));
});

Route::resource('products', 'ProductController', array('only' => array('show'), 'names' => array('show' => 'products.{slug}')));
Route::resource('news', 'PostController', array('only' => array('show')));
Route::resource('clients', 'ClientController', array('only' => array('show'), 'names' => array('show' => 'clients.{slug}')));
Route::resource('partnerships', 'PartnerController', array('only' => array('show'), 'names' => array('show' => 'partnerships.{slug}')));

Route::get("blog", [
    "as"   => "blog",
    "uses" => "PostController@blog"
]);

Route::get("blog/{slug}", [
    "as"   => "blog-article",
    "uses" => "PostController@showBlog"
]);

Route::get('/capabilities', [
	"as"   => "services-page",
	"uses" => "ServicesController@show"
]);

Route::get("search-blog/{q?}/{category?}/{tag?}", [
    "as"   => "search",
    "uses" => "PostController@search",
	function($q = null) {
		return $_GET['q'];
	},
    function($category = null) {
        return $category;
    },
    function ($tag = null) {
        return $tag;
    }
]);

Route::get("news", [
	"as"   => "insights",
	"uses" => "PostController@index"
]);

Route::get("/archive/{year}", [
	"as"   => "news-archive",
	"uses" => "PostController@archive"
]);

Route::get("/archive", function() {
	return Redirect::action('PostController@index');
});

Route::any("model-values", function(){
	$careers = array(
			'Career' => array(
					'careers' => 'all'
				)
		);

	$casestudies = array(
			'CaseStudy' => array(
					'caseStudies' => 'all'
				)
		);

	$posts = array(
			'Post' => array(
					'posts' => 'all'
				)
		);

	return $posts;

});

Route::any('webhooks/zencoder/{table}/{field}/{id}', [
	"as"   => "zencoder_webhook",
	function($table, $field, $id){
		Event::fire('webhook.zencoder', array($table, $field, $id));
	}
]);

Route::get("/sitemap.xml", [
	"as"   => "sitemap",
	"uses" => "PageController@sitemap"
]);

Route::get("/rss/{version}", [
	"as"   => "rss",
	"uses" => "PostController@rss"
]);
Route::get("/blogrss/{version}", [
    "as"   => "blogRSS",
    "uses" => "PostController@blogRSS"
]);
Route::get("/kcrss/{version}", [
    "as"   => "kcRSS",
    "uses" => "PostController@kcRSS"
]);

Route::get('404', array('uses' => 'RouteErrorController@fourZeroFour'));

Route::get("/login", [
	"as"   => "login",
	"uses" => "UserController@login"
]);

Route::get("/logout", [
	"as"   => "logout",
	"uses" => "UserController@logout"
]);

Route::post("/attempt-login", [
	"as"   => "loginAction",
	"uses" => "UserController@loginAction"
]);

Route::post('send-email', [
		'as' => 'send-email',
		'uses' => 'ContactController@email'
	]);

Route::get('/', [
	"as"   => "tms-page",
	"uses" => "PageController@show"
]);

Route::get('{slug}', [
	"as"   => "tms-page",
	"uses" => "PageController@show"
])->where('slug', '[A-Za-z0-9%\-\/]+');



function array_merge_assoc($arr1, $arr2){

	$final_arr = $arr1 + $arr2;

	foreach ($arr1 as $key => $val)
	{
		foreach($arr2 as $key2 => $val2)
		{
			if ($key2 == $key)
			{
				$final_arr[$key] = $final_arr[$key] + $arr2[$key];
			}
		}
	}

	return $final_arr;
}
