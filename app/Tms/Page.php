<?php namespace Tms;

use Tms\Collections\PageCollection;
use URL;
use DB;

class Page extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tms_pages';

	protected $softDelete = true;

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Sets the controller for ThingMS
	 * @var string
	 */
	public $controller = 'PageController';

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
		'name'  => 'required',
		'slug'  => 'required|unique:tms_pages,slug',
		'template_id'  => 'required|min:1'
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
		'slug'  => 'required|unique:tms_pages,slug'
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateFieldsRules = array(
	);


	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $imageUploadRules = array(
		'upload' => 'image'
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
		'name' => 'Your page must have a name.',
		'slug' => 'Your page must have a unique slug and cannot be empty.',
		'template_id' => 'You must select a template for your page.'
	);

	public static function boot()
	{
		parent::boot();

		Page::saving(function($page)
		{
			if(isset($page->encodedSlug)) unset($page->encodedSlug);

			$contentFields = array_diff_assoc($page->attributes, $page->original);

			foreach ($contentFields as $key => $value)
			{
				if (!strpos($key, '_dataType'))
				{
					// Does it have a dataType pair?
					if (isset($contentFields[$key.'_dataType']))
					{

						$contentType = $contentFields[$key.'_dataType'];

						switch ($contentType)
						{
							case 'varchar':
								$varchar  = new Varchar;
							break;
							case 'textarea':
								$textarea = new Textarea;
							break;
							case 'image':
								$image    = new Image;
							break;
							case 'wysiwyg':
								$wysiwyg  = new Wysiwyg;
							break;
							case 'link':
								$link     = new Link;
							break;
							case 'color':
								$color    = new Color;
							break;
							case 'select':
								$select   = new Select;
							break;
							case 'date':
								$date     = new Date;
							break;
							case 'video':
								$video    = new Video;
							break;
						}

						$contentRecord = $$contentType->where('key', '=', $key)
								 ->where(function($query) use ($page){
										$query->where('page_id', '=', $page->id)
											  ->orWhere('page_id', '=', 0);
									})->first();

						if($value != $contentRecord->value)
						{
							$contentRecord->value = $value;
							$contentRecord->save();
						}

						unset($page->$key);
					}

					unset($page[$key.'_dataType']);
				}
			}

			return true;

		});
	}

	public function template()
	{
		return $this->belongsTo('Tms\Template');
	}

	public function varchars()
	{
		return $this->hasMany('Tms\Varchar');
	}

	public function textareas()
	{
		return $this->hasMany('Tms\Textarea');
	}

	public function images()
	{
		return $this->hasMany('Tms\Image');
	}

	public function wysiwygs()
	{
		return $this->hasMany('Tms\Wysiwyg');
	}

	public function colors()
	{
		return $this->hasMany('Tms\Color');
	}

	public function links()
	{
		return $this->hasMany('Tms\Link');
	}

	public function selects()
	{
		return $this->hasMany('Tms\Select');
	}

	public function dates()
	{
		return $this->hasMany('Tms\Date');
	}

	public function videos()
	{
		return $this->hasMany('Tms\Video');
	}

	public function newCollection(array $models = array())
	{
		$page = null;

		foreach ($models as $key => $page) {
			$models[$key]->encodedSlug = rawurlencode($models[$key]->slug);
			$models[$key]->encodedSlug = str_replace('%2F', '/', $models[$key]->encodedSlug);
		}

		// Add in Colors
		$models = $this->prepareContent($page, $models, 'color');

		// Add in Dates
		$models = $this->prepareContent($page, $models, 'date');

		// Add in Images
		$models = $this->prepareContent($page, $models, 'image');

		// Add in Selects
		$models = $this->prepareContent($page, $models, 'select');

		// Add in Textareas
		$models = $this->prepareContent($page, $models, 'textarea');

		// Add in Varchars
		$models = $this->prepareContent($page, $models, 'varchar');

		// Add in Links
		$models = $this->prepareContent($page, $models, 'link');

		// Add in Video
		$models = $this->prepareContent($page, $models, 'video');

		// Add in WYSIWYGs
		$models = $this->prepareContent($page, $models, 'wysiwyg');

		return new PageCollection($models);
	}

	private function prepareContent($page, $models, $type)
	{
		foreach($models as $key => $model)
		{
			switch ($type)
			{
				case 'varchar':
					$content = $model->varchars()->orWhere('page_id', '=', 0)->get();
				break;
				case 'textarea':
					$content = $model->textareas()->orWhere('page_id', '=', 0)->get();
				break;
				case 'datetime':
				case 'date':
					$content = $model->dates()->orWhere('page_id', '=', 0)->get();
				break;
				case 'image':
					$content = $model->images()->orWhere('page_id', '=', 0)->get();
				break;
				case 'video':
					$content = $model->videos()->orWhere('page_id', '=', 0)->get();
				break;
				case 'wysiwyg':
					$content = $model->wysiwygs()->orWhere('page_id', '=', 0)->get();
				break;
				case 'link':
					$content = $model->links()->orWhere('page_id', '=', 0)->get();
				break;
				case 'color':
					$content = $model->colors()->orWhere('page_id', '=', 0)->get();
				break;
				case 'select':
					$content = $model->selects()->orWhere('page_id', '=', 0)->get();
				break;
			}

			foreach($content as $c)
			{
				if ($type === 'image' && $c->value == '')
				{
					$c->value = 'holder.js/100%x100%/text:FPO';
				}

				if($type === 'video' && is_array(json_decode($c->value))){
					$videos = json_decode($c->value);
					foreach ($videos as $index => $video) {
						$videos[ $index ] = URL::asset($video);
					}
					$c->value = json_encode($videos);
				}

				$models[$key][$c->key] = $c->value;
				$models[$key][$c->key . '_dataType'] = $type;
			}
		}

		return $models;
	}


	public function getAdditionalPageModels()
	{
		$withVars = array();

		if ($this->template->models != '')
		{
			$modelsNeeded = json_decode($this->template->models);

			foreach($modelsNeeded as $model => $variables)
			{
				foreach($variables as $variableName => $query)
				{
					$modelInstance = new $model;

					if(is_array($query) || is_object($query))
					{
						foreach($query as $method => $args)
						{
							if (!is_array($args) && $args !== null)
							{
								$args = array($args);
							}

							if (is_array($args))
							{
								foreach($args as $i => $arg)
								{
									$matches = array();

									if (preg_match('/(?<=DB::raw\(\')(.*)+(?=\'\))/', $arg, $matches))
									{
										$args[$i] = DB::raw($matches[0]);
									}
								}
							}

							if ($args === null)
							{
								$modelInstance = $modelInstance->$method();
							}
							else
							{
								$modelInstance = call_user_func_array(array($modelInstance, $method), $args);
							}
						}
					}

					$withVars[$variableName] = $modelInstance;
				}
			}
		}

		return $withVars;
	}
}