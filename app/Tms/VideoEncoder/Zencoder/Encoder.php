<?php namespace Tms\VideoEncoder\Zencoder;

use \Services_Zencoder;
use \Services_Zencoder_Exception;
use DB;

class Encoder {
	private $Zencoder;
	private $tmpFileURL;
	private $fileName;
	private $tmpFileName;
	private $fieldEncodeStates = array();

	public function __construct($apiKey) {
		$this->Zencoder = new Services_Zencoder($apiKey);
	}

	/**
	 * Submits video to the zencoder api
	 *
	 * @param url of target file
	 * @return boolean | array of errors
	 */
	public function encodeMedia($filePath, $settings, $webhookUrl) {

		$errors = array();
		$message = 'The encoding for your file has been started';

		$notifications = [ $webhookUrl ];

		$outputs = array();
		$filename = $this->randomName(32);

		foreach ($settings as $filetype => $fileSettings) {
			$outputOptns = array(
				"label" => $filetype,
				"notifications" => $notifications,
				"format" => $filetype,
				"filename" => $filename . '.' . $filetype
			);
			$outputOptns = array_merge($outputOptns, $fileSettings);

			$outputs[] = $outputOptns;
		}

		try {

			$encoding_job = $this->Zencoder->jobs->create (
				array(
					"input" =>  $filePath,
					"outputs" => $outputs
				)
			);

			return true;

		} catch (Services_Zencoder_Exception $e) {
			return $e->getErrors();
		}
	}

	private function randomName($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	/**
	 * Handles when the zencoder api submits data to our site. This function will use the
	 * Zencdoer class to parse the incomming data.
	 *
	 * @param object $Model Model is automatically passed by the Model using the behavior
	 * @return void
	 */
	public function handleNotification($table, $field, $id) {
		try {

			$notification = $this->Zencoder->notifications->parseIncoming();

		} catch (Services_Zencoder_Exception $e) {

			print_r($e->getErrors());exit;

		}

		foreach ($notification->job->outputs as $output) {
			if ($output->state == "finished") {
				$dir = public_path() . '/videos/';
				$filename     = md5(time()) . '.' . $output->label;

				if($this->downloadFile($output->url, $dir, $filename))
				{
					$video = DB::table($table)
								->where('id','=',$id)
								->first();

					$videos = json_decode($video->$field);
					if(is_array($videos)) {
						$videos[] = '/videos/' . $filename;
					} else {
						$videos = array('/videos/' . $filename);
					}

					DB::table($table)
						->where('id','=',$id)
						->update(array($field => json_encode($videos)));
				}
			}
		}
	}

	/**
	 * Accepts the url of the video on zencoder's servers and uses CURL to download the
	 * file and place on the server
	 *
	 * @param string $url The url of the encoded video file
	 * @param string $dir The target directory where the file will be placed
	 * @param string $filename The name of the new file on the server after download
	 * @return void
	 */
	private function downloadFile($url, $dir, $filename) {
		if(!is_dir($dir)){
			mkdir($dir, 0755, true);
		}

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$data = curl_exec($ch);

		curl_close($ch);

		return file_put_contents($dir . $filename, $data);
	}
}