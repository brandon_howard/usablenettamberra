<?php namespace Tms;

use URL;
use Tms\Page;

class Link extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tms_content_links';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
	);

	public function pages()
	{
		return $this->belongsTo('Page');
	}

	public function getValueAttribute($value)
	{
		if (!is_object($value))
		{
			$data = json_decode($value);

			if ($data !== false && $data !== null)
			{
				if($data->url != '')
				{
					$data->href = $data->url;
				}
				else
				{
					if($data->pageIdField != 0)
					{
						$linkPage = Page::find($data->pageIdField);
						$data->href = URL::to($linkPage->slug);
					}
					else
					{
						$data->href = '';
					}
				}
				return $data;
			}
			else
			{
				return $value;
			}
		}
		else
		{
			return $value;
		}
	}
}