<?php namespace Tms;

class Video extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tms_content_videos';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
	);

	public function pages()
	{
		return $this->belongsTo('Page');
	}
}