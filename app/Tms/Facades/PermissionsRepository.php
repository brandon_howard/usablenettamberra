<?php namespace Tms\Facades;

use Illuminate\Support\Facades\Facade;

class PermissionsRepository extends Facade {

	protected static function getFacadeAccessor() { return 'permissionsrepository'; }

}