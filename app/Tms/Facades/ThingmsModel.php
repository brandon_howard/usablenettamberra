<?php namespace Tms\Facades;

use Illuminate\Support\Facades\Facade;

class ThingmsModel extends Facade {

	protected static function getFacadeAccessor() { return 'thingmsmodel'; }

}