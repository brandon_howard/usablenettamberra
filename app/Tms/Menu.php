<?php namespace Tms;

use Tms\Page;
use URL;
use Log;

class Menu extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tms_menus';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
	);

	public function menuItems()
	{
		return $this->hasMany('Tms\MenuItem');
	}


	public function buildMenus($menu_id = null)
	{
		$pages = Page::all();
		$returnMenu = array();

		$menus = $this->with(array('menuItems' => function($query)
			{
				$query->with(array('children' => function($query){
					$query->orderBy('sort');
				}))->where('parent', '=', null)->orderBy('menu_id')->orderBy('sort');
			}
		));

		$menus = ($menu_id === null) ? $menus->get() : $menus->find($menu_id);

		if (get_class($menus) === 'Illuminate\Database\Eloquent\Collection')
		{
			foreach($menus as $menu)
			{
				$returnMenu = $this->buildMenuGuts($returnMenu, $menu, $pages);
			}
		}
		else
		{
			$returnMenu = $this->buildMenuGuts($returnMenu, $menus, $pages);
		}

		return $returnMenu;
	}

	private function buildMenuGuts($returnMenu, $menu, $pages)
	{
		$returnMenu[$menu->name] = $menu;

		foreach($returnMenu[$menu->name]->menuItems as $key=>$menu_item)
		{
			$returnMenu[$menu->name]->menuItems[$key]->link = $this->calculateLink($menu_item, $pages);

			foreach($menu_item->children as $childKey => $child)
			{
				$returnMenu[$menu->name]->menuItems[$key]['children'][$childKey]->link = $this->calculateLink($child, $pages);
			}
		}

		return $returnMenu;
	}

	public function calculateLink($menu_item, $pages)
	{
		if($menu_item->page_id !== null && $menu_item->page_id !== 0)
		{
			foreach($pages as $page)
			{
				if ($page->id == $menu_item->page_id)
				{
					return URL::to( $page->slug );
				}
			}
			Log::warning('Menu Item #'.$menu_item->id.' ("'.$menu_item->label.'") is linked to a non-existing (or deleted) page.');
			return '#';
		}
		else
		{
			if ($menu_item->url !== null)
			{
				return $menu_item->url;
			}
			else
			{
				return '#';
			}
		}
	}
}