<?php namespace Tms;

use Eloquent;
use DB;
use URL;
use File;
use TmsAuth;

class ThingmsModel extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = '';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	protected $allowedEditorTypes = array(
					'color',
					'date',
					'image',
					'link',
					'video',
					'wysiwyg',
					'checkbox',
					'select',
					'text',
					'textarea',
				);

	protected $pageFields = array(
					'name',
					'slug',
					'short_description',
					'meta_title',
					'meta_description',
					'meta_keywords',
					'head',
					'footer',
					'models'
				);

	/**
	 * Validation rules when creating a new car
	 * @var array
	 */
	public $storeRules = array();

	/**
	 * Validation rules when updating a car
	 * @var array
	 */
	public $updateRules = array();

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array();

	public function deleteThing($target=null)
	{
		if(TmsAuth::isInGroup('Administration')) {

			$options['model'] = get_class($this);
			$options['destroy'] = true;
			$options['updateTarget'] = $target;
			$action = 'destroy';
			$id = $this->id;

			if (isset($currentModel->controller))
			{
				$controller = $currentModel->controller . '@' . $action;
			}
			else
			{
				$controller = $options['model'].'Controller@' . $action;
			}

			$options['formAction'] = URL::action($controller, $id);

			$output = ' data-tms-editor=\''.htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8').'\' ';
			return $output;
		}
	}

	/**
	 * Builds a data-tms-editor attribute for a editor
	 * @param  string $field   Field Name
	 * @param  string $target  Selector string for the updated target
	 * @param  string $group   Unique group name
	 * @param  array  $options Additional options and overrides
	 * @return string          "data-tms-editor" attribute with JSON encoded value set
	 */
	public function editor($field, $target = null, $group = null, $options = array())
	{
		if(TmsAuth::isInGroup('Administration')) {

			$options['currentUrl'] = URL::current();

			if (!isset($options['model']))
			{
				$options['model'] = get_class($this);
				$currentModel = $this;
			}
			else
			{
				$currentModel = new $options['model'];
			}

			if (!isset($options['type']))
			{
				$options['type'] = $currentModel->allowedEditorTypes($options['model'], $field);
			}

			if ($options['type'] === 'link')
			{
				if (isset($this->$field->fieldLabel))
					$options['linkData']['fieldLabel'] = $this->$field->fieldLabel;
				if (isset($this->$field->url))
					$options['linkData']['url']        = $this->$field->url;
				if (isset($this->$field->page_id))
					$options['linkData']['page_id']    = $this->$field->page_id;
				if (isset($this->$field->linkType))
					$options['linkData']['linkType']   = $this->$field->linkType;
				if (isset($this->$field->href))
					$options['linkData']['href']       = $this->$field->href;
				if (isset($this->$field->target))
					$options['linkData']['target']     = $this->$field->target;

				$options['pageListUrl'] = URL::route('page-list');
			}

			if ($target !== null)
			{
				$options['updateTarget'] = $target;
			}

			if ($group !== null)
			{
				$options['group'] = $group;
			}

			if (!isset($options['fieldLabel']))
			{
				$options['fieldLabel'] = ucwords(str_replace('_', ' ', $field));
			}

			if (get_class($this) === 'Tms\ThingmsModel')
			{
				$options['formMethod'] = 'POST';
			}

			if (!isset($options['formAction']))
			{
				// Means that it's a create
				if (get_class($this) === 'Tms\ThingmsModel')
				{
					$action = 'store';
					$id = null;
					$options['create'] = true;
				}
				else
				{
					$action = 'update';
					$id = $this->id;
				}

				if (isset($currentModel->controller))
				{
					$controller = $currentModel->controller . '@' . $action;
				}
				else
				{
					$controller = $options['model'].'Controller@' . $action;
				}

				$options['formAction'] = URL::action($controller, $id);

			}

			if (!isset($options['fieldValue']))
			{
				$options['fieldValue'] = $this->$field;
			}

			$options['fieldName'] = $field;

			$output = ' data-tms-editor=\''.htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8').'\' ';
			return $output;
		}
		return '';
	}

	/**
	 * Guesses what the field type should be based on the
	 * database schema
	 * @todo Need to break out datetime and integer
	 * @param  string $model Model Name
	 * @param  string $field Name of field that we are going to guess the type of
	 * @return string        Editor type we guessed
	 */
	private function allowedEditorTypes($model, $field)
	{

		if ($model !== 'Tms\Page' || ($model === 'Tms\Page' && in_array($field, $this->pageFields))) {
            if ($field == 'tags') {
                return 'text';
            }

			$db_type = DB::connection()->getDoctrineColumn($this->table, $field);
			$comment = $db_type->getComment();

			if ($comment !== '' && in_array($comment, $this->allowedEditorTypes))
			{
				return $comment;
			}

			$name = $db_type->getType()->getName();
			$length = $db_type->getLength();

			switch($name)
			{
				case 'boolean':
					return 'checkbox';
				break;
				case 'date':
				case 'datetime': // Need to break out later
					return 'date';
				break;

				case 'text':
					return 'textarea';
				break;

				case 'string':
				case 'integer': // Need to break out later
				default:
					if ($length === 7)
					{
						return 'color';
					}

					return 'text';
				break;
			}
		} else {
			// If here we should be updating one of the content tables
			switch($this[$field.'_dataType'])
			{
				case 'select':
					return 'select';
				break;
				case 'color':
					return 'color';
				break;
				case 'wysiwyg':
					return 'wysiwyg';
				break;
				case 'image':
					return 'image';
				break;
				case 'video':
					return 'video';
				case 'link':
					return 'link';
				break;
				case 'date':
				case 'datetime':
					return 'date';
				break;
				case 'text':
				case 'textarea':
					return 'textarea';
				break;
				default:
					return 'text';
				break;
			}
		}
	}


	public function writeFile($file)
	{
		$image           = $file;
		$name            = $image->getClientOriginalName();
		$extension       = $image->getClientOriginalExtension();

		$mime_type = explode('/', $file->getMimeType());

		if ($mime_type[0] == 'image')
		{
			$path = 'img/uploads';
		}
		else
		{
			$path = 'videos';
		}

		$destinationPath = public_path() . '/' . $path;
		$name            = $this->nameFile($destinationPath, $name, $extension);
		$uploadedFile    = $file->move($destinationPath, $name);

		return  $path . '/' . $name;
	}


	public function nameFile($destinationPath, $file, $extension, $number = null, $originalName = null)
	{
		if (File::exists($destinationPath . '/' . $file))
		{
			$originalName = (is_null($originalName)) ? $file : $originalName;
			$number = (is_null($number)) ? 1 : $number + 1;

			$newName = $this->numberFile($originalName, $extension, $number);

			$file = $this->nameFile($destinationPath, $newName, $extension, $number, $originalName);
		}
		else
		{
			return $file;
		}

		return $file;
	}

	public function getImageAttr($value)
	{
		if (!is_object($value))
		{
			$data = json_decode($value);

			if ($data !== false && $data !== null)
			{
				if (!isset($data->src))
				{
					$data->src = '';
				}
				return $data;
			}
			else
			{
				$value = (object)array(
						'src' => $value,
						'alt' => ''
					);
				return $value;
			}
		}
		else
		{
			$value = (object)array(
					'src' => $value,
					'alt' => ''
				);
			return $value;
		}
	}
	
	

	private function numberFile($file, $extension, $number)
	{
		$p = strrpos($file, '.');
		if ($p !== false) // Sanity check, maybe there isn't a period after all.
		  $file = substr($file, 0, $p);

		$file = $file . '_' . $number . '.' . $extension;

		return $file;
	}
}