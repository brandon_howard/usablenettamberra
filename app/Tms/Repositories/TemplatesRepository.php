<?php namespace Tms\Repositories;

use Log;
use File;
use Response;
use Tms\Template;

use Tms\Varchar;
use Tms\Textarea;
use Tms\Date;
use Tms\Image;
use Tms\Video;
use Tms\Wysiwyg;
use Tms\Color;
use Tms\Select;
use Tms\Link;

class TemplatesRepository {

	protected $Template;

	public function __construct(Template $Template)
	{
		$this->Template = $Template;
	}

	public function getTemplateOptions()
	{
		return $this->Template->where('name', '<>', 'Insights')->orderBy('name')->lists('name', 'id');
	}

	public function scanTemplate($id)
	{
		$template = $this->Template->find($id);

		$templateFile = File::get(app_path().'/views/'.$template->blade.'.blade.php');

		preg_match_all('/\$page-\>editor\(\'(.*?)\'/', $templateFile, $matches);

		$orphanDbValues = json_decode($template->values);
		$orphanPageValues = $matches[1];

dd($matches[1]);

$dbvalues = array(
				'title_description',
				'title_title',
				'title_subtitle',
				'two_column_title',
				'two_column_subtitle',
				'two_column_column_1_title',
				'two_column_column_2_title',
				'title_background',
				'two_column_background',
				'two_column_column_1_image',
				'two_column_column_2_image',
				'two_column_column_1_description',
				'two_column_column_2_description',
				'two_column_column1_link',
				'two_column_column2_link',
				'two_column_text_color',
				'two_column_background_color',
				'two_column_video',
			);

dd(array_diff($dbvalues, $matches[1]));

		foreach($matches[1] as $key => $variable)
		{
			if (strpos($template->values, $variable) > 0)
			{
				unset($orphanPageValues[$key]);
			}
		}

		foreach($orphanDbValues as $type => $fields)
		{
			foreach($fields as $fieldName => $defaultValue)
			{
				foreach($matches[1] as $key => $variable)
				{
					if($variable == $fieldName)
					{
						unset($orphanDbValues->$type->$fieldName);
					}
				}
			}
		}

		var_dump($orphanPageValues);
		var_dump($orphanDbValues);
	}

	public function populateFields($Page)
	{
		$pageid = $Page->id;

		$template = $this->Template->find($Page->template_id);

		$values = (array)json_decode($template->values);

		if (is_array($values))
		{
			foreach($values as $type => $fields)
			{
				foreach($fields as $fieldName => $defaultValue)
				{
					$model = null;

					switch ($type)
					{
						case 'varchars':
							$model = new Varchar;
						break;
						case 'textareas':
							$model = new Textarea;
						break;
						case 'datetimes':
						case 'dates':
							$model = new Date;
						break;
						case 'images':
							$model = new Image;
						break;
						case 'videos':
							$model = new Video;
						break;
						case 'wysiwygs':
							$model = new Wysiwyg;
						break;
						case 'colors':
							$model = new Color;
						break;
						case 'selects':
							$model = new Select;
						break;
						case 'links':
							$model = new Link;
						break;
					}

					if ($model != null){

						$input = array(
							'page_id' => $pageid,
							'key'     => $fieldName,
							'value'   => $defaultValue
						);

						$model->fill($input);

						if(!$model->save())
						{
							$Page->delete();

							Log::error('Someone tried to save a new page and the content could not be saved. Type: '.$type.'.');

							return Response::json(array(
								'flash' => 'There was a problem storing the page data',
								'errors' => array('There was a problem storing the template data. The page was not saved. See application logs for details.')
							), 400);
						}
					} else {
						$Page->delete();

						Log::error('Someone tried to save a new page with a content type that doesn not exist. Type: '.$type.'.');

						return Response::json(array(
							'flash' => 'There was a problem storing the page data',
							'errors' => array('There was a problem storing the template data. The page was not saved. See application logs for details.')
						), 400);
					}
				}
			}
		}
		return true;
	}
}