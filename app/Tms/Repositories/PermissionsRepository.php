<?php namespace Tms\Repositories;

use Auth;
use Config;
use Tms\Permission;
use Tms\User;

class PermissionsRepository {

	protected $action;

	protected $User;

	protected $Permission;

	public function __construct(Permission $Permission, User $User)
	{
		$this->Permission = $Permission;
		$this->User = $User;
	}

	public function __call($method, $arguments)
	{
		if(strpos($method, 'isInGroup') === 0)
		{
			$group = substr($method, 9);

			if($this->isInGroup($group))
			{
				return true;
			}
			return false;
		}

		throw new PermissionRuleNotFoundException("We were unable to locate the permission function you added. Did you remove it?");
		return false;
	}

	public function checkSecurity($route)
	{
		$this->action = $route->getAction();
		$permission = $this->Permission->whereAction($this->action['controller'])->first();

		if ($permission)
		{
			$rules = json_decode($permission->rules);
			foreach($rules->functions as $rule)
			{
				if(!call_user_func(array($this, $rule)))
				{
					return false;
				}
			}
			return true;
		}
		else if (!Config::get('thingms.hardSecurity'))
		{
			return true;
		}

		return false;
	}

	public function isLoggedIn()
	{
		if (Auth::check())
		{
			return true;
		}

		return false;
	}

	public function isInGroup($group)
	{
		if(Auth::check())
		{
			$user = $this->User->with('groups')->find(Auth::user()->id);
			foreach($user->groups as $group){
				if ($group === $group)
				{
					return true;
				}
			}
		}

		return false;
	}

	public function mySpecialRule()
	{
		return true;
	}
}