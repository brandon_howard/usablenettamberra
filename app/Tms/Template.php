<?php namespace Tms;

class Template extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tms_templates';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
		'name'  => 'required',
		'blade' => 'required',
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
		'name'    => 'Your template must have a name.',
		'blade'  => 'Your template must have a blade file.',
	);

	public function pages()
	{
		return $this->hasMany('Page');
	}
}