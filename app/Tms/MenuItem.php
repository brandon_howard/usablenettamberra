<?php namespace Tms;

class MenuItem extends ThingmsModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tms_menu_items';

	/**
	 * Fields that are guarded against mass assignment
	 * @var array
	 */
	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * Validation rules when creating a new page
	 * @var array
	 */
	public $storeRules = array(
		'label' => 'required'
	);

	/**
	 * Validation rules when updating a page
	 * @var array
	 */
	public $updateRules = array(
	);

	/**
	 * Validation messages to display on error
	 * @var array
	 */
	public $messages = array(
	);

	public function menu()
	{
		return $this->belongsTo('Tms\Menu');
	}

	public function children()
	{
		return $this->hasMany('Tms\MenuItem', 'parent');
	}

	public function parent()
	{
		return $this->belongsTo('Tms\MenuItem', 'id', 'parent');
	}
}