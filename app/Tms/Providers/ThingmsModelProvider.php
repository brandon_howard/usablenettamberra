<?php namespace Tms\Providers;

use Illuminate\Support\ServiceProvider;
use Tms\ThingmsModel;
use Tms\Permission;
use Tms\User;

class ThingmsModelProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		// Register 'underlyingclass' instance container to our UnderlyingClass object
		$this->app['thingmsmodel'] = $this->app->share(function($app)
		{
			return new ThingmsModel();
		});

		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('Tms', 'Tms\Facades\ThingmsModel');
		});
	}
}
