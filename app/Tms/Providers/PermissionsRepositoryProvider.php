<?php namespace Tms\Providers;

use Illuminate\Support\ServiceProvider;
use Tms\Repositories\PermissionsRepository;
use Tms\Permission;
use Tms\User;

class PermissionsRepositoryProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		// Register 'underlyingclass' instance container to our UnderlyingClass object
		$this->app['permissionsrepository'] = $this->app->share(function($app)
		{
			$Permission = new Permission;
			$User = new User;
			return new PermissionsRepository($Permission, $User);
		});

		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('TmsAuth', 'Tms\Facades\PermissionsRepository');
		});
	}
}
