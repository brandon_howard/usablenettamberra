<?php namespace Tms\Providers;

use Illuminate\Support\ServiceProvider;
use Tms\VideoEncoder\Zencoder\Encoder;
use Tms\Video;
use \Product;
use URL;
use Event;
use File;

class VideoEncoderServiceProvider extends ServiceProvider {
	public function register()
	{

	}

	public function boot()
	{
		Video::updating(function($video)
		{
			if(File::exists(public_path() . '/' . $video->value)){
				// value is not an array
				// means a new video was uploaded an needs to be encoded
				$settings = array(
					'mp4' => array( /* 3gp, aac, ac3, ec3, flv, ism, mkv, mp3, mp4, ogg, ts, webm, and wmv */
						// 'width' => 640, // leave blank to not resize
						// 'height' => 460, // leave blank to not resize
						//'aspect_mode' => 'preserve', /* preserve, stretch, crop, pad */
						'upscale' => true, /* not refering to quality only size */
						'quality' => 4, /* 1(lowest)-5(best) */
						// 'test' => true, /* test will not charge if set to true */
						'video_codec' => 'h264', /* h264, vp6, theora, vp8, mpeg4, wmv */
						// 'audio_codec' => 'mp3', /* mp3, aac, vorbis, wma, mp3*/
					),
					'ogg' => array( /* 3gp, aac, ac3, ec3, flv, ism, mkv, mp3, mp4, ogg, ts, webm, and wmv */
						//'width' => 640, // leave blank to not resize
						//'height' => 460, // leave blank to not resize
						//'aspect_mode' => 'preserve', /* preserve, stretch, crop, pad */
						'upscale' => true, /* not refering to quality only size */
						'quality' => 4, /* 1(lowest)-5(best) */
						// 'test' => true, /* test will not charge if set to true */
						'video_codec' => 'theora', /* h264, vp6, theora, vp8, mpeg4, wmv */
						// 'audio_codec' => 'vorbis', /* mp3, aac, vorbis, wma, mp3*/
					)
				);
				$webhookUrl = URL::route('zencoder_webhook', ['tms_content_videos','value',$video->id]);
				$videoUrl = Url::asset($video->value);

				$encoder = new Encoder('3c81cf0c6a9954ef1e4ec6625ff556f5');
				$result = $encoder->encodeMedia($videoUrl, $settings, $webhookUrl);

			    $video->value = '';
			}

		    return true;
		});

		Product::updating(function($product)
		{
			if(File::exists(public_path() . '/' . $product->side_column_video)){
				// value is not an array
				// means a new video was uploaded an needs to be encoded
				$settings = array(
					'mp4' => array( /* 3gp, aac, ac3, ec3, flv, ism, mkv, mp3, mp4, ogg, ts, webm, and wmv */
						// 'width' => 640, // leave blank to not resize
						// 'height' => 460, // leave blank to not resize
						//'aspect_mode' => 'preserve', /* preserve, stretch, crop, pad */
						'upscale' => true, /* not refering to quality only size */
						'quality' => 4, /* 1(lowest)-5(best) */
						//'test' => true, /* test will not charge if set to true */
						'video_codec' => 'h264', /* h264, vp6, theora, vp8, mpeg4, wmv */
						//'audio_codec' => 'mp3', /* mp3, aac, vorbis, wma, mp3*/
					),
					'ogg' => array( /* 3gp, aac, ac3, ec3, flv, ism, mkv, mp3, mp4, ogg, ts, webm, and wmv */
						//'width' => 640, // leave blank to not resize
						//'height' => 460, // leave blank to not resize
						//'aspect_mode' => 'preserve', /* preserve, stretch, crop, pad */
						'upscale' => true, /* not refering to quality only size */
						'quality' => 4, /* 1(lowest)-5(best) */
						//'test' => true, /* test will not charge if set to true */
						'video_codec' => 'theora', /* h264, vp6, theora, vp8, mpeg4, wmv */
						//'audio_codec' => 'vorbis', /* mp3, aac, vorbis, wma, mp3*/
					)
				);
				$webhookUrl = URL::route('zencoder_webhook', ['products','side_column_video',$product->id]);
				$productUrl = Url::asset($product->side_column_video);

				$encoder = new Encoder('3c81cf0c6a9954ef1e4ec6625ff556f5');
				$result = $encoder->encodeMedia($productUrl, $settings, $webhookUrl);

			    $product->side_column_video = '';
			}

		    return true;
		});

		Event::listen('webhook.zencoder', function($table, $field, $id)
		{
			$encoder = new Encoder('3c81cf0c6a9954ef1e4ec6625ff556f5');
			$encoder->handleNotification($table, $field, $id);
		});
	}
}