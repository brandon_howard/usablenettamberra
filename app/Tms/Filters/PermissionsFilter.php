<?php namespace Tms\Filters;

use Tms\Repositories\PermissionsRepository;
use Tms\Group;
use User;
use Redirect;

class PermissionsFilter {

	protected $PermissionsRepository;

	public function __construct(PermissionsRepository $PermissionsRepository)
	{
		$this->PermissionsRepository = $PermissionsRepository;
	}

	public function filter($route, $request, $response = null, $value = null)
	{
		if ($this->PermissionsRepository->isLoggedIn())
		{
			if(!$this->PermissionsRepository->checkSecurity($route))
			{
				return $this->redirectToLogin();
			}
		} else {
			return $this->redirectToLogin();
		}
	}

	private function redirectToLogin()
	{
		return Redirect::action('UserController@logout')
							->with('message', 'You must be logged in to view that content.');
	}

}