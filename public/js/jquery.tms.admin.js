;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsAdmin",
			defaults = {
			};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initializes the Plugin
				 * @return {void}
				 */
				init: function ()
				{
					$(".chosen-selector").chosen({disable_search_threshold: 10});

					this._attachListeners();
				},

				_attachListeners: function()
				{
					var base = this;
					$('body').on('click', '.tms-admin', function(){
						base._loadAction(this);
					});
				},

				_loadAction: function(el)
				{
					var settings = $(el).data('tms-admin');

					switch(settings.action)
					{
						/* General Manager Stuff */
						case 'load-manager':
							if($('#tms-manager').length === 0)
							{
								this._loadPageManager(settings.contentUrl);
							}
						break;
						case 'cancel-slider':
							this._unloadSlider(settings);
						break;
						case 'change-tabs':
							this._loadTab(el);
						break;
						case 'close-manager':
							this._closeAdmin();
						break;

						/* Pages */
						case 'view-page':
							this._loadPage(settings.contentUrl);
						break;
						case 'new-page':
						case 'edit-page':
							this._loadSlider(settings.action, settings.contentUrl);
						break;
						case 'update-page':
							this._updatePage(settings.pageManagerUrl);
						break;

						/* Menus */
						case 'edit-menu':
							this._loadSlider(settings.action, settings.contentUrl);
						break;
						case 'update-menu':
							this._updateMenu(settings.pageManagerUrl);
						break;

						/* Menu Items */
						case 'edit-menu-item':
							this._loadSlider(settings.action, settings.contentUrl, settings.tier);
						break;
						case 'create-menu-item':
							this._loadSlider(settings.action, settings.contentUrl, settings.tier);
						break;
						case 'store-menu-item':
							this._storeMenuItem(settings);
						break;
						case 'update-menu-item':
							this._updateMenuItem(settings);
						break;
						case 'create-menu-item-break':
							settings.isBreak = true;
							this._storeMenuItem(settings);
						break;
						case 'destroy-menu-item':
							this._destroyMenuItem(settings);
						break;
					}
				},

				_loadTab: function(el)
				{

					var _newTab = $(el).attr('data-tms-tab');
					var _originalPosition = $('.tms-tab.active').position();

					$('.tms-tab.active').animate({
						'left':    '-=300',
						'opacity': 0
					}, 250, function(){
						$(this).removeClass('active')
							.css('opacity', 1);

						$('#'+_newTab).css({
								left:    -300,
								opacity: 0
							})
							.addClass('active')
							.animate({
								'left':'+=305',
								'opacity': 1
							});
					});
				},

				_loadSlider: function(action, contentUrl, tier)
				{
					var base = this;
					var wrapper;

					if (tier === undefined)
					{
						wrapper = $('<div id="tms-'+action+'">')
										.addClass('tms-page-manager-slider');
						sliderEl = '.tms-page-manager-slider';
						endLocaction = 75;
					}
					else
					{
						wrapper = $('<div id="tms-'+action+'">')
										.addClass('tms-page-manager-slider tier-'+tier);
						sliderEl = '.tms-page-manager-slider.tier-'+tier;
						endLocaction = 75 * tier;
					}


					var jqxhr = $.ajax( contentUrl )
									.done(function(html) {

										wrapper.append(html);
										$('#tms-manager-wrapper').append(wrapper);

										base._loadChosen();

										base._animate(
											$(sliderEl),
											'left',
											{
												start: '-=300',
												end: endLocaction,
												opacity:1
											}
										);

									});

				},

				_unloadSlider: function(options)
				{
					var base = this;
					var sliderEl;

					if (options.tier === undefined)
					{
						sliderEl = '.tms-page-manager-slider';
					}
					else
					{
						sliderEl = '.tms-page-manager-slider.tier-'+options.tier;
					}

					this._animate(
							$(sliderEl),
							'left',
							{
								start: '-=1',
								end: '-=300',
								opacity: 0
							},
							function()
							{
								if (options.pageManagerUrl !== undefined)
								{
									base._reloadPageManager(options.pageManagerUrl);
								}
								if (options.menuManagerUrl !== undefined)
								{
									base._reloadMenuManager(options.menuManagerUrl);
								}

								base._removeEl($(sliderEl));
							}
						);
				},

				_loadPageManager: function(contentUrl)
				{
					var base = this;

					var jqxhr = $.ajax( contentUrl )
									.done(function(html) {
										$('body').append(html);

										base._animate(
											$('#tms-page-manager'),
											'left',
											{
												start: '-=300',
												end: 5,
												opacity: 1
											}
										);
									});
				},

				_reloadPageManager: function(contentUrl)
				{
					var base = this;

					var jqxhr = $.ajax( contentUrl )
									.done(function(html) {

										$('#tms-manager-wrapper').fadeOut(250, function(){
											$('#tms-manager-wrapper').replaceWith(html);
											$('#tms-page-manager').hide().css('opacity', 1);
											$('#tms-page-manager').fadeIn(200);
										});
									});
				},

				_reloadMenuManager: function(contentUrl)
				{
					var base = this;

					var jqxhr = $.ajax( contentUrl )
									.done(function(html) {

										$('#tms-edit-menu').fadeOut(250, function(){
											$('#tms-edit-menu').html(html);
											$('#tms-edit-menu').hide().css('opacity', 1);
											$('#tms-edit-menu').fadeIn(200);
										});
									});
				},

				_updatePage: function(pageManagerUrl)
				{
					var base = this;

					var _data = $('#tms-page-manager-form').serializeArray();
					var _action = $('#tms-page-manager-form').attr('action');
					var _method = $('#tms-page-manager-form').find('input[name=_method]').val();

					if (_method === undefined)
					{
						_method = $('#tms-page-manager-form').attr('method');
					}

					var jqxhr = $.ajax({
										data: _data,
										url:  _action,
										type:  _method
									})
									.done(function(html) {
										options = {};
										if (pageManagerUrl !== undefined)
										{
											options.pageManagerUrl = pageManagerUrl;
										}
										base._unloadSlider(options);
									})
									.fail(function(jqXHR, response) {
										base._displayErrors('slider', jqXHR.responseJSON);
									});
				},

				_loadPage: function(contentUrl)
				{
					window.location.href = contentUrl;
				},

				_updateMenu: function(pageManagerUrl)
				{
					var base = this;

					var _action = $('#tms-menu-manager-form').attr('action');
					var _method = $('#tms-menu-manager-form').find('input[name=_method]').val();

					if (_method === undefined)
					{
						_method = $('#tms-menu-manager-form').attr('method');
					}

					var _data = '';
					$.each($('#tms-menu-manager-form').serializeArray(), function(i, field) {
						_data += field.name+'='+field.value+'&';
					});

					var _menuItems = $('#tms-menu-items-sortable').nestedSortable('serialize');

					var jqxhr = $.ajax({
										data: _data + _menuItems,
										url:  _action,
										type:  _method
									})
									.done(function(html) {
										options = {};
										if (pageManagerUrl !== undefined)
										{
											options.pageManagerUrl = pageManagerUrl;
										}
										base._unloadSlider(options);
									})
									.fail(function(jqXHR, response) {
										base._displayErrors('slider', jqXHR.responseJSON);
									});
				},

				_updateMenuItem: function(settings)
				{
					var base = this;

					var _data = $('#tms-menu-item-manager-form').serializeArray();
					var _action = $('#tms-menu-item-manager-form').attr('action');
					var _method = $('#tms-menu-item-manager-form').find('input[name=_method]').val();

					if (_method === undefined)
					{
						_method = $('#tms-menu-item-manager-form').attr('method');
					}

					var jqxhr = $.ajax({
										data: _data,
										url:  _action,
										type:  _method
									})
									.done(function(html) {
										base._unloadSlider({
											menuManagerUrl:settings.menuUrl,
											tier:settings.tier
										});
									})
									.fail(function(jqXHR, response) {
										base._displayErrors('slider', jqXHR.responseJSON);
									});
				},

				_storeMenuItem: function(settings)
				{
					var base = this;
					var _data;
					var _action;
					var _method;

					if (!settings.isBreak)
					{
						_data = $('#tms-menu-item-manager-form').serializeArray();
						_action = $('#tms-menu-item-manager-form').attr('action');
						_method = $('#tms-menu-item-manager-form').find('input[name=_method]').val();

						if (_method === undefined)
						{
							_method = $('#tms-menu-item-manager-form').attr('method');
						}
					}
					else
					{
						_action = settings.contentUrl;
						_method = 'POST';
					}

					var jqxhr = $.ajax({
										data: _data,
										url:  _action,
										type:  _method
									})
									.done(function(html) {
										if (!settings.isBreak)
										{
											base._unloadSlider({
												menuManagerUrl:settings.menuUrl,
												tier:settings.tier
											});
										}
										else
										{
											base._reloadMenuManager(settings.menuUrl);
										}
									})
									.fail(function(jqXHR, response) {
										base._displayErrors('slider', jqXHR.responseJSON);
									});
				},

				_destroyMenuItem: function(settings)
				{
					var base = this;

					var jqxhr = $.ajax({
										data: settings.data,
										url:  settings.contentUrl,
										type: settings.method
									})
									.done(function(html) {
										base._reloadMenuManager(settings.menuUrl);
									})
									.fail(function(jqXHR, response) {
										base._displayErrors('slider', jqXHR.responseJSON);
									});
				},

				_animate: function(el, direction, animation, callback)
				{
					var base = this;

					if (callback === undefined)
					{
						callback = function(){};
					}

					var _cssO = {};
					_cssO[direction] = animation.start;

					var _aniO = {};
					_aniO[direction] = animation.end;
					_aniO['opacity'] = animation.opacity;

					el.css(_cssO)
					.animate(_aniO, 500, function(){
						callback.call(base, el);
					});
				},

				_displayErrors: function(type, errors)
				{
					$('.tms-alert').remove();

					var _selector     = '';
					var _alert        = $('<div>').addClass('tms-alert');
					var _listOfIssues = $('<ul>');
					var _alertTitle   = $('<h4>').html(errors.flash);

					for(var field in errors.errors)
					{
						var _listItem = $('<li>').html(errors.errors[field][0]);
						_listOfIssues.append(_listItem);
					}

					if (type == 'slider')
					{
						_selector = $('.tms-page-manager-slider');
					}


					_alert.append(_alertTitle);
					_alert.append(_listOfIssues);

					_selector.prepend(_alert);

				},

				_removeEl: function(el)
				{
					el.remove();
				},

				_closeAdmin: function()
				{
					var base = this;

					this._animate(
						$('#tms-manager-wrapper'),
						'left',
						{
							start: '-=1',
							end: '-=300',
							opacity: 0
						},
						base._removeEl
					);
				},

				_loadChosen: function(){
					$(".chosen-selector").chosen({disable_search_threshold: 10});
				}



		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};

})( jQuery, window, document );