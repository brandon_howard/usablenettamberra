$(document).ready(function () {
    $('#slider-case-studies').bxSlider({
        mode: 'horizontal',
        pager: false,
        controls: false,
        auto: false,
        pause: 4000,
        responsive: true
    });
}); 

$(document).ready(function () {
    $('#slider-products-page').bxSlider({
        mode: 'horizontal',
        pager: true,
        controls: true,
        auto: true,
        pause: 4000,
        responsive: true
    });
}); 