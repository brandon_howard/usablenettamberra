;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsVideoEditor",
			defaults = {
				dragAndDrop: false
			};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( options.settings, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initizes the Text Editor Form
				 * @return {void}
				 */
				init: function ()
				{
					$(document).bind('drop dragover', function (e) {
						e.preventDefault();
					});
				},

				/**
				 * Calls the methods to build and add the form to the popup
				 * @return {void}
				 */
				loadEditor: function()
				{
					var imageForm = this._makeForm();
					this._addForm(imageForm);
					this._addListeners();
				},

				/**
				 * Builds the form with the text field
				 * @return {object} jQuery selector of a form element containing all elements
				 */
				_makeForm: function ()
				{
					// Build all the form elements
					var _formgroup = $('<div class="form-group">');
					var _fileField = $('<input>')
										.addClass('form-control')
										.attr({
											type:        "file",
											name:        this.settings.model+'.'+this.settings.fieldName,
											value:       this.settings.fieldValue,
											placeholder: this.settings.fieldLabel,
											id:          "tms_form_"+this.settings.fieldId
										});
					var _clearfield = $('<input>')
										.addClass('tms-clear-field')
										.attr({
											type:        "hidden",
											name:        this.settings.model+'.clearfield',
											value:       '',
										});

					var _contentType = $('<input>')
										.addClass('tms-content-type')
										.attr({
											type:        "hidden",
											name:        'video',
											value:       '',
										});
					var _dropZone  = $('<div>')
										.addClass('tms-editor-dropzone')
										.html('<h4>Or Drag &amp; Drop your file here to upload</h4>');


					var _clearBtn = $('<button type="button" name="tms-clear" class="tms-clear mt">'+ this.settings.clearText +'</button>')
									.addClass(this.settings.clearClass);

					// Glue it all together
					if (this.settings.fieldLabel !== '' && this.settings.showLabel)
					{
						_fileFieldLabel = $('<label>').html(this.settings.fieldLabel);
						_formgroup.append(_fileFieldLabel);
					}


					_formgroup.append(_fileField);
					if (this.settings.dragAndDrop)
					{
						_formgroup.append(_dropZone);
					}
					_formgroup.append(_clearfield);
					_formgroup.append(_contentType);

					_formgroup.append(_clearBtn);

					return _formgroup;
				},

				/**
				 * Adds the form to the Popup.
				 *
				 * TODO: detect if this is a popup instance or inline and
				 * add to the correct place.
				 *
				 * @param {obj} form An jQuery selector object that will be attached
				 * @return {void}
				 */
				_addForm: function (form)
				{
					var base = this;

					$('.tms-editor-form').prepend(form);
					$('.tms-editor-form').attr('enctype', 'multipart/form-data');

					$('.tms-clear').click(function(){
						$('.tms-clear-field').val(base.settings.fieldName);
						$(base.element).tmsEditorsNetwork('submitForm');
					});
				},

				_addListeners: function()
				{
					var base = this;

					$('#tms_form_'+this.settings.fieldId).change(function(e)
					{
						var _key = base.settings.model+'['+base.settings.fieldName+']';
						var _data = this.files[0];

						$(base.element).tmsEditor('addFileToQueue', _key, _data);
					});

					$(this.element).bind('tmsEditorSavedUpdateValues', function(response)
					{
						if(response.jqXHR.data !== undefined)
						{
							base.settings.fieldValue = response.jqXHR.data[base.settings.model][base.settings.fieldName];
						}
					});
				}

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};

})( jQuery, window, document );
