window.formInProgress = '';
window.formStarted = false;
var showModal = (function($, w, undefined) {
	var backdrop,
		modal,
		form,
		modalId;

	var init = function(e, validationRules) {
		e.preventDefault();

		backdrop = $('<div class="modal-backdrop fade"></div>');
		$('body').append(backdrop[0]);
		backdrop.addClass('in');

		modalId = $(e.currentTarget).data('modal');
		modal = $(modalId);
		modal.removeClass('hide').addClass('in');
 
		form = $(modalId + '-form');
		$(modalId + '-submit').off('click');
		$(modalId + '-submit').on('click', function(e) {
			console.log('bound event to submit');
			console.log(modalId);
			e.preventDefault();
			e.stopPropagation();
			submitForm(e, validationRules);
		});

		modal.find('[data-modal-close]').on('click', closeModal);

		if (!jQuery.isEmptyObject(validationRules)){

			jQuery(':input').blur(function () {
				if (jQuery(this).val().length > 0) {
					w.formStarted = true;
					var parentForm = jQuery(this).closest('form').find('input[name=program_name]').val();

					if (w.formInProgress != parentForm) {
						w.formInProgress = jQuery(this).closest('form').find('input[name=program_name]').val();

						if (jQuery(this).closest('form').data('formStarted') !== true) {
							var eventCategory = (w.formInProgress == 'Website Partnership Request') ? 'Partner Form' : 'Lead Form';
							dataLayer.push({
								'eventCategory':eventCategory,
								'eventAction':'Start',
								'eventLabel':w.formInProgress,
								'eventNonInteraction':'0',
								'event':'formEvent'
							});

							jQuery(this).closest('form').data('formStarted', true);
						}
					}
				}
			});

			jQuery(w).unload(function(){
				if (w.formStarted === true && w.formInProgress !== ''){
					var eventCategory = (w.formInProgress == 'Website Partnership Request') ? 'Partner Form' : 'Lead Form';
					dataLayer.push({
						'eventCategory':eventCategory,
						'eventAction':'Abandon',
						'eventLabel':w.formInProgress,
						'eventNonInteraction':'1',
						'event':'formEvent'
					});
				}
			});
		}

	};

	var submitForm = function(e, validationRules) {
		$(modalId + '-submit').addClass('hide');
		$(modalId + '-submit').siblings('.submitting').removeClass('hide');

		$('.message-fail').remove();

		if (!jQuery.isEmptyObject(validationRules)){

			jQuery(form).validate({
				rules: validationRules,
				submitHandler: function(form, event) {
					var eventCategory = (w.formInProgress == 'Website Partnership Request') ? 'Partner Form' : 'Lead Form';
					dataLayer.push({
						'eventCategory':eventCategory,
						'eventAction':'Complete',
						'eventLabel':jQuery(event.target).closest('form').find('input[name=program_name]').val(),
						'eventNonInteraction':'0',
						'event':'formEvent'
					});

					try {
						window._vis_opt_queue = window._vis_opt_queue || [];
						window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(201);});
					}catch(err){};

					try {
						var fvalues = {};
						fvalues['form_type'] = eventCategory;
						fvalues['program'] = jQuery(event.target).closest('form').find('input[name=program_name]').val();
						fvalues['name'] = jQuery(event.target).closest('form').find('input[name=FirstName]').val()+' '+jQuery(event.target).closest('form').find('input[name=LastName]').val();
						fvalues['email'] = jQuery(event.target).closest('form').find('input[name=Email]').val();
						fvalues['fcompany'] = jQuery(event.target).closest('form').find('input[name=Company]').val();

						if (jQuery(event.target).closest('form').find('input[name=Phone]').val() != ''){
							fvalues['phone'] = jQuery(event.target).closest('form').find('input[name=Phone]').val();
						}

						heap.identify(fvalues);
					}catch(err){}

					window.formStarted = false;
					window.formInProgress = '';

					$.ajax({
						url: $(form).attr('action'),
						data: $(form).serialize(),
						type: 'post',
						dataType: 'json',
						success: parseResponse,
						error: function(x,y) {
							console.log(x);
							console.log(y);
						}
					});
				},
				invalidHandler: function(event, validator) {
					var errors = validator.numberOfInvalids();
					if (errors) {
						var eventCategory = (window.formInProgress == 'Website Partnership Request') ? 'Partner Form' : 'Lead Form';

						dataLayer.push({
							'eventCategory':eventCategory,
							'eventAction':'Error',
							'eventLabel':jQuery(event.target).closest('form').find('input[name=program_name]').val(),
							'eventValue':errors,
							'eventNonInteraction':'0',
							'event':'formEvent'
						});
					}
				},
				highlight: function(element, errorClass, validClass) {
					jQuery(element).addClass(errorClass).removeClass(validClass);
					jQuery(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					jQuery(element).removeClass(errorClass).addClass(validClass);
					jQuery(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
				}
			});

			if ( $(form).valid() ) {
				$(form).submit();
			} else {
				$(modalId + '-submit').removeClass('hide');
				$(modalId + '-submit').siblings('.submitting').addClass('hide');
			}
		}
	};

	var parseResponse = function(json) {
		var errorWrap = $('<div class="message-fail"></div>'),
			errorList = $('<ul></ul>');

		if ( json.errors ) {
			$.each(json.errors, function(i, val) {
				var newEl = $('<li>' + val + '</li>');
				errorList.append(newEl[0]);
				newEl = {};
			});
			errorWrap.append(errorList[0]);
			modal.find('.form-wrap').before(errorWrap[0]);
			$(modalId + '-submit').removeClass('hide');
			$(modalId + '-submit').siblings('.submitting').addClass('hide');
		} else {
			modal.find('.form-body').html(json.message);
		}
	};

	var closeModal = function(e) {
		e.preventDefault();
		modal.removeClass('in').addClass('hide');
		backdrop.removeClass('in');
		backdrop.remove();
	};

	return {
		init: init
	};
})(jQuery, window);

$(document).ready(function() {
	var validationRules;
	$.validator.addMethod("phone", function(phone_number, element) {
		phone_number = phone_number.replace(/\s+/g, "");
		return this.optional(element) || phone_number.length > 9 &&
			phone_number.match(/^[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}$/);
	}, "Please specify a valid phone number");
	jQuery('form').each(function(){
		var formType = jQuery(this).find('input[name="from"]').val();
		validationRules = {};

		switch (formType){
			case 'ux-form':
				validationRules = {
					FirstName: {
						required: true
					},
					LastName: {
						required: true
					},
					Email: {
						required: true,
						email: true
					},
					Phone: {
						phone: true
					},
					Company: {
						required: true
					}
				};
				break;
			case 'contact-form-offices':
				validationRules = {
					FirstName: {
						required: true
					},
					LastName: {
						required: true
					},
					Email: {
						required: true,
						email: true
					},
					Company: {
						required: true
					}
				};
				break;
			case 'footer-contact':
				validationRules = {
					FirstName: {
						required: true
					},
					LastName: {
						required: true
					},
					Email: {
						required: true,
						email: true
					},
					Company: {
						required: true
					}
				};
				break;
			case 'contact-form-partner':
				validationRules = {
					FirstName: {
						required: true
					},
					LastName: {
						required: true
					},
					Email: {
						required: true,
						email: true
					},
					Company: {
						required: true
					}
				};
				break;
			case 'contact-form-videos':
				validationRules = {
					FirstName: {
						required: true
					},
					LastName: {
						required: true
					},
					Email: {
						required: true,
						email: true
					},
					Company: {
						required: true
					}
				};
				break;
			case 'contact-form-paragraph':
				validationRules = {
					FirstName: {
						required: true
					},
					LastName: {
						required: true
					},
					Email: {
						required: true,
						email: true
					},
					Company: {
						required: true
					}
				};
				break;
		}
	});
	$('[data-modal-trigger]').on('click', function(e) {
		showModal.init(e, validationRules);
	});
});