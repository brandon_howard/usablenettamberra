$(document).ready(function () {

    window.addEventListener('orientationchange', function () {
        if (window.orientation === 0 || window.orientation === 180) {
            // Reset scroll position if in portrait mode.
            window.scrollTo(0, 0);
        }
    }, false);
    
    if ($('.slider-case-study').length > 0) {
        var slider = $('#slider-case-studies').bxSlider({
            mode: 'horizontal',
            pager: true,
            controls: false,
            auto: true,
            pause: 4000,
            responsive: true
        });
    }

    if ($('.section.main-hero').length > 0) {
        var homeSlider = $('#hero-slider').bxSlider({
            mode: 'fade',
            pager: false,
            controls: false,
            auto: true,
            pause: 8000,
            responsive: true,
            onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                $('#hero-nav li').removeClass('active');
                $("#hero-nav ul li:nth-child(" + (newIndex + 1) + ")").addClass('active');
            }
        });
        $('#hero-nav li').click(function () {
            if (!$(this).hasClass('active')) {
                $('#hero-nav li').removeClass('active');
                var index = $(this).index();
                homeSlider.goToSlide(index);
                $("#hero-nav ul li:nth-child(" + (index + 1) + ")").addClass('active');
            }

            return false;
        });
    } 

    if ($('.content-menu').length > 0 && $('.content-wrapper').length > 0) {
        $('.content-menu li').click(function () {
            if (!$(this).hasClass('active')) {
                $('.content-wrapper div').hide();
                $('.content-menu li').removeClass('active');
                var index = $(this).index();
                $(this).addClass('active');
                $(".content-wrapper div:nth-child(" + (index + 1) + ")").show();
            }

            return false;
        });
    }

    $('.job-form-show').click(function () {
        $(this).siblings('.job-form').show();
    });

    if ($('.modal-video').length > 0) {

        var _video = document.getElementById('video-player');
        $('.modal-video').click(function () {
            var videos = $(this).data('video-url');
            var source = '';

            if (videos instanceof Array) {
                var isFirefox = typeof InstallTrigger !== 'undefined';
                for (var i = 0; i < videos.length; i++) {
                    var nameArr = videos[i].split('.');
                    var ext = nameArr[ nameArr.length - 1];
                    if (isFirefox && ext == 'ogg') {
                        source = videos[ i ];
                    }
                    if (!isFirefox && ext != 'ogg') {
                        source = videos[ i ];
                    }
                }
            }
            if (typeof videos == 'string') {
                source = videos;
            }

            _video.src = source;
            _video.load();
            $('#modal-video').modal('show');
          
          $('.close').click(function () {
            console.log('brandon test');
              _video.pause();
          });
        });

        $('#modal-video').on('hidden', function () {
          console.log('brandon test');
            _video.pause();
        });
    }

      
    $('.close').click(function () {
      console.log('brandon test');
        _video.pause();
    });

    if ($('.slider').length > 0) {
        var slider = $('.slider').bxSlider({
            mode: 'horizontal',
            pager: true,
            controls: false,
            auto: true,
            pause: 4000,
            responsive: true
        });
    }


    if ($('.slider-case-study').length > 0) {
        var slider = $('#slider-case-studies').bxSlider({
            mode: 'horizontal',
            pager: true,
            controls: false,
            auto: true,
            pause: 4000,
            responsive: true
        });
    }
    



        $('#modal-video').on('hidden', function () {
          console.log('brandon test');
        });

});