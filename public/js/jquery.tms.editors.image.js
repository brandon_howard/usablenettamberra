;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsImageEditor",
			defaults = {
				dragAndDrop: false
			};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( options.settings, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initizes the Text Editor Form
				 * @return {void}
				 */
				init: function ()
				{
					$(document).bind('drop dragover', function (e) {
						e.preventDefault();
					});
				},

				/**
				 * Calls the methods to build and add the form to the popup
				 * @return {void}
				 */
				loadEditor: function()
				{
					var imageForm = this._makeForm();
					this._addForm(imageForm);
					this._addListeners();
				},

				/**
				 * Builds the form with the text field
				 * @return {object} jQuery selector of a form element containing all elements
				 */
				_makeForm: function ()
				{
					// Build all the form elements
					var _formgroup = $('<div class="form-group">');
					var _fileField = $('<input>')
										.addClass('form-control')
										.attr({
											type:        "file",
											name:        this.settings.model+'.'+this.settings.fieldName+'||src',
											placeholder: this.settings.fieldLabel,
											id:          "tms_form_"+this.settings.fieldId+"_src"
										});
					var _fileVal = '';
					if (this.settings.fieldValue !== null && this.settings.fieldValue.src !== null)
					{
						_fileVal = this.settings.fieldValue.src;
					}

					var _originalFileField = $('<input>')
										.attr({
											type:        "hidden",
											name:        this.settings.model+'.'+this.settings.fieldName+'||orig_src',
											value:       _fileVal
										});

					var _clearfield = $('<input>')
										.addClass('tms-clear-field')
										.attr({
											type:        "hidden",
											id:          "tms-clearfield-"+this.settings.fieldId,
											name:        this.settings.model+'.clearfield||'+this.settings.fieldName,
											value:       '',
										});

					var _altVal = '';
					if (this.settings.fieldValue !== null && this.settings.fieldValue.alt !== null)
					{
						_altVal = this.settings.fieldValue.alt;
					}

					var _altField = $('<input>')
										.addClass('form-control')
										.attr({
											type:        "text",
											name:        this.settings.model+'.'+this.settings.fieldName+'||alt',
											value:       _altVal,
											placeholder: 'Alternative Text',
											id:          "tms_form_"+this.settings.fieldId+"_alt"
										});

					var _clearBtn = $('<button type="button" name="tms-clear" id="tms-clearbtn-'+this.settings.fieldId+'" class="tms-fr">'+ this.settings.clearText +'</button>')
									.addClass(this.settings.clearClass);
					// Glue it all together
					if (this.settings.fieldLabel !== '' && this.settings.showLabel)
					{
						_fileFieldLabel = $('<label>').html(this.settings.fieldLabel);
						_formgroup.append(_fileFieldLabel);
					}

					_formgroup.append(_fileField);
					_formgroup.append(_originalFileField);
					_formgroup.append(_clearfield);

					if (this.settings.fieldLabel !== '' && this.settings.showLabel)
					{
						_altFieldLabel = $('<label>').html('Alternative Text');
						_formgroup.append(_altFieldLabel);
					}
					_formgroup.append(_altField);

					if (this.settings.dragAndDrop)
					{
						_formgroup.append(_dropZone);
					}
					_formgroup.append(_clearBtn);

					return _formgroup;
				},

				/**
				 * Adds the form to the Popup.
				 *
				 * TODO: detect if this is a popup instance or inline and
				 * add to the correct place.
				 *
				 * @param {obj} form An jQuery selector object that will be attached
				 * @return {void}
				 */
				_addForm: function (form)
				{
					var base = this;

					$('.tms-editor-form').prepend(form);
					$('.tms-editor-form').attr('enctype', 'multipart/form-data');

					$('#tms-clearbtn-'+this.settings.fieldId).click(function(){
						$('#tms-clearfield-'+base.settings.fieldId).val(base.settings.fieldName);
					});
				},

				_addListeners: function()
				{
					var base = this;

					$('#tms_form_'+this.settings.fieldId+'_src').change(function(e)
					{
						var _key = base.settings.model+'['+base.settings.fieldName+'||src]';
						var _data = this.files[0];

						$(base.element).tmsEditor('addFileToQueue', _key, _data);
					});

					$(this.element).bind('tmsEditorSavedUpdateValues', function(response)
					{
						if(response.jqXHR.data !== undefined)
						{
							var value = response.jqXHR.data[base.settings.model][base.settings.fieldName];
							base.settings.fieldValue = $.parseJSON(value);
						}
					});
				}

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};

})( jQuery, window, document );
