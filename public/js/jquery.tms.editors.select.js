;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsSelectEditor",
			defaults = {};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( options.settings, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initizes the Text Editor Form
				 * @return {void}
				 */
				init: function ()
				{
					this._addListeners();
				},

				/**
				 * Calls the methods to build and add the form to the popup
				 * @return {void}
				 */
				loadEditor: function()
				{
					var selectForm = this._makeForm();
					this._addForm(selectForm);
				},

				/**
				 * Builds the form with the text field
				 * @return {object} jQuery selector of a form element containing all elements
				 */
				_makeForm: function ()
				{
					// Build all the form elements
					var _formgroup      = $('<div class="form-group">');
					var _selectField      = $('<select>')
									.addClass('form-control')
									.attr({
										name:        this.settings.model+'.'+this.settings.fieldName,
										value:       this.settings.fieldValue,
										placeholder: this.settings.fieldLabel,
										id:          "tms_form_" + this.settings.fieldId,
									});

					_selectField.css({
									fontSize: $(this.element).css('fontSize'),
									fontWeight: $(this.element).css('fontWeight'),
									fontFamily: $(this.element).css('fontFamily'),
								});


					$.each(this.settings.options, function(key, option){
						var _option = $('<option>').val(key).html(option);
						_selectField.append(_option);
					});

					// Glue it all together
					if (this.settings.fieldLabel !== '' && this.settings.showLabel)
					{
						_selectFieldLabel = $('<label>').html(this.settings.fieldLabel);
						_formgroup.append(_selectFieldLabel);
					}
					_formgroup.append(_selectField);

					return _formgroup;
				},

				/**
				 * Adds the form to the Popup.
				 *
				 * TODO: detect if this is a popup instance or inline and
				 * add to the correct place.
				 *
				 * @param {obj} form An jQuery selector object that will be attached
				 * @return {void}
				 */
				_addForm: function (form)
				{
					$('.tms-editor-form').prepend(form);
					$("#tms_form_" + this.settings.fieldId).val(this.settings.fieldValue);
				},

				_addListeners: function()
				{
					var base = this;

					$(this.element).bind('tmsEditorSavedUpdateValues', function(response)
					{
						base.settings.fieldValue = $("#tms_form_" + base.settings.fieldId).val();
					});
				}

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};

})( jQuery, window, document );
