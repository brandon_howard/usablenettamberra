;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsEditorsInline",
			inlineEl = '',
			inlineBlockerEl = '',
			offsetPadding = {width:40, height:30},
			defaults = {};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initializes the Popup plugin
				 * @return {void}
				 */
				init: function ()
				{
					var base = this;

					$( window ).resize(function() {
						base._setInlineLocation();
					});

				},

				showInline: function()
				{
					this._loadTmsInlineBlocker();
					this._loadTmsInline();
					this._addListeners();
				},

				hideInline: function()
				{
					this._unloadTmsInline();
				},

				/**
				 * Creates and appends the popup blocker to the DOM and animates
				 * @return {void}
				 */
				_loadTmsInlineBlocker: function()
				{
					var base = this;

					inlineBlockerEl = $('<div class="tms-editor-inline-blocker">');
					$( 'body' ).append(inlineBlockerEl);

					inlineBlockerEl.animate({
						opacity: 1
					});

					inlineBlockerEl.click(function(){
						base._unloadTmsInline();
					});
				},

				_unloadTmsInlineBlocker: function()
				{
					inlineBlockerEl.animate({
						opacity:0
					},
					{
						complete: function()
						{
							inlineBlockerEl.remove();
						}
					});

				},

				/**
				 * Creates and appends the popup to the DOM and animates
				 * @return {void}
				 */
				_loadTmsInline: function ()
				{
					var base = this;

					offsetPadding.width = parseInt($(this.element).css('paddingLeft').replace("px", ""));
					offsetPadding.height = parseInt($(this.element).css('paddingTop').replace("px", ""));

					var _submitBtn = $('<button type="submit" class="tms-btn-success">'+ this.settings.submitText +'</button>')
									.addClass(this.settings.submitClass);

					var _cancelBtn = $('<button type="button" class="tms-btn tms-btn-cancel">'+ this.settings.cancelText +'</button>')
									.addClass(this.settings.cancelClass);

					var _form = $('<form>')
									.addClass('tms-editor-form')
									.attr({
										role: 'form',
										method: 'POST',
										action: this.settings.formAction,
										id: this.settings.fieldId + '_editor_form'
									});

					if (this.settings.type === 'color')
					{
						_submitBtn.addClass('btn-xs');
						_cancelBtn.addClass('btn-xs');
					}

					_form.append(_submitBtn);
					_form.append(_cancelBtn);

					var _method = $('<input>')
									.attr({
										type: "hidden",
										name: "_method",
										value: this.settings.formMethod
									});

					_form.append(_method);

					var _clearDiv = $('<div style="clear:both">&nbsp;</div>');

					var _wrapperWidth = $(this.element).width() + offsetPadding.width + 20;


					_wrapperWidth = 600;

					var _inlineElInnerWrap = $('<div>')
									.addClass('tms-editor-inline-wrapper')
									.append(_form);

					inlineEl = $('<div class="tms-editor-inline">')
								.css({
									height: 'auto',
									width: _wrapperWidth,
									fontSize: $(this.element).css('fontSize'),
									fontWeight: $(this.element).css('fontWeight'),
									fontFamily: $(this.element).css('fontFamily'),
								})
								.append(_inlineElInnerWrap)
								.append(_clearDiv);

					$( 'body' ).append(inlineEl);

					$('.tms-btn-cancel').click(function(){
						base._unloadTmsInline();
					});

					inlineEl.animate({
						opacity: 1
					},
					{
						progress: base._setInlineLocation()
					});
				},

				_unloadTmsInline: function ()
				{
					var base = this;

					inlineEl.animate({
						opacity: 0
					},
					{
						progress: base._setInlineLocation(),
						complete: function()
						{

							$(base.element).trigger({
								'type': 'tmsEditorClosed'
							});

							$('.tms-editor-form').remove();
							inlineEl.remove();
							base._unloadTmsInlineBlocker();
						}
					});
				},

				_setInlineLocation: function()
				{
					var _elementOffset = $(this.element).offset();

					var _marginTop       = parseInt($(this.element).css('marginTop').replace("px", ""));
					var _marginLeft      = parseInt($(this.element).css('marginLeft').replace("px", ""));
					var _top             = offsetPadding.height + _marginTop;
					var _left            = offsetPadding.width + _marginLeft;

					inlineEl.css({
						left: (_elementOffset.left - offsetPadding.width) + _marginLeft - 32,
						top: _elementOffset.top + _marginTop - 32
					});

				},

				_addListeners: function()
				{
					var base = this;
					$(this.element).on('tmsEditorSaved', function(){
						base.hideInline();
					});

					$('.tms-cancel').click(function(){
						base.hideInline();
					});
				},

				/**
				 * Appends errors to the popup
				 * @param  {Object} html jQuery built html selector
				 * @return {void}
				 */
				appendErrors: function(html)
				{
					inlineEl.children('.alert').remove();
					inlineEl.prepend(html);
				},

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};


})( jQuery, window, document );
