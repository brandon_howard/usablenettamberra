;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsTextAreaEditor",
			defaults = {};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( options.settings, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initizes the Text Editor Form
				 * @return {void}
				 */
				init: function ()
				{
					this._addListeners();
				},

				/**
				 * Calls the methods to build and add the form to the popup
				 * @return {void}
				 */
				loadEditor: function()
				{
					var textAreaForm = this._makeForm();
					this._addForm(textAreaForm);
				},

				/**
				 * Builds the form with the text field
				 * @return {object} jQuery selector of a form element containing all elements
				 */
				_makeForm: function ()
				{
					// Build all the form elements
					var _formgroup      = $('<div class="form-group">');
					var _textAreaField      = $('<textarea>')
									.addClass('form-control')
									.attr({
										name:        this.settings.model+'.'+this.settings.fieldName,
										placeholder: this.settings.fieldLabel,
										id:          "tms_form_" + this.settings.fieldId,
									})
									.html(this.settings.fieldValue);

					_textAreaField.css({
									fontSize: $(this.element).css('fontSize'),
									fontWeight: $(this.element).css('fontWeight'),
									fontFamily: $(this.element).css('fontFamily'),
									lineHeight: $(this.element).css('lineHeight'),
								});

					// Glue it all together
					if (this.settings.fieldLabel !== '' && this.settings.showLabel)
					{
						_textAreaFieldLabel = $('<label>').html(this.settings.fieldLabel);
						_formgroup.append(_textAreaFieldLabel);
					}
					_formgroup.append(_textAreaField);

					return _formgroup;
				},

				/**
				 * Adds the form to the Popup.
				 *
				 * TODO: detect if this is a popup instance or inline and
				 * add to the correct place.
				 *
				 * @param {obj} form An jQuery selector object that will be attached
				 * @return {void}
				 */
				_addForm: function (form)
				{
					var base = this;
					var _height = 0;

					$('.tms-editor-form').prepend(form);

					if($('.tms-editor-form').parent().hasClass('tms-editor-inline-wrapper'))
					{
						_height = $(this.element).height();

					}
					else
					{
						_height = $( window ).height() * 0.25;
					}

					$('.tms-editor-form').find('textarea').height(_height);

					if(this.settings.type === 'wysiwyg')
					{
						if (!CKEDITOR.stylesSet.registered['tms_styles'])
						{
							CKEDITOR.stylesSet.add( 'tms_styles', [
								{ name: 'Huge',  element: 'h4', attributes: { 'class': 'huge' } },
								{ name: 'Title', element:'h3',  attributes: { 'class': 'title' } }
							]);
						}

						this.currentCkEditor = CKEDITOR.replace( "tms_form_" + this.settings.fieldId, {
							height: _height - 100,
							autoGrow_onStartup: true,
							removePlugins: 'maximize, iframe, about',
							filebrowserUploadUrl: '/ckeditor/file-upload',
							contentsCss: '/css/ckeditor-styles.css',
							instanceName: 'tms-ckeditor',
							format_tags: 'h1;h2;h3;h4;div;p',
							toolbar : [
								{ name: 'clipboard', items: [ 'Styles','Format','Font','FontSize','-','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'Link', 'Unlink', 'Anchor', '-', 'Image', '-', 'CreateDiv'] },
								{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat','-','NumberedList','BulletedList','-','Blockquote','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', '-','HorizontalRule','SpecialChar', '-', 'Source' ] },
							]
						});

						this.currentCkEditor.config.extraPlugins = 'div';
						this.currentCkEditor.config.stylesSet = 'tms_styles';
						this.currentCkEditor.config.baseFloatZIndex = 30000;
					}
				},

				_addListeners: function()
				{
					var base = this;

					$(this.element).bind('tmsEditorSavedUpdateValues', function(response)
					{
						base.settings.fieldValue = $("#tms_form_" + base.settings.fieldId).val();
					});

					$(this.element).bind('tmsEditorClosed', function()
					{
						base.currentCkEditor.destroy();
					});
				}

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};

})( jQuery, window, document );
