;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsEditorsPopup",
			defaults = {},
			popupEl,
			popupBlockerEl;

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initializes the Popup plugin
				 * @return {void}
				 */
				init: function ()
				{
					var base = this;
					$( window ).resize(function() {
						base._setPopupLocation();
					});

				},

				showPopup: function()
				{
					this._loadTmsPopupBlocker();
					this._loadTmsPopup();
					this._addListeners();
				},

				hidePopup: function()
				{
					this._unloadTmsPopup();
				},

				/**
				 * Creates and appends the popup blocker to the DOM and animates
				 * @return {void}
				 */
				_loadTmsPopupBlocker: function()
				{
					var base = this;

					popupBlockerEl = $('<div class="tms-editor-popup-blocker">');
					$( 'body' ).append(popupBlockerEl);

					popupBlockerEl.animate({
						opacity: 1
					});

					popupBlockerEl.click(function(){
						base._unloadTmsPopup();
					});
				},

				_unloadTmsPopupBlocker: function()
				{
					popupBlockerEl.animate({
						opacity:0
					},
					{
						complete: function()
						{
							popupBlockerEl.remove();
						}
					});

				},

				/**
				 * Creates and appends the popup to the DOM and animates
				 * @return {void}
				 */
				_loadTmsPopup: function ()
				{
					var _submitBtn = $('<button type="submit">'+ this.settings.submitText +'</button>')
									.addClass(this.settings.submitClass);

					var _cancelBtn = $('<button type="button" class="tms-cancel">'+ this.settings.cancelText +'</button>')
									.addClass(this.settings.cancelClass);


					var _form = $('<form>')
									.addClass('tms-editor-form form-vertical')
									.attr({
										role: 'form',
										method: 'POST',
										action: this.settings.formAction,
										id: this.settings.fieldId + '_editor_form'
									});

					_form.append(_submitBtn);
					_form.append(_cancelBtn);

					var _method = $('<input>')
									.attr({
										type: "hidden",
										name: "_method",
										value: this.settings.formMethod
									});

					_form.append(_method);


					popupEl = $('<div class="tms-editor-popup">')
								.css({
									height: 'auto',
									overflow: 'auto'
								}).append(_form);

					$( 'body' ).append(popupEl);

					popupEl.animate({
						width: this.settings.width,
						opacity: 1
					},
					{
						progress: this._setPopupLocation
					});
				},

				_unloadTmsPopup: function ()
				{
					var base = this;

					popupEl.animate({
						width: 0,
						height: 0,
						opacity: 0
					},
					{
						progress: this._setPopupLocation,
						complete: function()
						{
							$(base.element).trigger({
								'type': 'tmsEditorClosed'
							});

							popupEl.remove();
							base._unloadTmsPopupBlocker();
						}
					});
				},

				/**
				 * Finds the center of the screen and realigns the popup
				 * as needed. Also places these values in the settings obj
				 * in case they are needed elsewhere.
				 */
				_setPopupLocation: function()
				{
					popupEl.css({
						left: ($( document ).width() / 2) - (popupEl.width() / 2),
						top: ($( window ).height() / 2) - (popupEl.height()/2)
					});
				},

				_addListeners: function()
				{
					var base = this;
					$(this.element).on('tmsEditorSaved', function(){
						base.hidePopup();
					});

					$('.tms-cancel').click(function(){
						base.hidePopup();
					});
				},

				/**
				 * Appends errors to the popup
				 * @param  {Object} html jQuery built html selector
				 * @return {void}
				 */
				appendErrors: function(html)
				{
					popupEl.children('.alert').remove();
					popupEl.prepend(html);
				}

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};


})( jQuery, window, document );
