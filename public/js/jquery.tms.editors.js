;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsEditor",
			defaults = {
				debug:              "true",
				create:             false,
				destroy:            false,
				type:               "text",
				model:              "",
				fieldName:          "",
				group:              "",
				parent:             "",
				formAction:         "",
				formMethod:         "PUT",
				updateTarget:       "",
				updateAttribute:    "html",
				fieldClass:         "",
				fieldValue:         "",
				fieldLabel:         "",
				options:            [],
				showLabel:          true,
				submitClass:        "tms-btn-success",
				submitText:         "Submit",
				cancelClass:        "tms-btn",
				cancelText:         "Cancel",
				clearClass:         "tms-btn",
				clearText:          "Clear Field",
				inline:             false,
				width:              0.7,
				height:             0.7,
				beforeSendCallback: '',
				afterSaveCallback:  '',
			};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initializes the Plugin
				 * @return {void}
				 */
				init: function ()
				{
					if(this._setSettings())
					{
						this._setAdditionalClasses();
						this._attachListeners();
					}
				},

				/**
				 * Initializes the Editor
				 * (probably when the user clicks the element)
				 * @return {void}
				 */
				_initializeEditor: function()
				{
					var base = this;

					if(this.settings.destroy)
					{
						var confirmation = confirm("Are you sure you want to remove this?");
						if (confirmation === true)
						{
							var data = {
								'_method': 'DELETE',
								'model':   base.settings.model
							};

							var _ajaxSettings = {
								type: 'POST',
								url:  base.settings.formAction,
								data: data
							};

							$.ajax(_ajaxSettings)
								.done(function(){
									base._updateTarget();
								});
						}
					}
					else
					{
						this.settings.groupMembers = this._getGroupMembers();

						if (this.settings.updateTarget !== 'inline')
						{
							this._loadPopup();
						}
						else
						{
							this._loadInline();
						}

						if(this.settings.parent === '')
						{
							this._loadNetwork();

							$(this.element).on('tmsEditorSaved', function(e)
							{
								base._updateTarget();
							});
						}

						$.each(this.settings.groupMembers, function(key, member)
						{
							$(member.element).tmsEditor(member.settings.type, base.element);
						});
					}
				},

				/**
				 * Public function that returns the current instantiation's settings
				 * @return {object} Settings
				 */
				getSettings: function()
				{
					return this.settings;
				},

				/**
				 * Public function to set the value of an instantiation's settings
				 * @param {void}
				 */
				setFieldValue: function(value)
				{
					this.settings.fieldValue = value;
				},

				addFileToQueue: function(_key, _data)
				{
					$(this.settings.parent).tmsEditorsNetwork('addDataToQueue', _key, _data);
				},

				/**
				 * Calculates any additional seetings that need to set
				 * @return {boolean}
				 */
				_setSettings: function ()
				{
					data = $(this.element).data('tms-editor');

					if ($.isPlainObject( data ))
					{
						// Form Mode
						if (this.settings.updateTarget !== 'inline')
						{
							this.settings.form = $('.tms-editor-popup');
						} else {
							this.settings.form = $('.tms-editor-inline');
						}

						if ($(this.element).attr('id') === undefined)
						{
							this.settings.fieldId = this._makeId(32);
							$(this.element).attr('id', this.settings.fieldId);
						}
						else
						{
							this.settings.fieldId = $(this.element).attr('id');
						}

						this.settings = $.extend( this.settings, this.settings, data );

						this.setWidth();
						this.setHeight();

						return true;
					}
					else
					{
						if (this.settings.debug) {
							//alert('Error loading settings for ID: ' + $(this.element).attr('id'));
							console.log(data);
						}

						return false;
					}
				},

				/**
				 * Gets all the group members of the current editor instatiation
				 * @return {array} Group Members
				 */
				_getGroupMembers: function()
				{
					var base = this;
					var _groupMembers = [];

					if(this.settings.group !== '')
					{
						var _group = $('.'+this.settings.group);
						_group.each(function(key, val)
						{
							_groupMembers.push({element: val, settings: $(val).tmsEditor('getSettings')});
						});

					} else {
						_groupMembers.push({element: this.element, settings: this.settings});
					}

					return _groupMembers.reverse();
				},

				/**
				 * Attaches the click event lister to whatever we have bound base plugin to
				 * @return {void}
				 */
				_attachListeners: function ()
				{
					var base = this;
					$(this.element).click(function()
					{
						base._initializeEditor();
					});

					$(this.element).on('tmsEditorFailed', function(e){
						base._displayErrors(e.response.responseJSON.errors);
					});
				},

				/**
				 * Sets additional classes on the current instatiated element
				 * @return {void}
				 */
				_setAdditionalClasses: function()
				{
					if(this.settings.group !== '')
					{
						$(this.element).addClass(this.settings.group);
					}
				},


				/**
				 * Initializes the TMS Popup Modal Window plugin
				 * @return {void}
				 */
				_loadPopup: function()
				{
					// Initialize & Load the Modal
					$(this.element).tmsEditorsPopup(this.settings);
					$(this.element).tmsEditorsPopup('showPopup');
				},

				/**
				 * Initializes the TMS Popup Modal Window plugin
				 * @return {void}
				 */
				_loadInline: function()
				{
					// Initialize & Load the Modal
					$(this.element).tmsEditorsInline(this.settings);
					$(this.element).tmsEditorsInline('showInline');
				},

				/**
				 * Initializes the TMS Popup Modal Window plugin
				 * @return {void}
				 */
				_loadNetwork: function()
				{
					$(this.element).tmsEditorsNetwork(this.settings);
				},

				/**
				 * Displays validation errors appropriately
				 *
				 * @todo Need to show validation errors sexily when it's not a popup... hmmm...
				 * @param  {Object} errors
				 * @return {void}
				 */
				_displayErrors: function(errors)
				{
					var alert = $('<div class="alert alert-danger">')
								.html('<p><strong>There were errors with your submission</strong></p>');

					for (var field in errors)
					{
						if(errors.hasOwnProperty(field))
						{
							for (var i = 0; i < errors[field].length; i++) {
								alert.append($('<p>'+errors[field][i]+'</p>'));
							}
						}
					}

					$(this.element).tmsEditorsPopup('appendErrors', alert);
				},

				/**
				 * Updates the targeted selector if one is set
				 * @return {boolean}
				 */
				_updateTarget: function()
				{
					var base = this;

					if (this.settings.create !== true && this.settings.destroy !== true)
					{

						$.each(this.settings.groupMembers, function(i, member)
						{
							var target = member.settings.updateTarget;
							var attribute = member.settings.updateAttribute;
							var newValue = member.settings.fieldValue;

							$(member.element).tmsEditor('setFieldValue', newValue);

							if (target !== '')
							{
								if (attribute !== 'html' && attribute !== undefined && attribute !== '')
								{
									// If it is the style attribute
									if (attribute.substring(0,6) == 'style[')
									{
										var stylePattern = new RegExp("(style\\[)(.{0,})(?=\\])", "g");
										var styleProp    = stylePattern.exec(attribute);

										if (styleProp[2] == 'background-image')
										{
											newValue = 'url(/'+newValue.src+')';
										}

										if (target !== "inline")
										{
											$(target).css(styleProp[2], newValue);
										}
										else
										{
											$(base.element).css(styleProp[2], newValue);
										}
									}
									else
									{
										if(member.settings.type == 'image')
										{
											base._updateImageTarget(newValue, base.element);
										}
										else
										{
											if (target !== "inline")
											{
												$(target).attr(attribute, newValue);
											}
											else
											{
												$(base.element).attr(attribute, newValue);
											}
										}
									}
								}
								else
								{
									if (target !== "inline")
									{
										if(member.settings.type == 'link')
										{
											base._updateLinkTarget(newValue, target);
										}
										else if(member.settings.type == 'image')
										{
											base._updateImageTarget(newValue, target);
										}
										else
										{
											$(target).html(newValue);
										}
									}
									else
									{
										$(base.element).html(newValue);
									}
								}
							}
						});
					} else {

						var target = base.settings.updateTarget;

						if (target !== '' && target !== null) {
							$.get(base.settings.currentUrl, function(data) {
								var body = '<div id="body-mock">' + data.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/g, '') + '</div>';
								var replacement = $(target, $(body));

								$(target).replaceWith(replacement);
								$(".tms-editor").tmsEditor();
							});
						}
					}

					return true;
				},

				_updateLinkTarget: function(values, target)
				{
					$(target).attr('target', values.target);
					$(target).attr('href', values.href);
					$(target).html(values.fieldLabel);
				},

				_updateImageTarget: function(values, target)
				{
					$(target).attr('src', '/' + values.src);
					$(target).attr('alt', values.alt);
				},


				/**
				 * Loads the Color Editor Plugin
				 * @return {void}
				 */
				color: function(parent)
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsColorEditor({
						settings: base.settings
					});

					$(this.element).tmsColorEditor('loadEditor');
				},

				/**
				 * Loads the Date Plugin
				 * @return {void}
				 */
				date: function(parent)
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsDateEditor({
						settings: base.settings
					});

					$(this.element).tmsDateEditor('loadEditor');
				},

				/**
				 * Loads the Image Editor Plugin
				 * @return {void}
				 */
				image: function(parent)
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsImageEditor({
						settings: base.settings
					});

					$(this.element).tmsImageEditor('loadEditor');
				},

				/**
				 * Loads the Link Editor Plugin
				 * @return {void}
				 */
				link: function(parent)
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsLinkEditor({
						settings: base.settings
					});

					$(this.element).tmsLinkEditor('loadEditor');
				},

				/**
				 * Initializes the Select Editor Plugin
				 * @return {void}
				 */
				select: function()
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsSelectEditor({
						settings: base.settings
					});

					$(this.element).tmsSelectEditor('loadEditor');
				},

				/**
				 * Initializes the Text Editor Plugin
				 * @return {void}
				 */
				text: function()
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsTextEditor({
						settings: base.settings
					});

					$(this.element).tmsTextEditor('loadEditor');
				},


				/**
				 * Initializes the Text Area Editor Plugin
				 * @return {void}
				 */
				textarea: function()
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsTextAreaEditor({
						settings: base.settings
					});

					$(this.element).tmsTextAreaEditor('loadEditor');
				},

				/**
				 * Initializes the Text Area Editor Plugin
				 * @return {void}
				 */
				wysiwyg: function()
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsTextAreaEditor({
						settings: base.settings
					});

					$(this.element).tmsTextAreaEditor('loadEditor');
				},

				/**
				 * Loads the Image Editor Plugin
				 * @return {void}
				 */
				video: function(parent)
				{
					var base = this;

					if (parent !== null) this.settings.parent = parent;

					$(this.element).tmsVideoEditor({
						settings: base.settings
					});

					$(this.element).tmsVideoEditor('loadEditor');
				},

				/**
				 * Calculates and sets the width of the Popup
				 *
				 * TODO: I may move base to the Popup plugin if we end up
				 * not needing the values elsewhere
				 */
				setWidth: function()
				{
					var _width = this.settings.width;

					if (_width < 1)
					{
						_width = (_width * 100);
						this.settings.width = _width + '%';
					}
					else
					{
						this.settings.width = _width + 'px';
					}
				},

				/**
				 * Calculates and sets the value of the height settings
				 * for the Popup.
				 */
				setHeight: function()
				{
					var _height = this.settings.height;

					if (_height === 'auto')
					{
						this.settings.height = 'auto';
					}
					else if (_height < 1)
					{
						_height = $( window ).height() * _height;
						this.settings.height = _height + 'px';
					}
					else
					{
						this.settings.height = _height + 'px';
					}
				},

				/**
				 * Generates a random string used for an ID
				 * @param  {int} len
				 * @return {string}
				 */
				_makeId: function(len)
				{
					var text = "";
					var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

					for( var i=0; i < len; i++ )
						text += possible.charAt(Math.floor(Math.random() * possible.length));

					return text;
				}
		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};

})( jQuery, window, document );


