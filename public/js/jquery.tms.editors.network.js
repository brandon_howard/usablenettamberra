;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsEditorsNetwork",
			defaults = {
				formDataQueue: new FormData()
			};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initializes the Popup plugin
				 * @return {void}
				 */
				init: function ()
				{
					this._addFormListener();
				},

				/**
				 * Adds data to the FormData
				 */
				addDataToQueue: function(fieldName, value)
				{
					this.settings.formDataQueue.append(fieldName, value);
				},

				/**
				 * Submits the form via AJAX to the specified url and runs callbacks
				 * @return {void}
				 */
				submitForm: function()
				{
					var base = this;

					this._buildFormData();

					// Callback just before we send it off.
					var fn = window[this.settings.beforeSaveCallback];
					if(typeof fn === 'function') {
						fn(this.settings.formAction, this.settings.formDataQueue);
					}

					var _ajaxSettings = {
							type: 'POST',
							url:  this.settings.formAction,
							data: this.settings.formDataQueue,
							processData: false,
							contentType: false
					};

					base._sendFormData(_ajaxSettings);
				},

				_buildFormData: function()
				{
					var _data = {};
					var base = this;

					$.each($('.tms-editor-form#'+ this.settings.fieldId + '_editor_form').serializeArray(), function(i, field) {

						var _tmp_arr = field.name.split('.');
						var _model = '';

						$.each(_tmp_arr, function(j, val)
						{
							if (j === 0)
							{
								_model = val;

							} else
							{
								base.settings.formDataQueue.append(_model+'['+val+']', field.value);
							}
						});
					});

					base.settings.formDataQueue.append('_method', base.settings.formMethod);
				},

				_sendFormData: function(settings)
				{
					var base = this;

					var _ajaxRequest = $.ajax(settings);

					_ajaxRequest.done(function(jqXHR){

						// Callback just before we send it off.
						var fn = window[base.settings.afterSaveCallback];
						if(typeof fn === 'function') {
							jqXHR = fn(jqXHR);
						}

						base.settings.formDataQueue = new FormData();

						base._handleRequestSuccess(jqXHR);
					});

					_ajaxRequest.fail(function(jqXHR, textStatus)
					{
						base.settings.formDataQueue = new FormData();

						base._handleRequestFail(jqXHR);
					});
				},

				/**
				 * Handles a successful return from a request
				 * @return {void}
				 */
				_handleRequestSuccess: function(jqXHR)
				{
					$.each(this.settings.groupMembers, function(i, member)
					{
						$(member.element).trigger({
							'type': 'tmsEditorSavedUpdateValues',
							'jqXHR': jqXHR
						});

						setTimeout(function() {
							$(member.element).trigger({
								'type': 'tmsEditorSaved',
								'jqXHR': jqXHR
							});
						}, 1000);


					});
				},


				/**
				 * Handles a successful return from a request
				 * @return {void}
				 */
				_handleRequestFail: function(jqXHR)
				{
					$.each(this.settings.groupMembers, function(i, member)
					{
						$(member.element).trigger({
							'type': 'tmsEditorFailed',
							'jqXHR': jqXHR
						});
					});
				},

				/**
				 * Adds listener for click event on form submission
				 * @return {void}
				 */
				_addFormListener: function()
				{
					var base = this;

					$('body').on('submit', '#'+ this.settings.fieldId + '_editor_form', function(e){
						e.preventDefault();
						base.submitForm();
					});
				},

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};


})( jQuery, window, document );
