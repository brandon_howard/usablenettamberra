
;(function ( $, window, document, undefined ) {

		// Create the defaults once
		var pluginName = "tmsLinkEditor",
			defaults = {};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( options.settings, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {

				/**
				 * Initizes the Text Editor Form
				 * @return {void}
				 */
				init: function ()
				{
					this._addListeners();
				},

				/**
				 * Calls the methods to build and add the form to the popup
				 * @return {void}
				 */
				loadEditor: function()
				{
					var base = this;

					this._loadPageOptions(function()
					{
						var textForm = base._makeForm();
						base._addForm(textForm);
					});
				},

				/**
				 * Builds the form with the text field
				 * @return {object} jQuery selector of a form element containing all elements
				 */
				_makeForm: function ()
				{

					var _activeUrl = '';
					var _activePage = '';

					if (this.settings.fieldValue.url !== '') {
						_activeUrl = ' active';
					} else {
						_activePage = ' active';
					}

					// Build all the form elements
					var _formgroup      = $('<div class="form-group">');

					var _label      = $('<input>')
									.addClass('form-control')
									.attr({
										type:        "label",
										name:        this.settings.model+'.'+this.settings.fieldName+'||fieldLabel',
										value:       this.settings.fieldValue.fieldLabel,
										placeholder: 'Field Label',
										id:          "tms_form_" + this.settings.fieldId + "_label",
									});

					var _selfOption  = $('<option>')
									.attr({
										value: '_self'
									}).html('Self');

					var _blankOption  = $('<option>')
									.attr({
										value: '_blank'
									}).html('Blank');

					var _target   = $('<select>')
									.addClass('form-control')
									.attr({
										name:        this.settings.model+'.'+this.settings.fieldName+'||target',
										value:       this.settings.fieldValue.target,
										id:          "tms_form_" + this.settings.fieldId + "_target",
									})
									.append(_selfOption)
									.append(_blankOption);

					var _urlOption  = $('<option>')
									.attr({
										value: 'url'
									}).html('URL');

					var _pageOption  = $('<option>')
									.attr({
										value: 'page'
									}).html('Page');

					var _linkType   = $('<select>')
									.addClass('form-control')
									.attr({
										name:        this.settings.model+'.'+this.settings.fieldName+'||linkType',
										value:       this.settings.fieldValue,
										id:          "tms_form_" + this.settings.fieldId + "_linktype",
									})
									.append(_pageOption)
									.append(_urlOption);

					var _urlContainer  = $('<div>')
									.addClass('tms-link-type tms-link-type-url' + _activeUrl);

					var _urlField      = $('<input>')
									.addClass('form-control')
									.attr({
										type:        "text",
										name:        this.settings.model+'.'+this.settings.fieldName+'||url',
										value:       this.settings.fieldValue.url,
										placeholder: 'URL',
										id:          "tms_form_" + this.settings.fieldId + "_url",
									});

					var _pageIdContainer  = $('<div>')
									.addClass('tms-link-type tms-link-type-page' + _activePage);

					var _pageIdField = $('<select>')
									.addClass('form-control')
									.attr({
										name:        this.settings.model+'.'+this.settings.fieldName+'||pageIdField',
										value:       this.settings.fieldValue.pageId,
										id:          "tms_form_" + this.settings.fieldId + "_pageid",
									});

					_pageIdField.append($('<option>', {
							value: 0,
							text : 'Select a Page'
						}));

					$.each(this.settings.pageOptions, function (index, value) {
						_pageIdField.append($('<option>', {
							value: index,
							text : value
						}));
					});

					// Glue it all together
					if (this.settings.showLabel)
					{
						_textFieldLabel = $('<label>').html('Link Text');
						_formgroup.append(_textFieldLabel);
					}
					_formgroup.append(_label);

					if (this.settings.showLabel)
					{
						_textFieldLabel = $('<label>').html('Target');
						_formgroup.append(_textFieldLabel);
					}
					_formgroup.append(_target);

					if (this.settings.showLabel)
					{
						_textFieldLabel = $('<label>').html('Link Type');
						_formgroup.append(_textFieldLabel);
					}
					_formgroup.append(_linkType);


					if (this.settings.showLabel)
					{
						_textFieldLabel = $('<label>').html('Fully Qualified URL');
						_urlContainer.append(_textFieldLabel);
					}
					_urlContainer.append(_urlField);
					_formgroup.append(_urlContainer);


					if (this.settings.showLabel)
					{
						_textFieldLabel = $('<label>').html('Link to Page');
						_pageIdContainer.append(_textFieldLabel);
					}
					_pageIdContainer.append(_pageIdField);
					_formgroup.append(_pageIdContainer);

					return _formgroup;
				},

				/**
				 * Adds the form to the Popup.
				 *
				 * TODO: detect if this is a popup instance or inline and
				 * add to the correct place.
				 *
				 * @param {obj} form An jQuery selector object that will be attached
				 * @return {void}
				 */
				_addForm: function (form)
				{
					var base = this;

					$('.tms-editor-form').prepend(form);

					if (this.settings.fieldValue.target === '')
					{
						this.settings.fieldValue.target = '_self';
					}

					$("#tms_form_" + this.settings.fieldId + "_target").val(this.settings.fieldValue.target);

					$("#tms_form_" + this.settings.fieldId + "_linktype").change(function(){

						if ($(this).val() == 'url')
						{
							$('.tms-link-type').removeClass('active');
							$('.tms-link-type-url').addClass('active');
						}
						else
						{
							$('.tms-link-type').removeClass('active');
							$('.tms-link-type-page').addClass('active');
						}
					});

					$("#tms_form_" + this.settings.fieldId + "_url").keydown(function(){
						$("#tms_form_" + base.settings.fieldId + "_pageid").val(0);
					});

					$("#tms_form_" + this.settings.fieldId + "_pageid").change(function(){
						$("#tms_form_" + base.settings.fieldId + "_url").val('');
					});

					if (this.settings.fieldValue.url !== '')
					{
						$("#tms_form_" + this.settings.fieldId + "_linktype").val('url');
					}
					else
					{
						$("#tms_form_" + this.settings.fieldId + "_linktype").val('page');
						$("#tms_form_" + this.settings.fieldId + "_pageId").val(this.settings.fieldValue.pageIdField);
					}
				},

				_addListeners: function()
				{
					var base = this;

					$(this.element).bind('tmsEditorSavedUpdateValues', function(response)
					{
						var value = response.jqXHR.data[base.settings.model][base.settings.fieldName];
						base.settings.fieldValue = $.parseJSON(value);
					});
				},

				_loadPageOptions: function(callback)
				{
					var base = this;

					var _ajaxSettings = {
							type: 'GET',
							url:  this.settings.pageListUrl
					};

					var _ajaxRequest = $.ajax(_ajaxSettings);

					_ajaxRequest.done(function(jqXHR){
						base.settings.pageOptions = jqXHR;

						callback();
					});
				}

		};

		$.fn[pluginName] = function ( options ) {
			var args = arguments;

			if (options === undefined || typeof options === 'object') {
				return this.each(function ()
				{
					if (!$.data(this, 'plugin_' + pluginName))
					{
						$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
					}
				});
			}
			else if (typeof options === 'string' && options[0] !== '_' && options !== 'init')
			{
				var returns;

				this.each(function () {
					var instance = $.data(this, 'plugin_' + pluginName);

					if (instance instanceof Plugin && typeof instance[options] === 'function')
					{
						returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}

					if (options === 'destroy')
					{
						$.data(this, 'plugin_' + pluginName, null);
					}
				});

				return returns !== undefined ? returns : this;
			}
		};

})( jQuery, window, document );
